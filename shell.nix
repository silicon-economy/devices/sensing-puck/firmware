{ nightly ? true }:
let
  # Mozilla's Rust overlay gives us the rustChannelOf function which allows us
  # to select a specific Rust toolchain. Furthermore, we can configure
  # additional targets like shown below.
  moz_overlay = import (builtins.fetchTarball {
      name = "mozilla-overlay-2022-07-07";
      #url = "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz";
      url = "https://github.com/mozilla/nixpkgs-mozilla/archive/0508a66e28a5792fdfb126bbf4dec1029c2509e0.tar.gz";
      sha256 = "1nswjmya72g0qriidc2pkl54zn5sg0xp36vdq0ylspca56izivxc";
    });

  # Our own overlay with additional build environment tools
  serum_overlay = import (builtins.fetchGit {
      name = "serum-overlay-2022-11-14";
      url = "https://git.openlogisticsfoundation.org/silicon-economy/libraries/serum/serum-nix-overlay.git";
      allRefs = true;
      rev = "9c917984a1ec73b4358f8f933539e65ebe88af81"; # Use the newest version here
    });
  
  # Pinned nixpkgs
  nixpkgsBase = import (builtins.fetchTarball {
      name = "nixpkgs-stable-22.11";
      url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/22.11.tar.gz";
      sha256 = "11w3wn2yjhaa5pv20gbfbirvjq6i3m7pqrq2msf0g7cv44vijwgw";
    });
  nixpkgs = nixpkgsBase {
      overlays = [ moz_overlay serum_overlay ];
    };

  # On aarch64-darwin (macOS), gcc-arm-embedded is not supported. So we use an other version there.
  nixpkgs2205 = import (builtins.fetchTarball {
      name = "nixpkgs-stable-22.05";
      url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/22.05.tar.gz";
      sha256 = "0d643wp3l77hv2pmg2fi7vyxn4rwy0iyr8djcw1h5x72315ck9ik";
    }) {};

  # Choose between a specific Rust channel and 'latest', between stable and nightly
  # For nightly, we use a specific one so we do not download a new compiler every day
  rustChannel = if nightly
    then nixpkgs.rustChannelOf { channel = "nightly"; date = "2022-09-14"; }
    else nixpkgs.rustChannelOf { channel = "1.64.0"; };

  rust = rustChannel.rust.override {
    targets = [ "thumbv7em-none-eabihf" ];
    extensions = ["rust-src"];
  };

  # Currently (2022-03-03), tarpaulin only supports x86-64 Linux
  optionalCargoTarpaulin = with nixpkgs;
    if stdenv.hostPlatform.isx86_64 && stdenv.hostPlatform.isLinux
      then [ cargo-tarpaulin ]
      else [];
  # cargo-udeps only runs on nightly so we only need to include it if on nightly
  # this lets us skip downloading another nixpkgs when not required
  optionalCargoUdeps = if nightly
    then [ nixpkgs.cargo-udeps ]
    else [];
  # Without the following package, generating mbedtls bindings fails on Linux (only tested on x86_64) with:
  # "fatal error: 'gnu/stubs-32.h' file not found"
  optionalGlibc32 = with nixpkgs;
    if stdenv.hostPlatform.isLinux
    then [ pkgsi686Linux.glibc.dev ]
    else [];

  optionalPackages = optionalCargoTarpaulin ++ optionalCargoUdeps ++ optionalGlibc32;

  gccArmEmbedded = if nixpkgs.stdenv.hostPlatform.isDarwin
    then nixpkgs2205.gcc-arm-embedded
    else nixpkgs.gcc-arm-embedded;
in
  with nixpkgs;
  
  stdenv.mkDerivation {
    name = "rust-env";

    buildInputs = [
      rust
      nixpkgs2205.probe-run
      gitlab-clippy
      cargo2junit
      cargo-deny # license checking
      nodePackages.cspell #  spell checking
      which # for general nix-related debugging
      gccArmEmbedded # for rust-mbedtls
      cmake # for rust-mbedtls
      git
      openssh # to get gitlab dependencies from within the nix shell
      cacert # for certificate verification when downloading from crates.io
      libiconv # for linking on macOS
      clang # for generating headers for rust-mbedtls with bindgen
    ] ++ optionalPackages;

    # Set Environment Variables
    RUST_BACKTRACE = 1;

    # For C cross-compilation, e.g. rust-mbedtls
    CC_thumbv7em_none_eabihf = "arm-none-eabi-gcc";
    LIBCLANG_PATH = "${clang.cc.lib.outPath}/lib";
  }
