// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This binary forwards traffic between the user UART and the BG77 UART which can be used for
//! debugging the modem or implementing more modem features.

#![no_main]
#![no_std]

use core::fmt::Write as _;
use cortex_m_rt::*;
use embedded_hal::serial::{Read, Write};

// Ensure that the panic handler is registered
use firmware as _;

#[entry]
fn main() -> ! {
    // Initialize the RTT as early as possible to avoid probe-run connection timeouts
    rtt_target::rtt_init_print!();

    let puck = board::SensingPuck::take().unwrap();

    #[cfg(feature = "mbedtls")]
    firmware::allocator::init_allocator();

    let mut tx_debug = puck.debug_tx;
    let mut rx_debug = puck.debug_rx;
    let _ = writeln!(tx_debug, "Uart-Bypass (to modem): Just forwarding between user uart and bg77. Remember that the user uart rx is interrupt-driven so either type slowly or increase MCU speed (consider a release build).");
    let _ = write!(tx_debug, "modem.power_on_and_connect_network() ... ");
    let mut modem = puck.modem_driver.into_inner(); // extract driver from RefCell
    modem.power_on_and_connect_network().unwrap();
    let _ = writeln!(tx_debug, "modem connected!");
    let (tx_modem, rx_modem) = unsafe { modem.serial() };
    loop {
        if let Some(byte) = rx_debug.dequeue() {
            let _ = tx_modem.write(byte);
        }
        if let Ok(byte) = rx_modem.read() {
            let bytes = [byte];
            let _ = write!(tx_debug, "{:?}", try_ascii::try_ascii(&bytes));
        }
    }
}
