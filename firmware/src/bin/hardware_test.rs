// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This binary runs a basic hardware test which checks that the hardware is working as intended.
//! At some points, it requires user interactions which is requested via the debug output UART,
//! e.g. to double-tap the device when the double-tap detection is checked.

#![no_main]
#![no_std]

use core::fmt::Debug;
use cortex_m_rt::*;
use embedded_hal::blocking::delay::DelayMs;
use embedded_timers::clock::Clock;
use heapless::Vec;

use board::lsm6dsox;
use board::lsm6dsox::accelerometer::Accelerometer;
use board::WakeupReason;

#[entry]
fn main() -> ! {
    // Initialize the RTT as early as possible to avoid probe-run connection timeouts
    rtt_target::rtt_init_print!();

    let puck = board::SensingPuck::take();
    let mut puck = puck.unwrap();

    unsafe { firmware::logger::init_logger(&mut puck.debug_tx) };
    log::info!("Logger initialized");

    #[cfg(feature = "mbedtls")]
    firmware::allocator::init_allocator();

    basic_hardware_test(
        puck.eeprom,
        puck.led_matrix,
        puck.temperature_humidity_sensor,
        puck.accelerometer,
        puck.esp.enable,
        puck.esp.tx,
        puck.esp.rx,
        quectel_bg77::Bg77ClientStack::new(&mut puck.modem_driver),
        puck.clock,
        puck.vbat,
        puck.sleep_fn,
    );
}

#[allow(clippy::too_many_arguments)] // We could pass the board here but want to demonstrate the abstraction-ability
pub fn basic_hardware_test<ERR, EspTx, EspRx, Storage, StorageErr, SleepFn>(
    eeprom: Storage,
    led_matrix: board::LedMatrix,
    sht: board::Sht3x,
    mut lsm: board::Lsm6dsox,
    esp_enable: impl embedded_hal::digital::v2::OutputPin,
    esp_tx: EspTx,
    esp_rx: EspRx,
    bg77: impl embedded_nal::TcpClientStack,
    clock: &impl Clock,
    mut vbat: board::vbat_adc::VbatAdc,
    sleep: SleepFn,
) -> !
where
    ERR: core::fmt::Debug,
    EspTx: embedded_hal::blocking::serial::Write<u8, Error = ERR>,
    EspRx: embedded_hal::serial::Read<u8, Error = ERR>,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: core::fmt::Debug,
    SleepFn: Fn(Option<core::time::Duration>) -> enumflags2::BitFlags<WakeupReason>,
{
    test_clock(clock);

    match test_eeprom(eeprom) {
        Ok(_) => {
            log::info!("Eeprom OK");
        }
        Err(e) => {
            log::error!("Eeprom test failed: {:?}", e);
        }
    }

    match test_led_matrix(led_matrix) {
        Ok(_) => {
            log::info!("Writing to LED matrix succeeded. Please verify that it is turned on.");
        }
        Err(x) => {
            log::error!("Testing LED matrix failed: {:?}", x);
        }
    };

    match test_sht3xa_temperature_and_humidity_sensor(sht) {
        Ok(_) => {
            log::info!("SHT3XA temperature and humidity sensor ok.");
        }
        Err(x) => {
            log::error!("SHT3XA test failed: {}", x);
        }
    }

    match test_lsm6dsox_mems_sensor(&mut lsm) {
        Ok(_) => {
            log::info!("LSM6DSOX sensor ok.");
        }
        Err(x) => {
            log::error!("LSM6DSOX test failed: {}", x);
        }
    }
    match test_measurement_vbat(&mut vbat) {
        Ok(_) => {
            log::info!("Vbat ok.");
        }
        Err(x) => {
            log::error!("Vbat test failed: {}", x);
        }
    }

    match test_sleep_with_double_tap_wakeup(sleep, &mut lsm) {
        Ok(_) => {
            log::info!("Sleep function seems OK.");
        }
        Err(msg) => {
            log::error!("Sleep test failed: {}", msg);
        }
    }

    match test_bg77(bg77) {
        Ok(_) => {
            log::info!("BG77 ok.");
        }
        Err(x) => {
            log::error!("BG77 test failed: {}", x);
        }
    }

    match test_esp(esp_enable, esp_tx, esp_rx, clock) {
        Ok(_) => {
            log::info!("ESP ok.");
        }
        Err(x) => {
            // This currently fails because of overflows (the UART RX is interrupt-based and too
            // slow)
            log::warn!("ESP test failed: {}", x);
        }
    }

    loop {
        cortex_m::asm::bkpt();
    }
}

fn test_clock(clock: &impl Clock) {
    log::debug!("Waiting a second at {:?} ", clock.try_now());
    clock.new_delay().delay_ms(1000_u32);
    log::debug!("Waiting a second at {:?} ", clock.try_now());
    clock.new_delay().delay_ms(1000_u32);
    log::debug!("Waiting a second at {:?} ", clock.try_now());
}

#[derive(Debug)]
enum EepromError<StorageErr: Debug> {
    /// Error in the Storage trait
    Storage(StorageErr),
    /// Error in one of our assertions
    Check(&'static str),
}
impl<StorageErr: Debug> From<StorageErr> for EepromError<StorageErr> {
    fn from(e: StorageErr) -> Self {
        EepromError::Storage(e)
    }
}

fn test_eeprom<Storage, StorageErr>(mut eeprom: Storage) -> Result<(), EepromError<StorageErr>>
where
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: core::fmt::Debug,
{
    if eeprom.capacity() < 256 {
        return Err(EepromError::Check("Capacity should be at least 256 bytes"));
    }
    // the stm32l4xx-hal only allows to read up to 256 bytes in a single i2c transaction so
    // we just omit the last byte
    let mut bytes = [0u8; 255];
    let zeroes = [0; 256];
    eeprom.write(0, &zeroes)?;
    eeprom.read(0, &mut bytes)?;
    if bytes != zeroes[..255] {
        return Err(EepromError::Check(
            "256 zeroes have been written but something different has been read",
        ));
    }
    let ones = [1; 255];
    eeprom.write(0, &ones)?;
    eeprom.read(0, &mut bytes)?;
    if bytes != ones[..255] {
        return Err(EepromError::Check(
            "256 ones have been written but something different has been read",
        ));
    }
    Ok(())
}

fn test_led_matrix(mut led_matrix: board::LedMatrix) -> Result<(), board::LedMatrixError> {
    use board::issi_is31fl3731::*;
    led_matrix.power_on(true)?;
    let img: [[u8; 13]; 7] = [[0x08; 13]; 7];
    led_matrix.write_frame(FrameId::One, Frame { pixels: img })?;
    led_matrix.show_frame(FrameId::One)?;
    Ok(())
}

fn test_sht3xa_temperature_and_humidity_sensor(mut sht: board::Sht3x) -> Result<(), &'static str> {
    use board::sensirion_rht::Repeatability;
    let _serial = sht
        .read_serial_number()
        .map_err(|_| "Could not read serial number")?;
    let _measurement = sht
        .single_shot(Repeatability::Medium)
        .map_err(|_| "Measurement failed")?;
    Ok(())
}

fn test_lsm6dsox_mems_sensor(lsm: &mut board::Lsm6dsox) -> Result<(), &'static str> {
    let _serial = lsm
        .check_id()
        .map_err(|_| "Could not verify serial number")?;
    lsm.setup().map_err(|_| "Failed to set up lsm6dsox")?;
    let accel = lsm
        .accel_norm()
        .map_err(|_| "Failed to read Acceleration")?;
    log::debug!("Measured Acceleration: {:?}", accel);

    Ok(())
}

fn test_sleep_with_double_tap_wakeup(
    sleep: impl Fn(Option<core::time::Duration>) -> enumflags2::BitFlags<WakeupReason>,
    lsm: &mut board::Lsm6dsox,
) -> Result<(), &'static str> {
    log::info!("Test sleep function");
    lsm.check_id()
        .map_err(|_| "Could not verify serial number")?;
    lsm.setup_tap_detection(
        lsm6dsox::TapCfg {
            en_x_y_z: (true, true, true),
            lir: true,
        },
        lsm6dsox::TapMode::SingleAndDouble,
        Some(lsm6dsox::InterruptLine::INT2),
    )
    .map_err(|_| "Error setting up double tap")?;
    if lsm
        .check_tap()
        .map_err(|_| "error when resetting double tap")?
        .contains(lsm6dsox::TapSource::DoubleTap)
    {
        log::warn!("Double tap already set");
    }
    log::info!("Sleeping for 1 min, double-tap to wakeup");
    let wakeup_reasons = sleep(Some(core::time::Duration::from_secs(60)));
    if wakeup_reasons.contains(WakeupReason::SHTInterrupt) {
        log::debug!("WakeupReason::SHTInterrupt set");
    }
    if wakeup_reasons.contains(WakeupReason::LSM1Interrupt) {
        log::debug!("WakeupReason::LSM1Interrupt set");
    }
    if wakeup_reasons.contains(WakeupReason::LSM2Interrupt) {
        log::debug!("WakeupReason::LSM2Interrupt set");
    }
    // Again check the serial number to verify i2c working correctly
    lsm.check_id()
        .map_err(|_| "Could not verify serial number")?;
    let has_tap = lsm
        .check_tap() // this will clear the interrupt
        .map_err(|_| "error checking for double tap")?;
    if has_tap.contains(lsm6dsox::TapSource::DoubleTap) {
        log::debug!("Double tap found");
    } else {
        log::warn!("Double tap not found");
    }
    Ok(())
}

fn test_esp<TX, RX, E>(
    mut esp_enable: impl embedded_hal::digital::v2::OutputPin,
    mut tx: TX,
    mut rx: RX,
    clock: &impl Clock,
) -> Result<(), &'static str>
where
    E: core::fmt::Debug,
    TX: embedded_hal::blocking::serial::Write<u8, Error = E>,
    RX: embedded_hal::serial::Read<u8, Error = E>,
{
    log::info!("Test ESP-WROOM-02D (~9 secs)");
    log::warn!("This test is currently likely to fail, especially in debug builds. This is because the sysclk at 2 MHz which is too slow for interrupt-driven uart rx.");
    let mut delay = clock.new_delay();
    delay.delay_ms(100_u32);
    let _ = esp_enable.set_high();
    delay.delay_ms(100_u32);
    log::debug!("esp turned on");
    delay.delay_ms(5000_u32);
    loop {
        // "reset" the uart-rx (errors): read until WouldBlock occurs
        if let Err(nb::Error::WouldBlock) = rx.read() {
            break;
        }
    }
    let tx_bytes = b"AT\r\n";
    log::debug!("Sending Bytes: {:?}", tx_bytes);
    if let Err(e) = tx.bwrite_all(tx_bytes) {
        log::error!("ESP-UART-TX errored {:?}!", e);
    }
    let mut buf = Vec::<_, 256>::new();
    let mut last_error = None;
    let start = clock.try_now().unwrap();
    loop {
        match rx.read() {
            Ok(byte) => {
                let _ = buf.push(byte);
            }
            Err(nb::Error::WouldBlock) => {
                // no data available
            }
            Err(nb::Error::Other(e)) => {
                last_error = Some(e);
            }
        }
        let elapsed = clock.try_now().unwrap() - start;
        if elapsed >= core::time::Duration::from_secs(4) {
            break;
        }
    }
    if let Some(e) = last_error {
        log::error!("ESP-UART-RX errored {:?}!", e);
    }
    log::debug!("ESP: {:?}", buf);
    for pos in 0..buf.len() - 5 {
        let slice = &(*buf)[pos..];
        if slice.starts_with(b"AT\r\n\r\nOK\r\n") {
            return Ok(());
        }
    }
    Err("'OK' not found")
}

fn test_bg77(mut bg77: impl embedded_nal::TcpClientStack) -> Result<(), &'static str> {
    log::info!("Test BG-77 (up to some mins)");
    let mut socket = bg77
        .socket()
        .map_err(|_| "Could not acquire socket handle.")?;
    bg77.connect(&mut socket, ip4_addr_and_port((1, 1, 1, 1), 80))
        .map_err(|_| "Could not connect to TCP socket 1.1.1.1:80")?;
    bg77.close(socket).map_err(|_| "Could not close socket.")?;
    Ok(())
}
fn ip4_addr_and_port(addr: (u8, u8, u8, u8), port: u16) -> embedded_nal::SocketAddr {
    embedded_nal::SocketAddr::V4(embedded_nal::SocketAddrV4::new(
        embedded_nal::Ipv4Addr::new(addr.0, addr.1, addr.2, addr.3),
        port,
    ))
}

fn test_measurement_vbat(vbat: &mut board::vbat_adc::VbatAdc) -> Result<(), &'static str> {
    log::info!("Test measurement battery voltage");
    let vbat_volt = vbat.measure_vbat();
    log::debug!("Battery voltage: {:?} V", vbat_volt);
    Ok(())
}
