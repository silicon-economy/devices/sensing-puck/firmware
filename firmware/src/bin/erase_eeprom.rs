// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This binary initializes the hardware and fully erases the EEPROM, assuming a size of 256 bytes.

#![no_std]
#![no_main]

use cortex_m_rt::*;
use embedded_storage::Storage;

#[entry]
fn main() -> ! {
    // Initialize the RTT as early as possible to avoid probe-run connection timeouts
    rtt_target::rtt_init_print!();

    let puck = board::SensingPuck::take();
    let mut puck = puck.unwrap();

    unsafe { firmware::logger::init_logger(&mut puck.debug_tx) };

    #[cfg(feature = "mbedtls")]
    firmware::allocator::init_allocator();

    log::info!("Erasing EEPROM...\n");

    let ff = [0xFF; 256];
    if puck.eeprom.write(0, &ff).is_err() {
        log::info!("FAILED!\n");
    } else {
        log::info!("Done!\n");
    }

    loop {
        cortex_m::asm::bkpt();
    }
}
