// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This library part of the `firmware` crate implements the parts which are shared between all
//! binaries, i.e. not only the main device firmware but also the additional firmwares like
//! `hardware_test`.
//!
//! The `panic_handler` implemented for the main firmware is included here, too. Like this, it is
//! used for all other binaries as well.

// As of 2023-01-12, the additional binaries (e.g. hardware_test) failed to build because the
// `#[alloc_error_handler]` function and a global memory allocator was required (according to rustc)
// and missing. This requirement comes from the mbedtls dependency via the application dependency
// when the mbedtls feature is activated, which it is by default, although the mbedtls and
// application code should not be linked in the additional binaries. We could not figure out how to
// remove these requirements so we just added the memory allocation code to the other binaries as
// well, which required to extract it into a `lib.rs`.

#![no_std]
#![cfg_attr(feature = "mbedtls", feature(alloc_error_handler))] // for the allocator module
#![cfg_attr(feature = "mbedtls", feature(c_size_t))] // for using core::ffi::c_size_t in calloc

#[cfg(feature = "mbedtls")]
pub mod allocator;
#[cfg(feature = "mbedtls")]
mod calloc;

pub mod logger;
mod panic_handler;
