// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This is the main entry point for the Sensing Puck device firmware. It will instantiate and
//! configure the hardware (i.e. the [`board`] crate) and call [`application::sensing_puck_app`].

#![no_std]
#![no_main]

use cortex_m_rt::*;

mod hardware_impl;

#[entry]
fn main() -> ! {
    // Initialize the RTT as early as possible to avoid probe-run connection timeouts
    rtt_target::rtt_init_print!();

    let puck = board::SensingPuck::take();
    let mut puck = puck.unwrap();

    #[cfg(feature = "mbedtls")]
    firmware::allocator::init_allocator();

    unsafe { firmware::logger::init_logger(&mut puck.debug_tx) };
    log::info!("Logger initialized");

    use hardware_impl as hw;
    application::sensing_puck_app(
        hw::TemperatureHumiditySensorSht3x(puck.temperature_humidity_sensor),
        hw::VbatMeasurementAdc(puck.vbat),
        puck.eeprom,
        hw::LedMatrixDriverIssi(puck.led_matrix),
        quectel_bg77::Bg77ClientStack::new(&mut puck.modem_driver),
        puck.rng,
        puck.clock,
        hw::SleepAndDoubleTap::new(puck.sleep_fn, puck.accelerometer),
    );
}
