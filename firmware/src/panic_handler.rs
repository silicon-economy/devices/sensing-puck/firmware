// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module implements a custom panic handler which allows printing the panic message over RTT
//! and to either restart the MCU (if the `panic-sysreset` feature is enabled) or to print the
//! stack trace over `probe-rs` (if `panic-sysreset`is not enabled).

use core::{
    fmt::Write,
    sync::atomic::{AtomicBool, Ordering},
};

// This panic handler is just the merge of the panic-rtt-target handler (printing the panic message
// works) and the panic-probe handler (printing the stack trace over probe-rs).
// https://github.com/mvirkkunen/rtt-target/blob/master/panic-rtt-target/src/lib.rs
// https://github.com/knurling-rs/defmt/blob/main/firmware/panic-probe/src/lib.rs
#[inline(never)]
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    static PANICKED: AtomicBool = AtomicBool::new(false);

    cortex_m::interrupt::disable();

    // Guard against infinite recursion, just in case.
    if PANICKED.load(Ordering::Relaxed) {
        loop {
            cortex_m::asm::bkpt();
        }
    }

    PANICKED.store(true, Ordering::Relaxed);

    if let Some(mut channel) = unsafe { rtt_target::UpChannel::conjure(0) } {
        channel.set_mode(rtt_target::ChannelMode::BlockIfFull);
        writeln!(channel, "{}", info).ok();
    }

    // Trigger a `HardFault` via `udf` instruction.

    // If `UsageFault` is enabled, we disable that first, since otherwise `udf` will cause that
    // exception instead of `HardFault`.
    const SHCSR: *mut u32 = 0xE000ED24usize as _;
    const USGFAULTENA: usize = 18;

    unsafe {
        let mut shcsr = core::ptr::read_volatile(SHCSR);
        shcsr &= !(1 << USGFAULTENA);
        core::ptr::write_volatile(SHCSR, shcsr);
    }

    // The undefined instruction triggers the hardfault which triggers printing the stack trace
    // over probe-rs.
    #[cfg(not(feature = "panic-sysreset"))]
    cortex_m::asm::udf();
    #[cfg(feature = "panic-sysreset")]
    {
        for _ in 0..1600000 {
            cortex_m::asm::nop();
        }
        cortex_m::peripheral::SCB::sys_reset();
    }
}
