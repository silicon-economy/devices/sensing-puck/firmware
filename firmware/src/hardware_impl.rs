// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Implementations of the hardware abstraction traits defined by the application

use application::hardware_abstraction::{
    self, Frame, FrameId, Humidity, LedMatrixDriver, SleepFn, Temperature,
    TemperatureHumiditySensor, Voltage, VoltageMeasurement,
};
use board::{hal, issi_is31fl3731, sensirion_rht, WakeupReason};

pub struct SleepAndDoubleTap<'a> {
    sleep_fn: fn(Option<core::time::Duration>) -> enumflags2::BitFlags<WakeupReason>,
    lsm: board::Lsm6dsox<'a>,
}

impl<'a> SleepAndDoubleTap<'a> {
    pub fn new(
        sleep_fn: fn(Option<core::time::Duration>) -> enumflags2::BitFlags<WakeupReason>,
        mut lsm: board::Lsm6dsox<'a>,
    ) -> Self {
        // Set up the Accelerometer Double Tap
        lsm.setup().unwrap();
        lsm.setup_tap_detection(
            board::lsm6dsox::TapCfg {
                en_x_y_z: (true, true, true),
                lir: false,
                // if lir is set to true, check_double_tap() has to be called to reset the INT pin on every interrupt
            },
            board::lsm6dsox::TapMode::SingleAndDouble,
            Some(board::lsm6dsox::InterruptLine::INT2),
        )
        .unwrap();
        Self { sleep_fn, lsm }
    }
}

impl SleepFn for SleepAndDoubleTap<'_> {
    fn sleep(&mut self, duration: Option<core::time::Duration>) -> bool {
        let wakeup_reasons = (self.sleep_fn)(duration);
        if wakeup_reasons.contains(board::WakeupReason::LSM2Interrupt) {
            if let Ok(interrupt_src) = self.lsm.check_interrupt_sources() {
                return interrupt_src.contains(board::lsm6dsox::InterruptCause::DoubleTap);
            }
        }
        false
    }
}

pub struct TemperatureHumiditySensorSht3x<'a>(pub board::Sht3x<'a>);

impl TemperatureHumiditySensor for TemperatureHumiditySensorSht3x<'_> {
    type Error = sensirion_rht::Error<hal::i2c::Error>;
    fn read_serial_number(&mut self) -> Result<u32, Self::Error> {
        self.0.read_serial_number()
    }
    fn single_shot(&mut self) -> Result<(Temperature, Humidity), Self::Error> {
        self.0.single_shot(sensirion_rht::Repeatability::Medium)
    }
}

pub struct VbatMeasurementAdc<'a>(pub board::vbat_adc::VbatAdc<'a>);

impl VoltageMeasurement for VbatMeasurementAdc<'_> {
    type Error = nb::Error<core::convert::Infallible>;
    fn measurement_vbat(&mut self) -> Result<Voltage, Self::Error> {
        Ok(self.0.measure_vbat())
    }
}

pub struct LedMatrixDriverIssi(pub board::LedMatrix);

fn fid2fid(frame_id: FrameId) -> issi_is31fl3731::FrameId {
    match frame_id {
        hardware_abstraction::FrameId::One => issi_is31fl3731::FrameId::One,
        hardware_abstraction::FrameId::Two => issi_is31fl3731::FrameId::Two,
        hardware_abstraction::FrameId::Three => issi_is31fl3731::FrameId::Three,
        hardware_abstraction::FrameId::Four => issi_is31fl3731::FrameId::Four,
        hardware_abstraction::FrameId::Five => issi_is31fl3731::FrameId::Five,
        hardware_abstraction::FrameId::Six => issi_is31fl3731::FrameId::Six,
        hardware_abstraction::FrameId::Seven => issi_is31fl3731::FrameId::Seven,
        hardware_abstraction::FrameId::Eight => issi_is31fl3731::FrameId::Eight,
    }
}

use core::convert::Infallible;

impl LedMatrixDriver for LedMatrixDriverIssi {
    type Error = issi_is31fl3731::Error<Infallible, Infallible, hal::i2c::Error>;

    /// Power the driver (chip) on (true) or off (false)
    fn power(&mut self, turn_on: bool) -> Result<(), Self::Error> {
        self.0.power_on(turn_on)
    }

    /// Writes an array of PWM values to the desired frame
    ///
    /// The frame will be selected by `frame_id`. The driver should set each control and PWM pin
    /// according to the data in the `frame` array. For used LEDs, the control pin will be set to
    /// one and the PWM values will be set to the data in the `frame` array.
    fn write_frame(&mut self, frame_id: FrameId, frame: Frame) -> Result<(), Self::Error> {
        let frame = issi_is31fl3731::Frame {
            pixels: frame.pixels,
        };
        self.0.write_frame(fid2fid(frame_id), frame)
    }

    /// Displays the given frame with `frame_id` to the LED matrix
    fn show_frame(&mut self, frame_id: FrameId) -> Result<(), Self::Error> {
        self.0.show_frame(fid2fid(frame_id))
    }

    /// Enables the AutoPlayAnimation feature of the LED driver
    ///
    /// The AutoPlayAnimation cycles from the `first_frame` to the `last_frame` with the
    /// `frame_delay_time` between each frame.  On the actual sensing puck hardware, the delay
    /// time will be discretized to multiples of 11ms with a maximum of 704ms (6 bit resolution).
    fn play_animation(
        &mut self,
        first_frame: FrameId,
        last_frame: FrameId,
        repeat: bool,
        inter_frame_delay: core::time::Duration,
    ) -> Result<(), Self::Error> {
        let loops = if repeat {
            issi_is31fl3731::AnimationLoops::Endless
        } else {
            issi_is31fl3731::AnimationLoops::One
        };
        self.0.play_animation(
            fid2fid(first_frame),
            fid2fid(last_frame),
            loops,
            issi_is31fl3731::Delay::new(inter_frame_delay),
        )
    }
}
