// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Implements a simple [`Logger`](log::Log) printing on a UART and RTT at the same time which may
//! be set up with [`init_logger`].

/// Setting up a logger requires access to global static resources
static mut DEBUG_UART: Option<&mut dyn core::fmt::Write> = None;

/// [`log::Log`] type which uses the `rprint!` macro and the global `DEBUG_UART` instance
struct RttAndUartLogger {
    /// Static logging configuration. Can be used to define custom log levels for specific domains.
    target_levels: &'static [(&'static str, log::LevelFilter)],
}

static LOGGER: RttAndUartLogger = RttAndUartLogger {
    target_levels: &[
        // Configure max log level for module called "CONFIGURABLE"
        // ("CONFIGURABLE", log::LevelFilter::Trace),
    ],
};

impl log::Log for RttAndUartLogger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        let target = metadata.target();
        let level = metadata.level();
        let target_max_level = self
            .target_levels
            .iter()
            .find(|(name, _level)| target == *name)
            .map(|(_name, level)| *level)
            .unwrap_or(log::LevelFilter::Trace);
        level <= target_max_level
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            let target = record.target();
            let level = record.level();
            let args = record.args();
            let uart = unsafe { DEBUG_UART.as_mut().unwrap() };
            rtt_target::rprintln!("[{}] {:<5} {}", target, level, args);
            let _ = writeln!(uart, "[{}] {:<5} {}", target, level, args);
        }
    }

    fn flush(&self) {}
}

/// Sets up the global logger using the provided UART
///
/// # Safety
///
/// This function is unsafe because it sets the UART reference as a global, _static_ logger
/// instance. Since the UART is officially not static (it is only valid after it has been set up
/// and until it has been deinitialized), we need to lie about the UART's lifetime. To keep this
/// safe, a few things must be ensured:
/// 1. The UART instance may not move in memory afterwards (i.e. call this from your top-level
///    main).
/// 2. The UART may not be de-initialized.
/// 3. The UART may not be used from a different thread.
///
/// Additionally, you _must_ call [`rtt_target::rtt_init_print!`] before using the logger. This is
/// not done here because it should be done as early as possible to prevent `probe-run`
/// connection timeouts (if `probe-run` shall be used at the host side of the RTT channel).
/// Initializing the RTT after the board setup has been proven to be too late, but `init_logger`
/// requires the UART which is only available after board setup. Thus, you have to call
/// `rtt_init_print!` manually before initializing the board.
pub unsafe fn init_logger<UART: core::fmt::Write + 'static>(uart: &mut UART) {
    // Lie about the UART's lifetime
    let uart: &'static mut UART = core::mem::transmute(uart);
    // Set up Logger
    DEBUG_UART = Some(uart);
    log::set_logger(&LOGGER).unwrap();
    log::set_max_level(log::LevelFilter::Trace);
}
