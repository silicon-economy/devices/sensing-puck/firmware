# Sensing Puck Firmware

This is the Firmware Project of the Sensing Puck Device.
It is part of the "Modulare IoT Devices" Project of "Silicon Economy".

The current Firmware Version is compatible with version V3.0 of the SensingPuck Hardware.

## Configuration
The Sensing Puck firmware can be configured via environment variables which are incorporated into
the firmware at build time. Most of these _must_ be set for the device to function properly
(although the build won't fail if they are not set).
- `SENSING_PUCK_DEVICE_ID` The device ID configured into the firmware (currently `u32`). This is
used if no valid configuration is found in the EEPROM, or: this is _ignored_ if a valid
configuration is found.
- `SENSING_PUCK_PSK` and `SENSING_PUCK_PSK_IDENTITY` These set the configuration parameters for the
TLS connection (see `mbedtls` feature below) which currently uses the TLS_PSK_WITH_AES_128_CCM_8
cipher.

## Features
- `panic-sysreset` enables a code snippet in the panic handler which resets the MCU after a short
delay. If this is not enabled, the MCU stops after a panic so the device must be power-cycled to
restart but this can be desired because this prints the stack-trace if the flasher is still
connected to the device (with `probe-rs`).
- `use-defmt` additionally enables logging with `defmt`, logging over UART is still enabled then.
- `mbedtls` uses the mbedtls library to open an encrypted TLS connection to the IoT Broker. At the
time of writing, a nightly compiler is required because
  1. we use the [`alloc_error_handler`](https://github.com/rust-lang/rust/issues/51540)
attribute/feature which is required when using the `alloc` crate in `no_std` environments
  2. `alloc-cortex-m` uses the `linked-list-allocator` with the `const_mut_refs` feature which is
unstable.

The `mbedtls` feature is enabled automatically (it is a `default` feature) to emphasize that
secured communication should be the default.

## Quick Links
- [main documentation](https://git.openlogisticsfoundation.org/silicon-economy/devices/sensing-puck/documentation/-/blob/main/documentation/index.adoc)
- [web frontend](https://sensing-puck.public.apps.sele.iml.fraunhofer.de/)
- [rustdoc](https://silicon-economy.pages.fraunhofer.de/services/modular-iot-devices/sensingpuck/sensing-puck-firmware/firmware/)

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contacts

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>

