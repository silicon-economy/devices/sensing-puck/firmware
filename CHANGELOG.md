# Sensing Puck Firmware - Changelog

## Current Development

## v1.3.0 - 2023-04-06

* Upgrade `stm32l4xx-hal`
* Add first implementation for the simulator
* Cleanup config module
* Improve CI/CD with `cargo-udeps`, `cspell` and `cargo-deny` (the latter makes `check-sele-licenses` obsolete)
* Add TLS security based on mbedtls
* Introduce workspace inheritance
* Use OLF dependencies
* Battery voltage measurement
* Minor cleanups and improvements

## v1.2.0

* Sleep with increased resolution to prevent busy waiting to meet high accuracy for energy efficiency
  * Fix sleeping longer than one max RTC duration
* Use new `sensirion_rht` library version (main change: traits are either external or defined in the application, not in the driver anymore)
* Move `firmware/src/bin/firmware.rs` to `firmware/src/main.rs`
* Use newer `quectel-bg77` driver library which supports multiple open sockets and implements RAII to prevent resource leaks
* Change EEPROM access scheme: Alongside the serialized configuration data, write the git version hash and a crc32. The hash ensures that the data is invalidated when the code changes.
* Add the measurement of the battery voltage.
* Add uart_bypass_to_modem binary for testing and modem-driver development purposes
* Cargo now warns for dead code and some unused code was removed
* Remove all leftover dependencies between `board` and `application` by introducing appropriate traits in `hardware_abstraction`
* Add optional TLS-based communication security


## v1.1.0

* Refactorings
  * Crates structure
    * Different binaries for hardware_test and firmware instead of features
    * Board Support Crate which could support different hardware versions in the future
    * Pseudo-binary erase_eeprom which can be flashed to erase the eeprom once (mostly for development purposes)
  * Sleep management in its own module for separation
* Implement the Timers timebase with proper atomicity and 64 bits instead of 32 bits
* Firmware configurability via environment variables: Currently only `SENSING_PUCK_DEVICE_ID` supported
* Read Modem UART with new HAL DMA
* Implement rotation of the LED Matrix by 180 degrees
* Various fixes to the low power mode of the device
* Improvements to the accelerometer driver
* Fixed issue that the alarm_flag was changed without updating the EEPROM


## v1.0.0

* Loads last config from EEPROM on startup
  * Also loads internal alarm state
* If no active job is available
  * Double Tap to show ID
  * Double Tap while ID is shown to fetch job data
* If there is an active job available
  * Measures temperature [°C] and humidity [%rH] regularly according to job config
    * Only the "storage" interval is used due to a lack of motion recognition
  * Measurements are transmitted to the backend if enough data (according to job config) was measured
    * Amount of measurements is limited to 100
    * On failure, the measurements will be transmitted again after the next measurement cycle
    * If 100 measurements are reached, no new data will be collected
  * If either temperature or humidity are outside the range configured by the job data
    * An internal alarm state is set
      * Can be reset via backend and is reset when a new job arrives
      * Will be indicated by flashing "!!!" before showing the temperature and flashing "!!!" combined with a transmission animation during the transmission screen
  * Double Tap to take a measurement and display the temperature
  * Double Tap while showing the temperature will display the ID
  * Double Tap while showing the ID will transmit all available data to the backend
* Communication done/result is shown on the display
  * Animated checkmark for success
  * "Err" for error
* In any case, the display will turn off automatically after some time
  * Individual times for id, temperature and alarm are configurable bia backend
    * This feature may be removed in the future
