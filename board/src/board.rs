// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module combines the different board features into the single board support type
//! [`SensingPuck`]

// Defining structs for the init methods would be even worse
#![allow(clippy::too_many_arguments)]

pub use hal::stm32 as pac;
pub use stm32l4xx_hal as hal;

use crate::{circ_buffer_reader, sleep, uptime, vbat_adc::VbatAdc};
use core::{
    cell::RefCell, convert::Infallible, ops::DerefMut, sync::atomic::Ordering, time::Duration,
};
use cortex_m::{interrupt::Mutex, singleton};
use cortex_m_rt::exception;
use embedded_timers::clock::Clock;
use enumflags2::BitFlags;
use hal::{adc::Adc, dma::CircReadDma, i2c, interrupt, prelude::*, serial::Serial};
use inverted_pin::InvertedPin;

// statics:

/// Clock providing timers for the drivers and sleep function
static MILLISECOND_CLOCK: uptime::MilliSecondClock = uptime::MilliSecondClock {};

/// Gpio Pin for the SHTs interrupt line
static INT_SHT: Mutex<RefCell<Option<hal::gpio::PB3<hal::gpio::Input<hal::gpio::PullUp>>>>> =
    Mutex::new(RefCell::new(None));

/// Gpio Pin for the LSM Interrupt line 1
static INT1_LSM: Mutex<RefCell<Option<hal::gpio::PB4<hal::gpio::Input<hal::gpio::PullUp>>>>> =
    Mutex::new(RefCell::new(None));

/// Gpio Pin for the LSM Interrupt line 2
static INT2_LSM: Mutex<RefCell<Option<hal::gpio::PB5<hal::gpio::Input<hal::gpio::PullUp>>>>> =
    Mutex::new(RefCell::new(None));

static DEBUG_RX: Mutex<RefCell<Option<hal::serial::Rx<pac::USART2>>>> =
    Mutex::new(RefCell::new(None));
static mut DEBUG_RX_QUEUE: heapless::spsc::Queue<u8, 32> = heapless::spsc::Queue::new();

// type shortcuts:
pub type MilliSecondClockDelay<'a> = embedded_timers::delay::Delay<'a, uptime::MilliSecondClock>;

pub type DebugUartTx = hal::serial::Tx<hal::stm32::USART2>;

/// Const width of the LED Matrix
pub const LED_MATRIX_WIDTH: usize = 13;
/// Const height of the LED Matrix
pub const LED_MATRIX_HEIGHT: usize = 7;
/// LED Matrix type
pub type LedMatrix = issi_is31fl3731::LedMatrix<
    hal::gpio::PA5<hal::gpio::Output<hal::gpio::PushPull>>, // Power Pin
    hal::gpio::PA6<hal::gpio::Output<hal::gpio::PushPull>>, // Shutdown Pin
    i2c::I2c<
        pac::I2C1,
        (
            hal::gpio::PB8<hal::gpio::Alternate<hal::gpio::OpenDrain, 4>>, // SCL
            hal::gpio::PB9<hal::gpio::Alternate<hal::gpio::OpenDrain, 4>>, // SDA
        ),
    >,
    { LED_MATRIX_WIDTH },
    { LED_MATRIX_HEIGHT },
>;
pub type LedMatrixError = issi_is31fl3731::Error<Infallible, Infallible, i2c::Error>;

pub type SharedI2C4 = i2c::I2c<
    pac::I2C4,
    (
        hal::gpio::PB6<hal::gpio::Alternate<hal::gpio::OpenDrain, 5>>, // SCL
        hal::gpio::PB7<hal::gpio::Alternate<hal::gpio::OpenDrain, 5>>, // SDA
    ),
>;

pub type Sht3x<'a> = sensirion_rht::Device<
    shared_bus::I2cProxy<'a, shared_bus::CortexMMutex<SharedI2C4>>,
    MilliSecondClockDelay<'a>,
    sensirion_rht::kind::SHT3x,
>;

pub type Lsm6dsox<'a> = lsm6dsox::Lsm6dsox<
    shared_bus::I2cProxy<'a, shared_bus::CortexMMutex<SharedI2C4>>,
    MilliSecondClockDelay<'a>,
>;

pub type Eeprom<'a> = eeprom24x::Storage<
    i2c::I2c<
        pac::I2C2,
        (
            hal::gpio::PB13<hal::gpio::Alternate<hal::gpio::OpenDrain, 4>>, // SCL
            hal::gpio::PB14<hal::gpio::Alternate<hal::gpio::OpenDrain, 4>>, // SDA
        ),
    >,
    eeprom24x::page_size::B16,
    eeprom24x::addr_size::OneByte,
    embedded_timers::Timer<'a, uptime::MilliSecondClock>,
    //hal::gpio::PB12<hal::gpio::Output<hal::gpio::OpenDrain>>, // Write Protection
>;

type ModemDriver<'a> = core::cell::RefCell<
    quectel_bg77::Bg77Driver<
        hal::gpio::PB1<hal::gpio::Output<hal::gpio::PushPull>>, // Enable
        InvertedPin<hal::gpio::PA8<hal::gpio::Output<hal::gpio::PushPull>>>, // reset
        InvertedPin<hal::gpio::PB15<hal::gpio::Output<hal::gpio::PushPull>>>, // PowerKeyN
        hal::serial::Tx<pac::USART1>,                           // TX
        circ_buffer_reader::CircBufferReader,
        hal::serial::Error,
        embedded_timers::Timer<'a, uptime::MilliSecondClock>,
    >,
>;

/// The single board support type which contains all peripheral driver instances and further
/// hardware-related functionality, e.g. timing, sleeping or internal voltage measurement
pub struct SensingPuck<'a> {
    /// TX side of the debug UART (baudrate 115200). This is a [`stm32l4xx_hal::serial::Tx`] which
    /// which implements [`core::fmt::Write`] for formatted output, e.g. via the [`writeln!`] macro.
    pub debug_tx: DebugUartTx,
    /// RX side of the debug UART (baudrate 115200). This is the read side of a
    /// [`heapless::spsc::Queue`] which stores up to 32 bytes of input data which is read from the
    /// UART in the interrupt handler where it is written to the `Queue`. Interrupt-based RX may
    /// overflow depending on the optimization level.
    pub debug_rx: heapless::spsc::Consumer<'static, u8, 32>,
    /// LED matrix driver which implements [`hardware_abstraction::LedMatrixDriver`].
    pub led_matrix: LedMatrix,
    /// Temperature and relative humidity sensor which implements
    /// [`hardware_abstraction::TemperatureHumiditySensor`].
    pub temperature_humidity_sensor: Sht3x<'a>,
    /// [`lsm6dsox::Lsm6dsox`] accelerometer. To use (double) tap detection, call `setup` and
    /// `setup_tap_detection` (see `hardware_test` for usage example).
    pub accelerometer: Lsm6dsox<'a>,
    /// [`eeprom24x::Storage`] which implements
    /// [`embedded_storage::Storage`](https://docs.rs/embedded-storage/latest/embedded_storage/trait.Storage.html).
    pub eeprom: Eeprom<'a>,
    /// [`quectel_bg77::Bg77Driver`] wrapped in a `RefCell`. A mutable reference of this `RefCell`
    /// can be used to instantiate [`quectel_bg77::Bg77ClientStack`] which in turn implements
    /// some [`embedded_nal`](https://docs.rs/embedded-nal/latest/embedded_nal/) traits.
    pub modem_driver: ModemDriver<'a>,
    /// Battery voltage measurement type which implements
    /// [`hardware_abstraction::VoltageMeasurement`].
    pub vbat: VbatAdc<'a>,
    /// Random number generator which implements [`embedded_hal::blocking::rng::Read`].
    pub rng: hal::rng::Rng,
    /// Clock running with millisecond granularity. The underlying counter type is `u64` so that
    /// this clock will not wrap around. It is a `'static` reference so it can be copied/shared
    /// freely.
    pub clock: &'static uptime::MilliSecondClock,
    /// Esp32 raw hardware access.
    pub esp: Esp,
    /// Sleep function which takes an optional [`Duration`] when to wake up. Independent of whether
    /// a duration is supplied, the MCU wakes up if one of the configured interrupts occurs.  After
    /// waking up, all [`WakeupReason`](sleep::WakeupReason)s which have occurred
    /// are returned (may be none if the MCU wakes up after the timeout).
    pub sleep_fn: fn(Option<Duration>) -> BitFlags<sleep::WakeupReason>,
}

/// Esp32 hardware access type
pub struct Esp {
    /// TX side of the UART which connects the MCU to the Esp32
    pub tx: hal::serial::Tx<pac::UART4>,
    /// RX side of the UART which connects the MCU to the Esp32
    pub rx: hal::serial::Rx<pac::UART4>,
    /// Pin which controls a MOSFET to power on/off the Esp32
    pub enable: hal::gpio::PA4<hal::gpio::Output<hal::gpio::PushPull>>,
}

// Clock initialization, defined as a macro to use it in the (hacky) sleep function as well.
// "Returns" a tuple (systick_interrupt_reload_value, clocks)
macro_rules! init_clocks {
    ($rcc:expr, $flash:expr, $pwr:expr) => {{
        // Currently, the upstream stm32l4xx-hal forces us to either
        // a) use the pll (min 8MHz) or
        // b) use hsi (16MHz) as sysclk source
        // Therefore, we have done a patch in our fork which sets MSI as the fallback clock
        // source. To use MSI as sysclk input, do not specify the sysclk frequency (this will
        // try to guess a meaningful pll configuration and use the pll as sysclk input) but
        // just specify the MSI's frequency instead.
        let msi_range = hal::rcc::MsiFreq::RANGE2M;
        let clocks = $rcc
            .cfgr
            .msi(msi_range)
            .lse(
                hal::rcc::CrystalBypass::Disable,
                hal::rcc::ClockSecuritySystem::Disable,
            ) // use 32.767 kHz Crystal
            .hsi48(true)
            .freeze(&mut $flash.acr, &mut $pwr);
        // convenience calculation so that the reload value for the systick
        // interrupt does not need to be adjusted manually
        let msi_hertz = match msi_range {
            hal::rcc::MsiFreq::RANGE100K => 100_000,
            hal::rcc::MsiFreq::RANGE200K => 200_000,
            hal::rcc::MsiFreq::RANGE400K => 400_000,
            hal::rcc::MsiFreq::RANGE800K => 800_000,
            hal::rcc::MsiFreq::RANGE1M => 1_000_000,
            hal::rcc::MsiFreq::RANGE2M => 2_000_000,
            hal::rcc::MsiFreq::RANGE4M => 4_000_000,
            hal::rcc::MsiFreq::RANGE8M => 8_000_000,
            hal::rcc::MsiFreq::RANGE16M => 16_000_000,
            hal::rcc::MsiFreq::RANGE24M => 24_000_000,
            hal::rcc::MsiFreq::RANGE32M => 32_000_000,
            hal::rcc::MsiFreq::RANGE48M => 48_000_000,
        };
        (msi_hertz / 1000, clocks)
    }};
}
pub(crate) use init_clocks;

/// M24C02 Eeprom config (i2c2 + enable pin)
///
/// pb13 scl, pb14 sda, pb12 write protect
#[inline(always)]
fn new_eeprom<'a>(
    scl: hal::gpio::PB13<hal::gpio::Analog>,
    sda: hal::gpio::PB14<hal::gpio::Analog>,
    wp: hal::gpio::PB12<hal::gpio::Analog>,
    gpiob_moder: &mut hal::gpio::MODER<'B'>,
    gpiob_otyper: &mut hal::gpio::OTYPER<'B'>,
    gpiob_pupdr: &mut hal::gpio::PUPDR<'B'>,
    gpiob_afrh: &mut hal::gpio::Afr<hal::gpio::H8, 'B'>,
    rcc_apb1r1: &mut hal::rcc::APB1R1,
    clocks: hal::rcc::Clocks,
    i2c2: pac::I2C2,
) -> Eeprom<'a> {
    let mut scl = scl.into_alternate_open_drain(gpiob_moder, gpiob_otyper, gpiob_afrh);
    scl.internal_pull_up(gpiob_pupdr, true);

    let mut sda = sda.into_alternate_open_drain(gpiob_moder, gpiob_otyper, gpiob_afrh);
    sda.internal_pull_up(gpiob_pupdr, true);

    let conf = i2c::Config::new(hal::time::Hertz::kHz(100), clocks);
    let i2c2 = i2c::I2c::i2c2(i2c2, (scl, sda), conf, rcc_apb1r1);

    let mut eeprom_wp_pin = wp.into_open_drain_output(gpiob_moder, gpiob_otyper);
    // TODO This pin is currently unused. Moreover, it has not worked as expected in the
    // past, i.e. it was used in a wrong way and it worked nevertheless, see
    // https://oe160.iml.fraunhofer.de/jira/browse/SE-5472
    eeprom_wp_pin.internal_pull_up(gpiob_pupdr, false); // Turn on Write Protection by default

    let eeprom = eeprom24x::Eeprom24x::new_m24x02(i2c2, eeprom24x::SlaveAddr::Default);
    let storage = eeprom24x::Storage::new(eeprom, MILLISECOND_CLOCK.new_timer());
    storage
}

///
/// LED Matrix Config (i2c1)
///
///pb8 scl,  pb9 sda
/// pa5 leds enable, pa7 leds shutdown
#[inline(always)]
fn new_led_matrix(
    scl: hal::gpio::PB8<hal::gpio::Analog>,
    sda: hal::gpio::PB9<hal::gpio::Analog>,
    leds_enable: hal::gpio::PA5<hal::gpio::Analog>,
    leds_shutdown: hal::gpio::PA6<hal::gpio::Analog>,
    gpiob_moder: &mut hal::gpio::MODER<'B'>,
    gpiob_otyper: &mut hal::gpio::OTYPER<'B'>,
    gpiob_pupdr: &mut hal::gpio::PUPDR<'B'>,
    gpiob_afrh: &mut hal::gpio::Afr<hal::gpio::H8, 'B'>,
    gpioa_moder: &mut hal::gpio::MODER<'A'>,
    gpioa_otyper: &mut hal::gpio::OTYPER<'A'>,
    rcc_apb1r1: &mut hal::rcc::APB1R1,
    clocks: hal::rcc::Clocks,
    i2c1: pac::I2C1,
) -> LedMatrix {
    let mut scl = scl.into_alternate_open_drain(gpiob_moder, gpiob_otyper, gpiob_afrh);
    scl.internal_pull_up(gpiob_pupdr, true);

    let mut sda = sda.into_alternate_open_drain(gpiob_moder, gpiob_otyper, gpiob_afrh);
    sda.internal_pull_up(gpiob_pupdr, true);

    let conf = i2c::Config::new(hal::time::Hertz::kHz(100), clocks);
    let i2c1 = i2c::I2c::i2c1(i2c1, (scl, sda), conf, rcc_apb1r1);

    // led matrix GPIOs config
    let leds_enable = leds_enable.into_push_pull_output(gpioa_moder, gpioa_otyper);
    let leds_shutdown = leds_shutdown.into_push_pull_output(gpioa_moder, gpioa_otyper);

    let led_matrix: issi_is31fl3731::LedMatrix<_, _, _, 13, 7> = issi_is31fl3731::LedMatrix::new(
        issi_is31fl3731::Addr::GND,
        issi_is31fl3731::PowerActive::High(leds_enable),
        leds_shutdown,
        i2c1,
    );

    led_matrix
}

///
/// SHT & LSM6DSOX Sensors Config (i2c4)
///
/// pb6 scl, pb7 sda
#[inline(always)]
fn init_temperature_humidity_accelerometer<'a>(
    scl: hal::gpio::PB6<hal::gpio::Analog>,
    sda: hal::gpio::PB7<hal::gpio::Analog>,
    gpiob_moder: &mut hal::gpio::MODER<'B'>,
    gpiob_otyper: &mut hal::gpio::OTYPER<'B'>,
    gpiob_pupdr: &mut hal::gpio::PUPDR<'B'>,
    gpiob_afrl: &mut hal::gpio::Afr<hal::gpio::L8, 'B'>,
    rcc_apb1r1: &mut hal::rcc::APB1R2,
    clocks: hal::rcc::Clocks,
    i2c4: pac::I2C4,
) -> (Sht3x<'a>, Lsm6dsox<'a>) {
    let mut scl = scl.into_alternate_open_drain(gpiob_moder, gpiob_otyper, gpiob_afrl);
    scl.internal_pull_up(gpiob_pupdr, true);

    let mut sda = sda.into_alternate_open_drain(gpiob_moder, gpiob_otyper, gpiob_afrl);
    sda.internal_pull_up(gpiob_pupdr, true);

    let conf = i2c::Config::new(hal::time::Hertz::kHz(100), clocks);
    let i2c4 = i2c::I2c::i2c4(i2c4, (scl, sda), conf, rcc_apb1r1);

    // Instantiate a shared, mutexed bus with static lifetime
    let shared_i2c4 = shared_bus::new_cortexm!(SharedI2C4 = i2c4).unwrap();

    let sht3x = sensirion_rht::Device::new_sht3x(
        sensirion_rht::Addr::A,
        shared_i2c4.acquire_i2c(),
        MILLISECOND_CLOCK.new_delay(),
    );

    let accelerometer = lsm6dsox::Lsm6dsox::new(
        shared_i2c4.acquire_i2c(),
        lsm6dsox::SlaveAddress::Low,
        MILLISECOND_CLOCK.new_delay(),
    );

    (sht3x, accelerometer)
}

///
/// BG77 Config (uart1 + output-pins)
///
#[inline(always)]
fn init_modem<'a>(
    txpin: hal::gpio::PA9<hal::gpio::Analog>,
    rxpin: hal::gpio::PA10<hal::gpio::Analog>,
    bg77_enable: hal::gpio::PB1<hal::gpio::Analog>,
    bg77_reset: hal::gpio::PA8<hal::gpio::Analog>,
    bg77_pwrkey_n: hal::gpio::PB15<hal::gpio::Analog>,
    gpioa_moder: &mut hal::gpio::MODER<'A'>,
    gpioa_otyper: &mut hal::gpio::OTYPER<'A'>,
    gpioa_afrh: &mut hal::gpio::Afr<hal::gpio::H8, 'A'>,
    gpiob_moder: &mut hal::gpio::MODER<'B'>,
    gpiob_otyper: &mut hal::gpio::OTYPER<'B'>,
    rcc_apb2: &mut hal::rcc::APB2,
    clocks: hal::rcc::Clocks,
    usart1: pac::USART1,
    dma1_channel5: hal::dma::dma1::C5,
) -> ModemDriver<'a> {
    let txpin = txpin.into_alternate_push_pull(gpioa_moder, gpioa_otyper, gpioa_afrh);
    let rxpin = rxpin.into_alternate_push_pull(gpioa_moder, gpioa_otyper, gpioa_afrh);
    let serial = Serial::usart1(
        usart1,
        (txpin, rxpin),
        hal::serial::Config::default().baudrate(115_200.bps()),
        clocks,
        rcc_apb2,
    );
    let (bg77_tx, bg77_rx) = serial.split();
    let bg77_rxbuf = singleton!(: [u8; 2048] = [0; 2048]).unwrap();
    let bg77_rx_circ_buffer = bg77_rx.with_dma(dma1_channel5).circ_read(bg77_rxbuf);
    let bg77_rx = circ_buffer_reader::CircBufferReader(bg77_rx_circ_buffer);
    // bg77 gpio config
    let bg77_enable = bg77_enable.into_push_pull_output(gpiob_moder, gpiob_otyper);
    let bg77_reset_n =
        InvertedPin::new(bg77_reset.into_push_pull_output(gpioa_moder, gpioa_otyper));
    let bg77_pwrkey =
        InvertedPin::new(bg77_pwrkey_n.into_push_pull_output(gpiob_moder, gpiob_otyper));
    // Bg77 setup
    let at_timer = MILLISECOND_CLOCK.new_timer();
    let modem_timer = MILLISECOND_CLOCK.new_timer();
    let bg77_hal = quectel_bg77::Bg77Hal {
        pin_enable: bg77_enable,
        pin_reset_n: bg77_reset_n,
        pin_pwrkey: bg77_pwrkey,
        tx: bg77_tx,
        rx: bg77_rx,
        at_timer,
        modem_timer,
    };
    let radio_config = quectel_bg77::RadioConfig::OnlyNbiot {
        bands: quectel_bg77::NbiotBand::B8.into(),
    };
    quectel_bg77::Bg77Driver::new(
        bg77_hal,
        radio_config,
        Some("iot.1nce.net"),
        Some("26201"),
        core::time::Duration::from_secs(60),
        core::time::Duration::from_millis(500),
        core::time::Duration::from_millis(20),
    )
}

///
/// ESP Config (uart4 + enable-pin)
///
#[inline(always)]
fn init_esp(
    txpin: hal::gpio::PA0<hal::gpio::Analog>,
    rxpin: hal::gpio::PA1<hal::gpio::Analog>,
    esp_enable: hal::gpio::PA4<hal::gpio::Analog>,
    gpioa_moder: &mut hal::gpio::MODER<'A'>,
    gpioa_otyper: &mut hal::gpio::OTYPER<'A'>,
    gpioa_afrl: &mut hal::gpio::Afr<hal::gpio::L8, 'A'>,
    rcc_apb1r1: &mut hal::rcc::APB1R1,
    clocks: hal::rcc::Clocks,
    uart4: pac::UART4,
) -> Esp {
    let txpin = txpin.into_alternate_push_pull(gpioa_moder, gpioa_otyper, gpioa_afrl);
    let rxpin = rxpin.into_alternate_push_pull(gpioa_moder, gpioa_otyper, gpioa_afrl);
    let serial = Serial::uart4(
        uart4,
        (txpin, rxpin),
        hal::serial::Config::default().baudrate(115_200.bps()),
        clocks,
        rcc_apb1r1,
    );
    let (esp_tx, esp_rx) = serial.split();

    // esp enable gpio config
    let esp_enable = esp_enable.into_push_pull_output(gpioa_moder, gpioa_otyper);
    Esp {
        tx: esp_tx,
        rx: esp_rx,
        enable: esp_enable,
    }
}

#[inline(always)]
fn init_vbat_measurement(
    adc: stm32l4xx_hal::stm32::ADC1,
    adc_common: stm32l4xx_hal::stm32::ADC_COMMON,
    ahb2: &mut stm32l4xx_hal::rcc::AHB2,
    ccipr: &mut stm32l4xx_hal::rcc::CCIPR,
) -> VbatAdc<'static> {
    let mut delay = uptime::MilliSecondClock {}.new_delay();
    let adc_common = hal::adc::AdcCommon::new(adc_common, ahb2);
    let adc1 = Adc::adc1(adc, adc_common, ccipr, &mut delay);
    VbatAdc { adc: adc1, delay }
}

///
/// debug-uart (usart2) config
///
#[inline(always)]
fn init_debug_uart(
    txpin: hal::gpio::PA2<hal::gpio::Analog>,
    rxpin: hal::gpio::PA3<hal::gpio::Analog>,
    gpioa_moder: &mut hal::gpio::MODER<'A'>,
    gpioa_otyper: &mut hal::gpio::OTYPER<'A'>,
    gpioa_afrl: &mut hal::gpio::Afr<hal::gpio::L8, 'A'>,
    rcc_apb1r1: &mut hal::rcc::APB1R1,
    clocks: hal::rcc::Clocks,
    usart2: pac::USART2,
) -> (DebugUartTx, heapless::spsc::Consumer<'static, u8, 32>) {
    let txpin = txpin.into_alternate_push_pull(gpioa_moder, gpioa_otyper, gpioa_afrl);
    let rxpin = rxpin.into_alternate_push_pull(gpioa_moder, gpioa_otyper, gpioa_afrl);
    let mut serial = Serial::usart2(
        usart2,
        (txpin, rxpin),
        hal::serial::Config::default().baudrate(115_200.bps()),
        clocks,
        rcc_apb1r1,
    );
    serial.listen(hal::serial::Event::Rxne);
    let (debug_tx, debug_rx) = serial.split();

    // Returning the consumer should only be done once so it is unsafe to call this function twice.
    // This is impossible anyways because this function requires ownership of the serial pins. So
    // we can safely do this here.
    // Additionally, we are not allowed to use the producer because the DEBUG_RX_QUEUE producer
    // side is used in the interrupt handler.
    let (_, debug_rx_consumer) = unsafe { DEBUG_RX_QUEUE.split() };

    cortex_m::interrupt::free(|cs| {
        DEBUG_RX.borrow(cs).replace(Some(debug_rx));
    });
    unsafe {
        cortex_m::peripheral::NVIC::unmask(hal::stm32::Interrupt::USART2);
    }

    // Debug-Output to RTT and serial simultaneously
    // this Writer writes to RTT and our output UART at the same time, thus allowing us to read
    // messages via UART and via RTT (cargo run output). Nice!
    (debug_tx, debug_rx_consumer)
}

impl<'a> SensingPuck<'a> {
    /// take the one and only instance of this hardware. Subsequent calls to this function will
    /// return None! This initializes the whole hardware and wraps the devices in drivers.
    pub fn take() -> Option<SensingPuck<'a>> {
        Some(Self::new(
            cortex_m::Peripherals::take()?,
            pac::Peripherals::take()?,
        ))
    }

    /// create a new instance of the SensingPuck hardware abstraction. Initializes clocks, the RTC, interrupts and
    /// creates drivers.
    fn new(cp: cortex_m::Peripherals, mut dp: pac::Peripherals) -> SensingPuck<'a> {
        // This method can only be called once, so we are allowed to initialize static global variables here!

        let mut rcc = dp.RCC.constrain();
        let mut flash = dp.FLASH.constrain();
        let mut pwr = dp.PWR.constrain(&mut rcc.apb1r1);

        let (systick_reload_val, clocks) = init_clocks!(rcc, flash, pwr);

        // Init Systick to 1 tick per millisecond
        let mut syst = cp.SYST;
        syst.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
        syst.set_reload(systick_reload_val);
        syst.clear_current();
        syst.enable_counter();
        syst.enable_interrupt();
        uptime::SYSTICK_RUNNING.store(true, Ordering::SeqCst);

        let sleep_fn = sleep::init_sleep(
            dp.RTC,
            &mut rcc.apb1r1,
            &mut rcc.bdcr,
            &mut pwr,
            &mut dp.EXTI,
        );

        // Init GPIO ports and DMA

        let mut gpioa = dp.GPIOA.split(&mut rcc.ahb2);
        let mut gpiob = dp.GPIOB.split(&mut rcc.ahb2);

        let dma1_channels = dp.DMA1.split(&mut rcc.ahb1);

        // Configure interrupts for external wakeup pins
        // PB3: SIG_ALERT/SHT Sensor
        // PB4: SIG_INT1/LSM(MEMS) Sensor
        // PB5: SIG_INT2/LSM(MEMS) Sensor

        let mut int_sht = gpiob
            .pb3
            .into_pull_up_input(&mut gpiob.moder, &mut gpiob.pupdr);
        int_sht.make_interrupt_source(&mut dp.SYSCFG, &mut rcc.apb2);
        int_sht.enable_interrupt(&mut dp.EXTI);
        int_sht.trigger_on_edge(&mut dp.EXTI, hal::gpio::Edge::Falling); // assuming this is active low
        cortex_m::interrupt::free(|cs| {
            INT_SHT.borrow(cs).replace(Some(int_sht));
        });

        let mut int1_lsm = gpiob
            .pb4
            .into_pull_up_input(&mut gpiob.moder, &mut gpiob.pupdr);
        int1_lsm.make_interrupt_source(&mut dp.SYSCFG, &mut rcc.apb2);
        int1_lsm.enable_interrupt(&mut dp.EXTI);
        int1_lsm.trigger_on_edge(&mut dp.EXTI, hal::gpio::Edge::Rising); // active high
        cortex_m::interrupt::free(|cs| {
            INT1_LSM.borrow(cs).replace(Some(int1_lsm));
        });

        let mut int2_lsm = gpiob
            .pb5
            .into_pull_up_input(&mut gpiob.moder, &mut gpiob.pupdr);
        int2_lsm.make_interrupt_source(&mut dp.SYSCFG, &mut rcc.apb2);
        int2_lsm.enable_interrupt(&mut dp.EXTI);
        int2_lsm.trigger_on_edge(&mut dp.EXTI, hal::gpio::Edge::Rising); // active high
        cortex_m::interrupt::free(|cs| {
            INT2_LSM.borrow(cs).replace(Some(int2_lsm));
        });

        // unmask all interrupts, grouped together because multiple pins can be
        // handled by the same line or multiple lines handled by the same exti
        unsafe {
            cortex_m::peripheral::NVIC::unmask(hal::stm32::Interrupt::EXTI3);
            cortex_m::peripheral::NVIC::unmask(hal::stm32::Interrupt::EXTI4);
            cortex_m::peripheral::NVIC::unmask(hal::stm32::Interrupt::EXTI9_5);
        }

        // Initialize all external devices
        let eeprom = new_eeprom(
            gpiob.pb13,
            gpiob.pb14,
            gpiob.pb12,
            &mut gpiob.moder,
            &mut gpiob.otyper,
            &mut gpiob.pupdr,
            &mut gpiob.afrh,
            &mut rcc.apb1r1,
            clocks,
            dp.I2C2,
        );

        let led_matrix = new_led_matrix(
            gpiob.pb8,
            gpiob.pb9,
            gpioa.pa5,
            gpioa.pa6,
            &mut gpiob.moder,
            &mut gpiob.otyper,
            &mut gpiob.pupdr,
            &mut gpiob.afrh,
            &mut gpioa.moder,
            &mut gpioa.otyper,
            &mut rcc.apb1r1,
            clocks,
            dp.I2C1,
        );

        let (temperature_humidity_sensor, accelerometer) = init_temperature_humidity_accelerometer(
            gpiob.pb6,
            gpiob.pb7,
            &mut gpiob.moder,
            &mut gpiob.otyper,
            &mut gpiob.pupdr,
            &mut gpiob.afrl,
            &mut rcc.apb1r2,
            clocks,
            dp.I2C4,
        );

        let modem_driver = init_modem(
            gpioa.pa9,
            gpioa.pa10,
            gpiob.pb1,
            gpioa.pa8,
            gpiob.pb15,
            &mut gpioa.moder,
            &mut gpioa.otyper,
            &mut gpioa.afrh,
            &mut gpiob.moder,
            &mut gpiob.otyper,
            &mut rcc.apb2,
            clocks,
            dp.USART1,
            dma1_channels.5,
        );

        let esp = init_esp(
            gpioa.pa0,
            gpioa.pa1,
            gpioa.pa4,
            &mut gpioa.moder,
            &mut gpioa.otyper,
            &mut gpioa.afrl,
            &mut rcc.apb1r1,
            clocks,
            dp.UART4,
        );

        let vbat = init_vbat_measurement(dp.ADC1, dp.ADC_COMMON, &mut rcc.ahb2, &mut rcc.ccipr);

        let (debug_tx, debug_rx) = init_debug_uart(
            gpioa.pa2,
            gpioa.pa3,
            &mut gpioa.moder,
            &mut gpioa.otyper,
            &mut gpioa.afrl,
            &mut rcc.apb1r1,
            clocks,
            dp.USART2,
        );

        let rng = dp.RNG.enable(&mut rcc.ahb2, clocks);

        Self {
            eeprom,
            led_matrix,
            debug_tx,
            debug_rx,
            accelerometer,
            temperature_humidity_sensor,
            modem_driver,
            esp,
            vbat,
            rng,
            clock: &MILLISECOND_CLOCK,
            sleep_fn,
        }
    }
}

#[interrupt]
#[allow(non_snake_case)]
fn EXTI3() {
    cortex_m::interrupt::free(|cs| {
        // Check all interrupt sources and eventually clear the flag and set the wakeup reason
        let mut sht_ref = INT_SHT.borrow(cs).borrow_mut();
        if let Some(ref mut sht) = sht_ref.deref_mut() {
            if sht.check_interrupt() {
                // if we don't clear this bit, the ISR would trigger indefinitely
                sht.clear_interrupt_pending_bit();
                sleep::add_wakeup_reason(cs, sleep::WakeupReason::SHTInterrupt);
            }
        }
    });
}

#[interrupt]
#[allow(non_snake_case)]
fn EXTI4() {
    cortex_m::interrupt::free(|cs| {
        // Check all interrupt sources and eventually clear the flag and set the wakeup reason
        let mut lsm1_ref = INT1_LSM.borrow(cs).borrow_mut();
        if let Some(ref mut lsm) = lsm1_ref.deref_mut() {
            if lsm.check_interrupt() {
                // if we don't clear this bit, the ISR would trigger indefinitely
                lsm.clear_interrupt_pending_bit();
                sleep::add_wakeup_reason(cs, sleep::WakeupReason::LSM1Interrupt);
            }
        }
    });
}

#[interrupt]
#[allow(non_snake_case)]
fn EXTI9_5() {
    cortex_m::interrupt::free(|cs| {
        // Check all interrupt sources and eventually clear the flag and set the wakeup reason
        let mut lsm2_ref = INT2_LSM.borrow(cs).borrow_mut();
        if let Some(ref mut lsm) = lsm2_ref.deref_mut() {
            if lsm.check_interrupt() {
                // if we don't clear this bit, the ISR would trigger indefinitely
                lsm.clear_interrupt_pending_bit();
                sleep::add_wakeup_reason(cs, sleep::WakeupReason::LSM2Interrupt);
            }
        }
    });
}

#[interrupt]
#[allow(non_snake_case)]
fn USART2() {
    cortex_m::interrupt::free(|cs| {
        // Check if a byte was received and push it to the rx queue
        let mut rx_ref = DEBUG_RX.borrow(cs).borrow_mut();
        if let Some(ref mut rx) = rx_ref.deref_mut() {
            if let Ok(byte) = rx.read() {
                unsafe {
                    // When setting up the debug-uart, we hand out the consumer side to the
                    // spsc::Queue but we throw away the producer side. So here, we are (only)
                    // allowed to use the producer side's methods, i.e. enqueue is allowed.
                    // The producer is basically just a thin layer around the Queue, see
                    // https://docs.rs/heapless/0.7.10/src/heapless/spsc.rs.html#559-561
                    let _ = DEBUG_RX_QUEUE.enqueue(byte);
                }
            }
        }
    });
}

#[exception]
#[allow(non_snake_case)]
fn SysTick() {
    uptime::increment_uptime(1);
}
