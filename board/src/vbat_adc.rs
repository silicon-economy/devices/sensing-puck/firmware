// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Implements [`VbatAdc`] to read the battery voltage

use crate::uptime;
use core::convert::Infallible;
use embedded_hal::adc::OneShot;
use measurements::voltage::Voltage;
use stm32l4xx_hal as hal;

/// Battery voltage reader type
pub struct VbatAdc<'a> {
    pub adc: hal::adc::Adc<hal::pac::ADC1>,
    pub delay: embedded_timers::delay::Delay<'a, uptime::MilliSecondClock>,
}

impl VbatAdc<'_> {
    /// Reads the battery voltage
    pub fn measure_vbat(&mut self) -> Voltage {
        self.adc.calibrate(&mut self.delay);
        let mut vbat = self.adc.enable_vbat().expect("Vbat should not be taken");
        let vbat_sample_res: Result<u16, Infallible> = nb::block!(self.adc.read(&mut vbat));
        let vbat_sample = vbat_sample_res.unwrap();
        self.adc.disable_vbat(vbat);
        // the adc register value of vbat is multiplied by 3, because the VBAT pin is internally
        // connected to a bridge divider by 3.
        let vbat_millivolts = self.adc.to_millivolts(vbat_sample * 3);
        Voltage::from_millivolts(vbat_millivolts as f64)
    }
}
