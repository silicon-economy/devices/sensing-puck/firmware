// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! The `board` crate implements a board support package (BSP) for the Sensing Puck hardware
//!
//! It can be instantiated exactly once with `board::SensingPuck::take().unwrap()`. Currently, the
//! instantiation is not configurable but matched to the requirements of the whole Sensing Puck
//! application.

#![no_std]

pub(crate) mod board;
pub use board::*;

mod circ_buffer_reader;
mod sleep;
pub mod uptime;
pub mod vbat_adc;
pub use sleep::WakeupReason;

// pub re-uses to simplify access from the firmware crate

pub use board::hal;
pub use board::pac;

pub use issi_is31fl3731;
pub use lsm6dsox;
pub use sensirion_rht;
