// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Implements a 64 bit counter to measure the time the system is up which is used as a clock
//!
//! A 64 bit counter is used to prevent the infamous overflow after around 50 days if using 32
//! bit millisecond clocks. Since incrementing a 64 bit integer is not atomic on 32 bit platforms,
//! implementing such a counter/clock is non-trivial. The implementation used here requires that
//! the uptime must not be read (private function `get_uptime_millis`) when the increment operation
//! is running (private function `increment_uptime`). Specifically, this means that
//! `get_uptime_millis` should not be called from interrupt contexts or, if called from an
//! interrupt, that interrupt should have a lower priority than the one which is driving
//! `increment_uptime` to not preempt it.
//!
//! Using this 64 bit uptime counter, the [`MilliSecondClock`] is defined which implements the
//! [`embedded_timers::clock::Clock`] trait.

use core::{
    sync::atomic::{AtomicBool, AtomicU32, Ordering},
    time::Duration,
};

/// Set to true when the systick timer is running
pub(crate) static SYSTICK_RUNNING: AtomicBool = AtomicBool::new(false);

/// High Bytes of the U64 Uptime value
static UPTIME_HIGH: AtomicU32 = AtomicU32::new(0);

/// Low Bytes of the U64 Uptime value
static UPTIME_LOW: AtomicU32 = AtomicU32::new(0);

/// Increment the uptime with a duration given in milliseconds.
///
/// This function must not be interrupted by calls to [`get_uptime_millis`], also see the
/// respective documentation.
pub(crate) fn increment_uptime(millis: u64) {
    let (high_word, low_word) = ((millis >> 32) as u32, millis as u32);

    let (new_uptime_low, carry) = UPTIME_LOW.load(Ordering::Relaxed).overflowing_add(low_word);
    UPTIME_LOW.store(new_uptime_low, Ordering::Relaxed);

    // If the lower word has overflown, add 1 to the high word
    UPTIME_HIGH.store(
        UPTIME_HIGH.load(Ordering::Relaxed) + high_word + u32::from(carry),
        Ordering::Relaxed,
    );
}

/// Get the current uptime of the device in milliseconds
///
/// This function is allowed to be interrupted by calls to [`increment_uptime`] because it can
/// detect overflows between the low and high word of the uptime counter.
///
/// Attention: The other direction is forbidden! This function must never interrupt
/// `increment_uptime`, i.e. it must not be called from an interrupt with a
/// higher priority than the SysTick (or whichever interrupt drives the uptime counter).
fn get_uptime_millis() -> u64 {
    // Read the high bytes first, then the low bytes, and then the high bytes again. If the high
    // bytes are different, the low byte has overflown and thus we have to query the two again. Do
    // this as long as the two high reads are not equal.
    loop {
        let uptime_high1 = UPTIME_HIGH.load(Ordering::SeqCst);
        let uptime_low = UPTIME_LOW.load(Ordering::SeqCst);
        let uptime_high2 = UPTIME_HIGH.load(Ordering::SeqCst);
        if uptime_high1 == uptime_high2 {
            let uptime: u64 = ((uptime_high1 as u64) << 32) | uptime_low as u64;
            break uptime;
        }
    }
}

/// get the current uptime of the device as a Duration
pub(crate) fn get_uptime() -> Duration {
    Duration::from_millis(get_uptime_millis())
}

/// Clock running on the SysTick millisecond timer. Also increment after sleep. Fails when you try
/// to poll it while sleeping (huh) and before initialization of the board.
///
/// The MilliSecondClock is a static instance. It can be used regardless of the SysTick
/// state, it will handle the systick not running yet (managed through SYSTICK_RUNNING).
#[derive(Debug)]
pub struct MilliSecondClock;

impl embedded_timers::clock::Clock for MilliSecondClock {
    fn try_now(
        &self,
    ) -> Result<embedded_timers::clock::Instant, embedded_timers::clock::ClockError> {
        match SYSTICK_RUNNING.load(Ordering::SeqCst) {
            true => Ok(get_uptime()),
            false => Err(embedded_timers::clock::ClockError::NotRunning),
        }
    }
}
