// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Implements sleep mode and wakeup reason handling
//!
//! Unfortunately, a low-power sleep mode requires de-initializing many MCU peripherals and
//! stopping clocks which contradicts some of the strong guarantees that the `stm32l4xx_hal` tries
//! to enforce (like many other Rust HALs, too). Therefore, this module is full of low-level
//! (unsafe) code which is carefully crafted to achieve low power consumption in sleep mode while
//! not breaking any functionality after wake up. This is closely tied to the board setup code and
//! may fail if that setup is changed.

pub use hal::stm32 as pac;
pub use stm32l4xx_hal as hal;

use crate::uptime;
use core::{
    cell::{Cell, RefCell},
    ops::DerefMut,
    sync::atomic::Ordering,
    time::Duration,
};
use cortex_m::interrupt::{CriticalSection, Mutex};
use enumflags2::{bitflags, BitFlags};
use hal::{interrupt, prelude::*};

/// Represents all external Wakeup Reasons/Interrupts
#[bitflags]
#[repr(u8)]
#[derive(Debug, Copy, Clone)]
pub enum WakeupReason {
    /// Interrupt from SHT31 (RHT) Sensor
    SHTInterrupt,
    /// Interrupt Line 1 from LSM6DSOX Accelerometer
    LSM1Interrupt,
    /// Interrupt Line 2 from LSM6DSOX Accelerometer
    LSM2Interrupt,
}

/// Mutexed instance of the devices RTC, used in the sleep function
static RTC: Mutex<RefCell<Option<hal::rtc::Rtc>>> = Mutex::new(RefCell::new(None));

/// Wakeup Reason flags set from the pin interrupts
static WAKEUP_REASONS: Mutex<Cell<BitFlags<WakeupReason>>> = Mutex::new(Cell::new(BitFlags::EMPTY));

/// Adds a wakeup reason (if not already set) to the list of wakeup reasons
///
/// This has to be called from the interrupt handlers which handle the external wakeup interrupts.
/// The wakeup reasons will be reset whenever the device goes to sleep.
pub fn add_wakeup_reason(cs: &CriticalSection, wakeup_reason: WakeupReason) {
    // cs is a CriticalSection, required to unlock the Mutexes
    let mut reasons = WAKEUP_REASONS.borrow(cs).get();
    reasons.insert(wakeup_reason);
    WAKEUP_REASONS.borrow(cs).set(reasons);
}

/// Configures everything that is needed to successfully use the sleep function
///
/// The sleep function pointer is returned so that this has to be called before using it.
#[inline(always)]
pub fn init_sleep(
    rtc: pac::RTC,
    rcc_apb1r1: &mut hal::rcc::APB1R1,
    rcc_bdcr: &mut hal::rcc::BDCR,
    pwr: &mut hal::pwr::Pwr,
    exti: &mut pac::EXTI,
) -> fn(Option<Duration>) -> BitFlags<WakeupReason> {
    // Init RTC with external crystal
    let mut rtc = hal::rtc::Rtc::rtc(
        rtc,
        rcc_apb1r1,
        rcc_bdcr,
        &mut pwr.cr1,
        hal::rtc::RtcConfig::default()
            .clock_config(hal::rtc::RtcClockSource::LSE)
            .wakeup_clock_config(hal::rtc::RtcWakeupClockSource::RtcClkDiv16),
    );
    rtc.listen(exti, hal::rtc::Event::WakeupTimer);
    unsafe {
        pac::NVIC::unmask(pac::Interrupt::RTC_WKUP);
    }

    // store global RTC instance
    cortex_m::interrupt::free(|cs| {
        RTC.borrow(cs).replace(Some(rtc));
    });

    sleep
}

/// Put the Sensing Puck Hardware to sleep for the Duration. Sleeps with a resolution of seconds,
/// everything less than 1s results in a delay. The Clock is incremented accordingly, Interrupts
/// may wake up the device. If so, you can derive which Interrupts triggered from the returned
/// WakeupReasons.
///
/// If No sleep duration is given, the device will sleep until an interrupt happens.
fn sleep(sleep_duration: Option<Duration>) -> BitFlags<WakeupReason> {
    log::info!("Sleep: {:?}", sleep_duration);

    // Reset the WAKEUP_REASONS here:
    // 1. we reset any interrupts that occurred during the active phase
    // 2. we can safely "forget" to reset them after having been woken up :)
    cortex_m::interrupt::free(|cs| {
        WAKEUP_REASONS.borrow(cs).set(BitFlags::empty());
    });
    // this will be the function / closure to determine the wakeup-reasons later on
    let read_wakeup_reasons = || {
        cortex_m::interrupt::free(|cs| {
            // no mutex
            WAKEUP_REASONS.borrow(cs).get()
        })
    };
    // this will be the function / closure to check if we have been woken up
    let check_wakeup = || read_wakeup_reasons() != BitFlags::empty();

    // Only on debug builds, delay 200ms before any sleeping attempt to allow RTT messages to flush
    #[cfg(debug_assertions)]
    let sleep_duration = {
        const RTT_DELAY: Duration = Duration::from_millis(200);
        use embedded_timers::clock::Clock;
        let mut delay = uptime::MilliSecondClock {}.new_delay();
        match sleep_duration {
            Some(sleep_duration) if sleep_duration <= RTT_DELAY => {
                delay.delay_ms(sleep_duration.as_millis() as u32);
                return read_wakeup_reasons();
            }
            Some(sleep_duration) => {
                delay.delay_ms(RTT_DELAY.as_millis() as u32);
                Some(sleep_duration - RTT_DELAY)
            }
            None => {
                delay.delay_ms(RTT_DELAY.as_millis() as u32);
                None
            }
        }
    };

    // Steal the peripherals because some of them are frozen and unaccessible
    // this can only be fixed by integrating the sleep mode into the HAL
    let mut cp = unsafe { cortex_m::Peripherals::steal() };
    let _dp = unsafe { pac::Peripherals::steal() };

    // disable systick and read rtc time: switch to rtc-based timing
    uptime::SYSTICK_RUNNING.store(false, Ordering::SeqCst);
    cp.SYST.disable_interrupt();
    let start_datetime = rtc_read_date_and_time().unwrap();

    // repeatedly go to sleep until we find any wakeup-condition (interrupt or sleep-duration)
    loop {
        // stop sleeping when interrupted
        if check_wakeup() {
            break;
        }

        // stop sleeping when sleep duration has been reached
        let sleep_rest = if let Some(sleep_duration) = sleep_duration {
            let sleep_until_now = rtc_duration_since(start_datetime);
            if sleep_until_now >= sleep_duration {
                break;
            }
            Some(sleep_duration - sleep_until_now)
        } else {
            None
        };

        log::info!("Sleep rest: {:?}", sleep_rest);

        single_sleep(sleep_rest);
    }

    // increase systick counter and re-enable systick: switch back to systick-based timing
    uptime::increment_uptime(rtc_duration_since(start_datetime).as_millis() as u64);
    cp.SYST.enable_interrupt();
    uptime::SYSTICK_RUNNING.store(true, Ordering::SeqCst);

    read_wakeup_reasons()
}

/// Sleeps for the given duration, or shorter! Can be shorter because:
/// a) sleep is interrupted
/// b) requested duration exceeds maximum supported rtc duration
///
/// Before going to sleep, all pins/peripherals are de-initialized. After waking up, everything
/// is re-initialized again. This seems like a huge overhead if we are sleeping the maximum sleep
/// duration in a loop to reach a bigger total sleep duration, but at least it is conceptually
/// simple and should work in any case. If a desired sleep duration is given, the device is active
/// anyways (it will read sensors from time to time and transmit data from time to time). So for
/// these cases, this small amount of extra energy consumption could be tolerable.
/// TODO The above should probably be measured one day?
fn single_sleep(max_sleep_duration: Option<core::time::Duration>) {
    let mut cp = unsafe { cortex_m::Peripherals::steal() };
    let dp = unsafe { pac::Peripherals::steal() };

    // disable peripherals
    dp.USART1.cr1.write(|w| w.ue().clear_bit());
    dp.USART2.cr1.write(|w| w.ue().clear_bit());
    dp.UART4.cr1.write(|w| w.ue().clear_bit());
    dp.I2C1.cr1.write(|w| w.pe().clear_bit());
    dp.I2C4.cr1.write(|w| w.pe().clear_bit());
    dp.RCC.ahb1enr.write(|w| w.dma1en().clear_bit()); // disable dma as well

    // read the current state of the gpio mode registers
    let gpioa_bits = dp.GPIOA.moder.read().bits();
    let gpiob_bits = dp.GPIOB.moder.read().bits();
    let gpioc_bits = dp.GPIOC.moder.read().bits();
    let gpiod_bits = dp.GPIOD.moder.read().bits();
    let gpioe_bits = dp.GPIOE.moder.read().bits();
    let gpioh_bits = dp.GPIOH.moder.read().bits();

    // Set all pins which are not required during sleep to analog mode (0b11)
    dp.GPIOA.moder.write(|w| unsafe {
        w.bits(0xFFFF_FFFF)
            .moder5()
            .output() // leds_enable
            .moder6()
            .output() // leds_shutdown
    });
    dp.GPIOB.moder.write(|w| unsafe {
        w.bits(0xFFFF_FFFF)
            .moder3()
            .input() // int_sht
            .moder4()
            .input() // int1_lsm
            .moder5()
            .input() // int2_lsm
    });

    dp.GPIOC.moder.write(|w| unsafe { w.bits(0xFFFF_FFFF) });
    dp.GPIOD.moder.write(|w| unsafe { w.bits(0xFFFF_FFFF) });
    dp.GPIOE.moder.write(|w| unsafe { w.bits(0xFFFF_FFFF) });
    dp.GPIOH.moder.write(|w| unsafe { w.bits(0x0000_000F) });

    // Ensure that there are no PU/PD configurations for the input pins
    dp.GPIOB.pupdr.modify(|_, w| {
        w.pupdr3()
            .floating()
            .pupdr4()
            .floating()
            .pupdr5()
            .floating()
    });

    if let Some(sleep_duration) = max_sleep_duration {
        // maximum value for wakeup timer, 2^16 ticks
        const MAX_SLEEP_TICKS: u64 = 1 << 16;
        // TODO: stm32l4xx-hal::WakeupTimer documentation says something different than above:
        // WakeupTimer::start() panics if delay is outside of range 1 <= delay <= 2^17
        // Oops, stm32l4xx-hal seems to be wrong. But anyways, we have to make sure that we sleep
        // at least 1 tick.
        const MIN_SLEEP_TICKS: u32 = 1;
        const RTCCLK: f64 = 32767.0; // Hz
        const WAKEUP_TIMER_CLK: f64 = RTCCLK / 16.0; // Derive from WUCKSEL config

        let sleep_ticks = sleep_duration.as_millis() as f64 * (WAKEUP_TIMER_CLK / 1000.0);
        let sleep_ticks = core::cmp::min(sleep_ticks as u64, MAX_SLEEP_TICKS);
        let sleep_ticks = core::cmp::max(sleep_ticks as u32, MIN_SLEEP_TICKS);

        // Start wakeup timer
        cortex_m::interrupt::free(|cs| {
            let mut rtc_ref = RTC.borrow(cs).borrow_mut();
            if let Some(ref mut rtc) = rtc_ref.deref_mut() {
                rtc.wakeup_timer().start(sleep_ticks);
            } else {
                log::error!("Could not acquire RTC to start wakeup timer!");
            }
        });
    }

    // Go to Stop2 Mode when in sleep (wfi)
    unsafe {
        dp.PWR.cr1.write(|w| w.lpms().bits(0b010)); // Low Power Mode Select = 0b010 -> Stop2
        dp.PWR.cr3.write(|w| {
            w.ewf().set_bit(); // enable internal wakeup line
            w.ewup1().clear_bit(); // disable external wakeup lines
            w.ewup2().clear_bit();
            w.ewup3().clear_bit();
            w.ewup4().clear_bit();
            w.ewup5().clear_bit()
        });
    }

    // Enable debugging during stop2 mode, but not in release builds, costs around 250uA
    #[cfg(debug_assertions)]
    dp.DBGMCU.cr.write(|w| w.dbg_stop().set_bit());
    #[cfg(not(debug_assertions))]
    dp.DBGMCU.cr.write(|w| w.dbg_stop().clear_bit());

    // Enable deep sleep on WFI/WFE
    cp.SCB.set_sleepdeep();
    cp.SCB.clear_sleeponexit();

    // enter Stop2 mode
    cortex_m::asm::wfi();

    if max_sleep_duration.is_some() {
        // Cancel (clear internal flag) wakeup timer, another hacky workaround because we seem
        // to get stuck in timer.cancel(). Possibly because of ST, but possible also because of
        // a bug documented in the HAL which leads back to... ST as well!
        dp.RTC.cr.write(|w| w.wute().clear_bit());
        cortex_m::asm::delay(100);
        dp.RTC.isr.write(|w| w.wutf().clear_bit());
    }

    // Reset GPIO modes to initially stored values
    dp.GPIOA.moder.write(|w| unsafe { w.bits(gpioa_bits) });
    dp.GPIOB.moder.write(|w| unsafe { w.bits(gpiob_bits) });
    dp.GPIOC.moder.write(|w| unsafe { w.bits(gpioc_bits) });
    dp.GPIOD.moder.write(|w| unsafe { w.bits(gpiod_bits) });
    dp.GPIOE.moder.write(|w| unsafe { w.bits(gpioe_bits) });
    dp.GPIOH.moder.write(|w| unsafe { w.bits(gpioh_bits) });

    // enable peripherals
    dp.RCC.ahb1enr.write(|w| w.dma1en().set_bit());
    dp.USART1
        .cr1
        .write(|w| w.ue().set_bit().re().set_bit().te().set_bit());
    dp.USART2
        .cr1
        .write(|w| w.ue().set_bit().re().set_bit().te().set_bit());
    dp.UART4
        .cr1
        .write(|w| w.ue().set_bit().re().set_bit().te().set_bit());
    dp.I2C1.cr1.write(|w| w.pe().set_bit());
    dp.I2C4.cr1.write(|w| w.pe().set_bit());

    // Reinit clocks
    let mut rcc = dp.RCC.constrain();
    let mut flash = dp.FLASH.constrain();
    let mut pwr = dp.PWR.constrain(&mut rcc.apb1r1);

    let (_systick_reload_val, _clocks) = crate::board::init_clocks!(rcc, flash, pwr);
}

/// Reads date and time from the rtc
fn rtc_read_date_and_time() -> Result<time::PrimitiveDateTime, ()> {
    cortex_m::interrupt::free(|cs| {
        let mut rtc_ref = RTC.borrow(cs).borrow_mut();
        if let Some(ref mut rtc) = rtc_ref.deref_mut() {
            Ok(rtc.get_datetime())
        } else {
            log::error!("Could not acquire RTC!");
            Err(())
        }
    })
}

/// Reads the rtc and calculates how much time has passed since the given instant
fn rtc_duration_since(start_datetime: time::PrimitiveDateTime) -> core::time::Duration {
    let end_datetime = rtc_read_date_and_time().unwrap();
    let time_duration = end_datetime - start_datetime;
    assert!(!time_duration.is_negative(), "sleep duration should be >=0");
    time_duration.unsigned_abs()
}

#[interrupt]
#[allow(non_snake_case)]
fn RTC_WKUP() {
    cortex_m::interrupt::free(|cs| {
        let mut rtc_ref = RTC.borrow(cs).borrow_mut();
        if let Some(ref mut rtc) = rtc_ref.deref_mut() {
            if rtc.check_interrupt(hal::rtc::Event::WakeupTimer, true) {
                cortex_m::asm::nop(); // Dont optimize this away!
                                      // RTC Wakeup triggered through WakeupTimer, flag cleared!
            }
        }
    });
}
