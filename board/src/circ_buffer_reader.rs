// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Implements the [`embedded_hal::serial::Read`] trait for the [`stm32l4xx_hal::dma::CircBuffer`]
//! using a wrapper type. The `CircBuffer` is used for DMA-based UART RX.

use crate::hal;

/// Simple wrapper type to implement [`embedded_hal::serial::Read`] on a
/// [`stm32l4xx_hal::dma::CircBuffer`]
pub struct CircBufferReader(pub(crate) hal::dma::CircBuffer<[u8; 2048], hal::serial::RxDma1>);

impl embedded_hal::serial::Read<u8> for CircBufferReader {
    type Error = hal::serial::Error; // we translate from hal::dma::Error
    fn read(&mut self) -> nb::Result<u8, hal::serial::Error> {
        let mut buf = [0u8; 1];
        match self.0.read(&mut buf) {
            Ok(1) => Ok(buf[0]),
            Ok(0) => Err(nb::Error::WouldBlock),
            Ok(num) => panic!("{} bytes read into a 1 byte read buffer", num),
            Err(hal::dma::Error::Overrun) => Err(nb::Error::Other(hal::serial::Error::Overrun)),
            Err(_) => Err(nb::Error::Other(hal::serial::Error::Noise)),
        }
    }
}
