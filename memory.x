/* Specify the memory areas */
MEMORY
{
    RAM (xrw)   : ORIGIN = 0x20000000, LENGTH = 160K
    FLASH (rx)  : ORIGIN = 0x8000000, LENGTH = 256K
}
