// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module defines the underlying state machine for Sensing Puck
//!
//! The <a href="https://docs.rs/smlang/0.4.0/smlang/">smlang</a> is used to define the state machine. <br />
//! The state machine consists of two different part, a Job and a NoJob part.
//!
//! No Job Part<br />
//! The device will sleep until manually operated.
//! On Double Tap, the Device ID will be shown on the Display.
//! A second Double Tap while the ID is still shown will let the Device fetch Job Data from the backend.
//!
//! Job Part<br />
//! The Device will take measurements according to the job config and sleep otherwise.
//! If there is an alarm case or the amount of measurements taken exceeds the data amount from the job config,
//! the device will send all measurements to the backend.
//! For manual operation via Double Tap, there is a sequence of 3 action that might be taken.<br />
//! Showing the Temperature -> Showing the Device ID -> Transmitting data to the backend<br />
//! The Double Tap must occur while the display is still turned on, or the state will reset to the default JobSleep

use smlang::statemachine;

use crate::communication_controller;
use crate::config;
use crate::hardware_abstraction;
use crate::led_matrix::LedMatrix;
use embedded_hal::timer::{Cancel, CountDown};
use embedded_timers::{clock::Clock, Timer};

use heapless::Vec;

use crate::communication_controller::ComController;
use crate::config::DeviceConfig;
use crate::protocol_definition::{ComCause, MeasurementData, MotionState};

use core::fmt::Debug;
use core::time::Duration;

statemachine! {
    transitions: {
        // No Job Part
        *Start + NoJobFound / to_no_job_shutdown = NoJobShutdown,
        // TODO If this would be a classic match, I would have matched against NoJobShutdown(Some(led_timer))
        // or so instead of using a dedicated NoJobShowId state
        // but I couldn't figure out how to do this with smlang, probably currently not possible
        NoJobShowId + BatteryMeasurementTimerExpired / show_id = NoJobShowId,
        NoJobShutdown + DoubleTap / show_vbat_user = NoJobShowId,
        NoJobShutdown + NewJobFound / to_job_sleep = JobSleep,
        NoJobShowId + DoubleTap / fetch_job_data = NoJobCommunication,
        NoJobShowId + LedTimerExpired / turn_off_display_no_tim = NoJobShutdown,
        NoJobCommunication + ComSuccess / show_success = NoJobShowComResult,
        NoJobCommunication + ComFailure / show_failure = NoJobShowComResult,
        NoJobShowComResult +  LedTimerExpired / turn_off_display_no_tim = NoJobShutdown,
        // Job Part
        // TODO Motion
        Start + NewJobFound / to_job_sleep = JobSleep,
        JobSleep + NoJobFound / to_no_job_shutdown = NoJobShutdown,
        JobSleep + MeasurementTimerExpired / measure = JobSleep,
        JobSleep + DoubleTap / show_temp_alarm = JobShowMeasurement,
        // JobSleep + MotionChanged / measure = JobSleep,
        JobSleep + Send / send = JobCommunicating,
        JobShowMeasurement + DoubleTap / show_id = JobShowId,
        JobShowMeasurement + LedTimerExpired / turn_off_display_no_tim = JobSleep, // no_tim because it should already run
        JobShowMeasurement + MeasurementTimerExpired / measure = JobShowMeasurement,
        JobShowMeasurement + BatteryMeasurementTimerExpired / show_vbat_alarm = JobShowMeasurement,
        JobShowId + DoubleTap / send_user = JobCommunicating,
        JobShowId + LedTimerExpired / turn_off_display_no_tim = JobSleep, // no_tim because it should already run
        JobShowId + MeasurementTimerExpired / measure = JobShowId,
        JobCommunicating + ComSuccess / show_success = JobShowComResult,
        JobCommunicating + ComFailure / show_failure = JobShowComResult,
        JobShowComResult + LedTimerExpired / turn_off_display_tim = JobSleep,
        JobShowComResult + MeasurementTimerExpired / measure = JobShowComResult,
    }
}

/// StateMachine Context that contains all data needed throughout the whole state machine.
pub struct Context<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, BG77, RNG>
where
    CLOCK: Clock + 'static,
    SHT: hardware_abstraction::TemperatureHumiditySensor,
    VBAT: hardware_abstraction::VoltageMeasurement,
    LEDDRIVER: hardware_abstraction::LedMatrixDriver,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
    BG77: embedded_nal::TcpClientStack,
    RNG: embedded_hal::blocking::rng::Read,
    RNG::Error: Debug,
{
    // TODO I don't like all these pubs
    /// Clock instance for taking timestamps
    pub clock: &'static CLOCK,
    /// Led timer to turn off the display
    pub led_timer: Timer<'static, CLOCK>,
    /// Timer to regularly take measurements
    pub measurement_timer: Timer<'static, CLOCK>,
    /// Timer for battery measurement
    pub vbat_timer: Timer<'static, CLOCK>,
    /// The RH/T sensor for measuring
    sht: SHT,
    // Measurement of the battery voltage
    vbat: VBAT,
    /// Vector to store all measurements taken since the last successful transmission.
    /// Has to be bigger than (at least equal to) the data amount configured in the job config.
    pub measurements: Vec<MeasurementData, { config::MEASUREMENT_CAPACITY }>,
    /// Device Configuration Instance
    pub config: DeviceConfig<Storage, StorageErr>,
    /// Led Display
    leds: LedMatrix<LEDDRIVER>,
    /// Abstraction Layer to the Modem
    com_controller: ComController<CLOCK, BG77, RNG>,
    // last_com_success is used like an old c-style last_err_no()
    // it is needed because we have blocking communication
    // that is started inside an action and depending on the com result
    // a different state transition has to be triggered
    // this kind of "internal" events is not supported by smlang
    // and imho it is cleaner anyways to put the events into the sm
    // instead of having internal and external events mixed
    // TODO I'm currently not sure what is the cleanest way
    // to get an output (in form of data and not "just" an action)
    // from the smlang.
    // For now I will use this "last_error" style variable.
    // Other options would be to put this data onto a state,
    // but then we would have to query the state from outside all the time
    // what is also not nice. Therefore I put it directly inside the context for now.
    // Maybe we should consider to extend the smlang to output data and see
    // if we can make matching witch state parameters possible
    pub last_com_success: Option<bool>,
    /// Current cause of communication. Default is Data.
    com_cause: ComCause,
    /// Internal alarm for low battery
    alarm_vbat: bool,
}

impl<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, BG77, RNG> StateMachineContext
    for Context<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, BG77, RNG>
where
    CLOCK: Clock + Sync + 'static,
    SHT: hardware_abstraction::TemperatureHumiditySensor,
    VBAT: hardware_abstraction::VoltageMeasurement,
    LEDDRIVER: hardware_abstraction::LedMatrixDriver,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
    BG77: embedded_nal::TcpClientStack,
    RNG: embedded_hal::blocking::rng::Read + 'static,
    RNG::Error: Debug,
{
    /// Cancels all timers and clears the new job state
    fn to_no_job_shutdown(&mut self) {
        log::trace!("Start -> NoJobShutdown");

        // Stop all Timers
        let _ = self.led_timer.cancel();
        let _ = self.measurement_timer.cancel();

        self.config.clear_new_job_state();
    }

    /// Shows the Device ID on the display
    fn show_id(&mut self) {
        log::trace!("show_id");

        let _ = self.leds.power(true);
        let _ = self.leds.show_id(*self.config.device_id());

        self.led_timer
            .start(self.config.led_timing_config().show_id());
    }

    /// Shows a low battery in NOJOB state, if the battery voltage is smaller than the default config voltage value.
    fn show_vbat_user(&mut self) {
        log::trace!("show_vbat_alarm");
        self.show_vbat_alarm();
        let _ = self.led_timer.cancel();

        let timeout = if self.alarm_vbat {
            config::SHOW_LOW_BAT
        } else {
            config::SHOW_NO_LOW_BAT
        };

        self.vbat_timer.start(timeout);
    }

    /// Communicates with the backend using ComCause::JobRequest
    /// in order to fetch the latest job data.
    fn fetch_job_data(&mut self) {
        log::trace!("fetch_job_data");

        let _ = self.leds.power(true);
        let _ = self.leds.show_com(false);

        let resp = self.com_controller.tx_rx(
            &self.measurements,
            self.clock.try_now().unwrap().as_millis() as u64, // TODO: Error Handling
            ComCause::JobRequest,
            &self.config,
        );

        match resp {
            Ok(msg) => {
                log::trace!("msg: {:?}", msg);

                self.config.set_remote_config(msg);

                self.measurements.clear();
                self.config.clear_alarm_send();

                self.last_com_success = Some(true);
            }
            Err(e) => {
                log::info!("Send Data Error");

                if matches!(e, communication_controller::ComError::Serialization(_)) {
                    // A serialization failure suggests that we have collected too much data
                    self.measurements.clear();
                }

                self.last_com_success = Some(false);
            }
        }
    }

    /// Turns off the display
    fn turn_off_display_no_tim(&mut self) {
        log::trace!("turn_off_display_no_tim");
        let _ = self.leds.power(false);
    }

    /// Turns the display off and starts the measurement timer
    fn turn_off_display_tim(&mut self) {
        log::trace!("turn_off_display_tim");
        let _ = self.leds.power(false);

        self.measurement_timer
            .start(self.config.measurement_config().storage());
    }

    /// Shows the Success Screen and resets the ComCause to Data
    fn show_success(&mut self) {
        log::trace!("show_success");

        let _ = self.leds.power(true);
        let _ = self.leds.show_ok();

        self.led_timer.start(config::SHOW_SUCCESS_TIMEOUT);

        self.com_cause = ComCause::Data;
    }

    /// Shows the Failure Screen
    fn show_failure(&mut self) {
        log::trace!("show_failure");

        let _ = self.leds.power(true);
        let _ = self.leds.show_err();

        self.led_timer.start(config::SHOW_FAILURE_TIMEOUT);
    }

    /// Starts the measurement timer and clears the new job state
    fn to_job_sleep(&mut self) {
        log::trace!("Start -> JobSleep");

        self.measurement_timer
            .start(self.config.measurement_config().storage());

        self.config.clear_new_job_state();
    }

    /// Takes a measurement, stores it in the measurement vector if possible,
    /// and shows the temperature on the display. It will check the alarm condition
    /// and sets and shows the alarm flag accordingly.
    fn show_temp_alarm(&mut self) {
        log::trace!("show_temp_alarm");

        // TODO Get Motion State and save it to current measurement

        match (self.sht.single_shot(), self.vbat.measurement_vbat()) {
            (Ok((temperature, humidity)), Ok(voltage)) => {
                log::debug!("Measurement: {temperature}, {humidity}");
                log::debug!("Battery voltage: {voltage}");
                let temperature = temperature.as_celsius() as f32;
                let humidity = humidity.as_percent() as f32;
                let voltage = voltage.as_volts() as f32;
                // Ignore push fails
                let _ = self.measurements.push(MeasurementData {
                    // Safe to unwrap because we already checked if is_some
                    timestamp: self.clock.try_now().unwrap().as_millis() as u64,
                    temperature,
                    humidity,
                    voltage: Some(voltage),
                    motionState: MotionState::Static,
                });
                self.config.set_new_data();
                log::debug!("measurements.len(): {}", self.measurements.len());

                if temperature > (self.config.temperature_alarm_config()).upperBound
                    || temperature < (self.config.temperature_alarm_config()).lowerBound
                    || humidity > (self.config.humidity_alarm_config()).upperBound
                    || humidity < (self.config.humidity_alarm_config()).lowerBound
                {
                    self.config.set_alarm(true);
                    self.com_cause = ComCause::Alarm;
                }

                match *self.config.voltage_alarm_config() {
                    Some(voltage_alarm) if (voltage <= voltage_alarm) => self.alarm_vbat = true,
                    _ => self.alarm_vbat = false, // voltage ok or no alarm configured
                }

                let _ = self.leds.power(true);
                let _ = self
                    .leds
                    .show_temperature(temperature as i8, *self.config.alarm());
            }
            _ => {
                log::error!("Measurement Error");
            }
        }

        let timeout = if *self.config.alarm() {
            self.config.led_timing_config().show_alarm()
        } else {
            self.config.led_timing_config().show_temp()
        };

        self.vbat_timer.start(timeout);
    }

    /// Shows a low battery in JOB state, if the battery voltage is smaller than the config voltage value
    fn show_vbat_alarm(&mut self) {
        log::trace!("show_vbat_alarm");

        match self.vbat.measurement_vbat() {
            Ok(voltage) => {
                log::debug!("Battery voltage: {voltage}");
                let voltage = voltage.as_volts() as f32;
                match *self.config.voltage_alarm_config() {
                    Some(voltage_alarm) if (voltage <= voltage_alarm) => self.alarm_vbat = true,
                    _ => self.alarm_vbat = false, // voltage ok or no alarm configured
                }

                let _ = self.leds.power(true);
                if self.alarm_vbat {
                    let _ = self.leds.show_low_battery();
                }
            }
            Err(_) => {
                log::error!("Measurement Error");
            }
        }

        let timeout = if self.alarm_vbat {
            config::SHOW_LOW_BAT
        } else {
            config::SHOW_NO_LOW_BAT
        };

        self.led_timer.start(timeout);
    }

    /// Takes a measurement from the RH/T sensor, if the measurements vector is not full.
    /// It will set the new_data flag in the config module anyways to trigger a new transmission if the
    /// measurement capacity is exceeded.<br />
    /// It will also check for temperature and humidity alarms and set this accordingly.
    // TODO This method shares a lot of code with the show_temp_alarm method, refactor?
    fn measure(&mut self) {
        log::trace!("measure");

        let _ = self.measurement_timer.cancel();

        // Set new data even when we cannot read any
        // to make sure that we will send if the buffer is full
        self.config.set_new_data();

        // TODO Get Motion State and save it to current measurement

        if self.measurements.len() < self.measurements.capacity() {
            match (self.sht.single_shot(), self.vbat.measurement_vbat()) {
                (Ok((temperature, humidity)), Ok(voltage)) => {
                    // TODO Err Handling
                    log::debug!("Measurement: {temperature}, {humidity}");
                    log::debug!("Battery voltage: {voltage}");
                    let temperature = temperature.as_celsius() as f32;
                    let humidity = humidity.as_percent() as f32;
                    let voltage = voltage.as_volts() as f32;

                    // Ignore push fails
                    let _ = self.measurements.push(MeasurementData {
                        timestamp: self.clock.try_now().unwrap().as_millis() as u64, // TODO: Error Handling?
                        // Save to unwrap because we already checked if is_some
                        temperature,
                        humidity,
                        voltage: Some(voltage),
                        motionState: MotionState::Static,
                    });
                    log::debug!("measurements.len(): {}", self.measurements.len());

                    if temperature > (self.config.temperature_alarm_config()).upperBound
                        || temperature < (self.config.temperature_alarm_config()).lowerBound
                        || humidity > (self.config.humidity_alarm_config()).upperBound
                        || humidity < (self.config.humidity_alarm_config()).lowerBound
                    {
                        self.config.set_alarm(true);
                        self.com_cause = ComCause::Alarm;
                    }

                    match *self.config.voltage_alarm_config() {
                        Some(voltage_alarm) if (voltage <= voltage_alarm) => self.alarm_vbat = true,
                        _ => self.alarm_vbat = false, // voltage ok or no alarm configured
                    }
                }
                _ => {
                    log::error!("Measurement Error");
                }
            }
        }

        self.measurement_timer
            .start(self.config.measurement_config().storage());
    }

    /// Transmits the current data to the backend and sets ComCause::User before
    fn send_user(&mut self) {
        self.com_cause = ComCause::User;

        self.send();
    }

    /// Transmits the current data to the backend
    fn send(&mut self) {
        log::trace!("send");

        let _ = self.leds.power(true);
        let _ = self.leds.show_com(*self.config.alarm());

        let _ = self.measurement_timer.cancel(); // Stop the timer, we will activate it again after the communication (including show result) is done

        // TODO ComCause
        let resp = self.com_controller.tx_rx(
            &self.measurements,
            self.clock.try_now().unwrap().as_millis() as u64,
            self.com_cause,
            &self.config,
        );

        match resp {
            Ok(msg) => {
                self.config.set_remote_config(msg);

                self.measurements.clear();
                self.config.clear_alarm_send();

                self.last_com_success = Some(true);
            }
            Err(e) => {
                log::info!("Send Data Error");

                if matches!(e, communication_controller::ComError::Serialization(_)) {
                    // A serialization failure suggests that we have collected too much data
                    self.measurements.clear();
                }

                self.last_com_success = Some(false);
            }
        }
    }
}

impl<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, BG77, RNG>
    Context<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, BG77, RNG>
where
    CLOCK: Clock + Sync + 'static,
    SHT: hardware_abstraction::TemperatureHumiditySensor,
    VBAT: hardware_abstraction::VoltageMeasurement,
    LEDDRIVER: hardware_abstraction::LedMatrixDriver,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
    BG77: embedded_nal::TcpClientStack,
    RNG: embedded_hal::blocking::rng::Read,
    RNG::Error: Debug,
{
    pub fn new(
        clock: &'static CLOCK,
        sht: SHT,
        vbat: VBAT,
        config: DeviceConfig<Storage, StorageErr>,
        leds: LedMatrix<LEDDRIVER>,
        com_controller: ComController<CLOCK, BG77, RNG>,
    ) -> Self {
        Self {
            clock,
            led_timer: clock.new_timer(),
            measurement_timer: clock.new_timer(),
            vbat_timer: clock.new_timer(),
            sht,
            vbat,
            measurements: Vec::new(),
            config,
            leds,
            com_controller,
            last_com_success: None,
            com_cause: ComCause::Data,
            alarm_vbat: false,
        }
    }

    /// Returns the Duration until the next timer would expire,
    /// or None if no timer is running
    pub fn until_next_timer(&self) -> Option<Duration> {
        let mut durations: Vec<Duration, 3> = Vec::new();

        if let Ok(duration) = self.led_timer.duration_left() {
            durations.push(duration).unwrap();
        }

        if let Ok(duration) = self.measurement_timer.duration_left() {
            durations.push(duration).unwrap();
        }

        if let Ok(duration) = self.vbat_timer.duration_left() {
            durations.push(duration).unwrap();
        }

        durations.into_iter().min()
    }
}
