// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module defines the applications program logic
//!
//! It provides a function that serves as a main loop,
//! where basically are just events checked and translated into input events
//! for the underlying state machine.

pub mod statemachine;

use crate::hardware_abstraction;
use crate::led_matrix::*;
use embedded_timers::clock::Clock;

use statemachine::Events as SmEvents;

use crate::communication_controller::ComController;
use crate::config::DeviceConfig;

use core::fmt::Debug;

pub struct ProgFlow<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, SleepFn, BG77, RNG>
where
    CLOCK: Clock + Sync + 'static,
    SHT: hardware_abstraction::TemperatureHumiditySensor,
    VBAT: hardware_abstraction::VoltageMeasurement,
    LEDDRIVER: hardware_abstraction::LedMatrixDriver,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
    SleepFn: hardware_abstraction::SleepFn,
    BG77: embedded_nal::TcpClientStack,
    RNG: embedded_hal::blocking::rng::Read + 'static,
    RNG::Error: Debug,
{
    #[allow(clippy::type_complexity)]
    sm: statemachine::StateMachine<
        statemachine::Context<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, BG77, RNG>,
    >,
    sleep_fn: SleepFn,
    has_double_tap: bool,
}

impl<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, SleepFn, BG77, RNG>
    ProgFlow<CLOCK, SHT, VBAT, LEDDRIVER, Storage, StorageErr, SleepFn, BG77, RNG>
where
    CLOCK: Clock + Sync + 'static,
    SHT: hardware_abstraction::TemperatureHumiditySensor,
    VBAT: hardware_abstraction::VoltageMeasurement,
    LEDDRIVER: hardware_abstraction::LedMatrixDriver,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
    SleepFn: hardware_abstraction::SleepFn,
    BG77: embedded_nal::TcpClientStack,
    RNG: embedded_hal::blocking::rng::Read,
    RNG::Error: Debug,
{
    pub fn new(
        clock: &'static CLOCK,
        sht: SHT,
        vbat: VBAT,
        config: DeviceConfig<Storage, StorageErr>,
        sleep: SleepFn,
        leds: LedMatrix<LEDDRIVER>,
        com_controller: ComController<CLOCK, BG77, RNG>,
    ) -> Self {
        Self {
            sm: statemachine::StateMachine::new(statemachine::Context::new(
                clock,
                sht,
                vbat,
                config,
                leds,
                com_controller,
            )),
            sleep_fn: sleep,
            has_double_tap: false,
        }
    }

    /// The Main Loop for this application.
    /// It does the event dispatching and will feed them
    /// as state machine events into the underlying state machine
    /// where the actual functionality is implemented.
    pub fn run(&mut self) -> ! {
        // We unwrap here in this block since this is considered as part of
        // initializing and this two transitions have to be valid.
        // We just look if we already have an active job and transit the state machine accordingly
        if self.sm.context().config.is_new_job_active() {
            self.sm.process_event(SmEvents::NewJobFound).unwrap();
        } else {
            self.sm.process_event(SmEvents::NoJobFound).unwrap();
        }
        self.sm.context_mut().config.clear_new_job_state();

        loop {
            // Event Dispatching
            let event = if self.is_com_finished() {
                let event_temp = if self.is_last_com_success() {
                    Some(SmEvents::ComSuccess)
                } else {
                    Some(SmEvents::ComFailure)
                };
                self.sm.context_mut().last_com_success = None;

                event_temp
            } else if self.is_led_timer_event() {
                Some(SmEvents::LedTimerExpired)
            } else if self.is_measurement_timer_event() {
                Some(SmEvents::MeasurementTimerExpired)
            } else if self.is_vbat_timer_event() {
                Some(SmEvents::BatteryMeasurementTimerExpired)
            } else if self.is_send_condition() {
                Some(SmEvents::Send)
            } else if self.is_double_tap_event() {
                log::info!("DoubleTap");
                Some(SmEvents::DoubleTap)
            } else if self.sm.context().config.is_new_job_state() {
                if self.sm.context().config.is_new_job_active() {
                    Some(SmEvents::NewJobFound)
                } else {
                    Some(SmEvents::NoJobFound)
                }
            } else {
                None
            };

            // Just a debug output block
            if event.is_some() {
                let state = self.sm.state();
                // State doesn't implement Display or Debug
                match state {
                    statemachine::States::Start => log::trace!("State: Start"),
                    statemachine::States::NoJobShutdown => log::trace!("State: NoJobShutdown"),
                    statemachine::States::NoJobShowId => log::trace!("State: NoJobShowId"),
                    statemachine::States::NoJobCommunication => {
                        log::trace!("State: NoJobCommunication")
                    }
                    statemachine::States::NoJobShowComResult => {
                        log::trace!("State: NoJobShowComResult")
                    }
                    statemachine::States::JobSleep => log::trace!("State: JobSleep"),
                    statemachine::States::JobShowMeasurement => {
                        log::trace!("State: JobShowMeasurement")
                    }
                    statemachine::States::JobShowId => log::trace!("State: JobShowId"),
                    statemachine::States::JobCommunicating => {
                        log::trace!("State: JobCommunicating")
                    }
                    statemachine::States::JobShowComResult => {
                        log::trace!("State: JobShowComResult")
                    }
                }

                // Event doesn't implement Display or Debug
                match &event {
                    Some(e) => match &e {
                        SmEvents::NoJobFound => log::trace!("Event: NoJobFound"),
                        SmEvents::DoubleTap => log::trace!("Event: DoubleTap"),
                        SmEvents::NewJobFound => log::trace!("Event: NewJobFound"),
                        SmEvents::LedTimerExpired => log::trace!("Event: LedTimerExpired"),
                        SmEvents::ComSuccess => log::trace!("Event: ComSuccess"),
                        SmEvents::ComFailure => log::trace!("Event: ComFailure"),
                        SmEvents::MeasurementTimerExpired => {
                            log::trace!("Event: MeasurementTimerExpired")
                        }
                        SmEvents::BatteryMeasurementTimerExpired => {
                            log::trace!("Event: BatteryMeasurementTimerExpired")
                        }
                        SmEvents::Send => log::trace!("Event: Send"),
                    },
                    None => {
                        log::trace!("Event: None");
                    }
                }
            }

            // Process Events
            if let Some(event) = event {
                let transition = self.sm.process_event(event);

                // Debug print the transition
                match transition {
                    Ok(state) => {
                        // State is neither Display nor Debug
                        log::info!("Transition Success");

                        // State doesn't implement Display or Debug
                        match state {
                            statemachine::States::Start => log::trace!("State: Start"),
                            statemachine::States::NoJobShutdown => {
                                log::trace!("State: NoJobShutdown")
                            }
                            statemachine::States::NoJobShowId => log::trace!("State: NoJobShowId"),
                            statemachine::States::NoJobCommunication => {
                                log::trace!("State: NoJobCommunication")
                            }
                            statemachine::States::NoJobShowComResult => {
                                log::trace!("State: NoJobShowComResult")
                            }
                            statemachine::States::JobSleep => log::trace!("State: JobSleep"),
                            statemachine::States::JobShowMeasurement => {
                                log::trace!("State: JobShowMeasurement")
                            }
                            statemachine::States::JobShowId => log::trace!("State: JobShowId"),
                            statemachine::States::JobCommunicating => {
                                log::trace!("State: JobCommunicating")
                            }
                            statemachine::States::JobShowComResult => {
                                log::trace!("State: JobShowComResult")
                            }
                        }
                    }
                    Err(_) => {
                        // Error is neither Display nor Debug
                        log::info!("Transition Error");
                    }
                }
            } else {
                let duration = self.sm.context().until_next_timer();
                log::info!("Sleep for {:?}", duration);
                self.has_double_tap = self.sleep_fn.sleep(duration);
            }
        }
    }

    /// Checks if we have a finished communication
    fn is_com_finished(&self) -> bool {
        self.sm.context().last_com_success.is_some()
    }

    /// Checks if last communication was successful
    fn is_last_com_success(&self) -> bool {
        if let Some(success) = &self.sm.context().last_com_success {
            *success
        } else {
            false
        }
    }

    /// Checks if the led timer is expired
    fn is_led_timer_event(&mut self) -> bool {
        self.sm.context_mut().led_timer.try_wait().is_ok()
    }

    /// Checks if the measurement timer is expired
    fn is_measurement_timer_event(&mut self) -> bool {
        self.sm.context_mut().measurement_timer.try_wait().is_ok()
    }

    /// Checks if the battery timer is expired
    fn is_vbat_timer_event(&mut self) -> bool {
        self.sm.context_mut().vbat_timer.try_wait().is_ok()
    }

    /// Checks if a send condition is met
    fn is_send_condition(&mut self) -> bool {
        let current_measurement_length = self.sm.context().measurements.len();
        let config = &mut self.sm.context_mut().config;

        config.is_send_condition(current_measurement_length as u32)
    }

    /// Checks for double tap event
    fn is_double_tap_event(&mut self) -> bool {
        let result = self.has_double_tap;
        // Resetting the variable on read like with a status register. Not nice but needed to go to
        // sleep again.
        self.has_double_tap = false;

        result
    }
}
