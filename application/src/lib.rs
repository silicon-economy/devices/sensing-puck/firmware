// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! The application crate contains the main application logic
//!
//! It takes the board components like sensors as trait types to make the application independent
//! of the actual hardware configuration. This allows to not only instantiate the `board` in
//! `firmware` and run the `application` code but to also run this code on a development machine
//! which is done with the `simulator`.
//!
//! The application prints debug output using the [`log`] crate. A logger should be set up before
//! calling into [`sensing_puck_app`].

#![no_std]

// allows to use println!() in tests
#[cfg(test)]
#[macro_use]
extern crate std;

pub mod hardware_abstraction;

pub mod program_flow;
pub mod protocol_definition;

use embedded_hal::blocking::delay::DelayMs;
use embedded_timers::clock::Clock;
mod communication_controller;
mod config;

use communication_controller::ComController;
pub mod version;

mod led_matrix;

use core::fmt::Debug;

// Make the led matrix orientation available for the simulator
pub use config::LED_MATRIX_ORIENTATION;
pub use led_matrix::Orientation as LedMatrixOrientation;

/// Main function for the application
///
/// All peripherals used by the application must be passed here.
/// The function will perform all the application logic and will never return
#[allow(clippy::too_many_arguments)] // Alternative abstraction: A trait which returns all the hardware needed to get rid of the many arguments.
pub fn sensing_puck_app<Storage, StorageErr, Rng>(
    sht: impl hardware_abstraction::TemperatureHumiditySensor,
    vbat: impl hardware_abstraction::VoltageMeasurement,
    eeprom: Storage,
    led_driver: impl hardware_abstraction::LedMatrixDriver,
    bg77: impl embedded_nal::TcpClientStack,
    rng: Rng,
    clock: &'static (impl Clock + Sync),
    sleep_fn: impl hardware_abstraction::SleepFn,
) -> !
where
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
    Rng: embedded_hal::blocking::rng::Read + 'static,
    Rng::Error: Debug,
{
    // Startup, print Version and Delay a little bit
    log::info!("Firmware Version: {:?}", version::VERSION);
    log::info!("Device ID: {}", config::DEVICE_ID);
    // Delay is especially useful while testing/debugging, giving you some time to take action
    let boot_delay_time = 2000u64;
    log::info!("Boot Delay for {} ms", boot_delay_time);
    clock.new_delay().delay_ms(boot_delay_time);

    let config = config::DeviceConfig::new(eeprom);

    // Create the Program Flow and run it
    let mut prog_flow = program_flow::ProgFlow::new(
        clock,
        sht,
        vbat,
        config,
        sleep_fn,
        led_matrix::LedMatrix::new(led_driver),
        ComController::new(clock, bg77, rng),
    );

    // Run forever
    prog_flow.run();

    // #[allow(unreachable_code)]
    // unreachable!("prog_flow.run() should never return");
}
