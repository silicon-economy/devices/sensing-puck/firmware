// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module implements all conversion to an Image type, which is holding an array of the desired matrix size.
//! All the conversion methods are based on the From-Trait or the TryFrom-Trait.
//! It can be used to convert a temperature to an Image, a Symbol to an Image or an Id to an Image.

use super::symbols;
use crate::config;
use crate::hardware_abstraction::Frame;
use core::convert::From;
use core::convert::TryFrom;

/// Consts for the Matrix size
const WIDTH: usize = 13;
const HEIGHT: usize = 7;

/// Value of a fully brighten LED
const BRIGHTNESS_FULL: u8 = 255;

/// Custom type for a Temperature (i8) and the Id (u32) to be used in the From-Traits
pub type Temperature = i8;
pub type Id = u32;

/// Custom type for handling Errors
#[derive(PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Error {
    Number,
}

/// Type for holding the Symbols which can be shown on the LED matrix.
pub enum Symbol {
    Ok(usize),
    OkStatic(usize),
    Alarm(usize),
    Com(usize),
    ComAlarm(usize),
    LowBat(usize),
    IdBase,
    Err,
}

/// Custom type Image which holds the array pixels of the type u8.
///
/// The array is the size of the LED matrix and holds the brightness of each individual LED.
pub struct Image {
    pub pixels: [[u8; WIDTH]; HEIGHT],
}

impl From<Image> for Frame {
    fn from(img: Image) -> Self {
        Frame { pixels: img.pixels }
    }
}

#[allow(dead_code)]
pub enum Orientation {
    SensorUp,
    SensorDown,
}

/// Custom implementation for the type Image
impl Image {
    /// This function will set a complete frame to the given brightness. Doesn't care about the value which was in the array
    /// before, this method will set each pixel to the brightness value, whenever the value before was greater than zero.
    /// It returns an Image with the modified brightness.
    pub fn set_brightness(self, brightness: u8) -> Self {
        let mut img = Image {
            pixels: [[0_u8; WIDTH]; HEIGHT],
        };

        for x in 0..WIDTH {
            for y in 0..HEIGHT {
                match config::LED_MATRIX_ORIENTATION {
                    Orientation::SensorDown => {
                        img.pixels[y][x] = if self.pixels[y][x] != 0 {
                            brightness
                        } else {
                            0
                        }
                    }
                    Orientation::SensorUp => {
                        img.pixels[HEIGHT - y - 1][WIDTH - x - 1] = if self.pixels[y][x] != 0 {
                            brightness
                        } else {
                            0
                        }
                    }
                }
            }
        }
        img
    }
}

/// From-Trait implementation for converting a Symbol to an Image.
///
/// Returning an Image.
impl From<Symbol> for Image {
    fn from(sym: Symbol) -> Self {
        let mut pixels = [[0_u8; WIDTH]; HEIGHT];
        let center_pos = [WIDTH / 2_usize, HEIGHT / 2_usize];

        let sym_size = match sym {
            Symbol::Ok(_) => [symbols::OK[0][0].len(), symbols::OK[0].len()],
            Symbol::OkStatic(_) => [symbols::OK[0][0].len(), symbols::OK[0].len()],
            Symbol::Alarm(_) => [symbols::ALARM[0][0].len(), symbols::ALARM[0].len()],
            Symbol::Com(_) => [symbols::COM[0][0].len(), symbols::COM[0].len()],
            Symbol::ComAlarm(_) => [symbols::COM_ALARM[0][0].len(), symbols::COM_ALARM[0].len()],
            Symbol::LowBat(_) => [symbols::LOW_BAT[0][0].len(), symbols::LOW_BAT[0].len()],
            Symbol::Err => [symbols::ERR[0].len(), symbols::ERR.len()],
            Symbol::IdBase => [symbols::ID_BASE[0].len(), symbols::ID_BASE.len()],
        };

        let pos = [
            center_pos[0] - sym_size[0] / 2,
            center_pos[1] - sym_size[1] / 2,
        ];

        #[allow(clippy::needless_range_loop)] // An iterator would be very illegible here
        for x in pos[0]..(pos[0] + sym_size[0]) {
            for y in pos[1]..(pos[1] + sym_size[1]) {
                pixels[y][x] = match sym {
                    Symbol::Ok(i) => symbols::OK[i][y - pos[1]][x - pos[0]],
                    Symbol::OkStatic(_i) => symbols::OK[6][y - pos[1]][x - pos[0]],
                    Symbol::Alarm(i) => symbols::ALARM[i][y - pos[1]][x - pos[0]],
                    Symbol::Com(i) => symbols::COM[i][y - pos[1]][x - pos[0]],
                    Symbol::ComAlarm(i) => symbols::COM_ALARM[i][y - pos[1]][x - pos[0]],
                    Symbol::LowBat(i) => symbols::LOW_BAT[i][y - pos[1]][x - pos[0]],
                    Symbol::Err => symbols::ERR[y - pos[1]][x - pos[0]],
                    Symbol::IdBase => symbols::ID_BASE[y - pos[1]][x - pos[0]],
                } * 255;
            }
        }

        Image { pixels }
    }
}

/// TryFrom-Trait implementation for converting a Temperature to an Image.
///
/// Returning an Image if no error occurs. If the given Temperature is greater than 99 and smaller than -99, the
/// temperature cannot be visualized on the LED matrix and results in an error.
impl TryFrom<Temperature> for Image {
    type Error = Error;

    fn try_from(temp: Temperature) -> Result<Self, Error> {
        let mut img = [[0_u8; WIDTH]; HEIGHT];

        let (start_col, is_negative, first_digit, has_2_digits, second_digit) = match temp {
            -99..=-10 => (
                (WIDTH - 12) / 2_usize,
                true,
                ((-temp) / 10) as u8,
                true,
                ((-temp) % 10) as u8,
            ),
            -9..=-1 => (
                (WIDTH - 7) / 2_usize,
                true,
                ((-temp) % 10) as u8,
                false,
                0_u8,
            ),
            0..=9 => ((WIDTH - 3) / 2_usize, false, (temp % 10) as u8, false, 0_u8),
            10..=99 => (
                (WIDTH - 9) / 2_usize,
                false,
                (temp / 10) as u8,
                true,
                (temp % 10) as u8,
            ),
            _ => return Err(Error::Number),
        };

        let mut col = start_col;

        if is_negative {
            print_signum(&mut img, col, BRIGHTNESS_FULL);
            col += 3;
        }

        print_digit(&mut img, col, first_digit, BRIGHTNESS_FULL);

        if has_2_digits {
            col += 4;
            print_digit(&mut img, col, second_digit, BRIGHTNESS_FULL);
        }

        //print degree symbol °
        col += 4;
        img[1][col] = BRIGHTNESS_FULL;

        Ok(Image { pixels: img })
    }
}

/// From-Trait implementation for converting an Id to an Image.
///
/// Returning an Image.
impl From<Id> for Image {
    fn from(id: Id) -> Self {
        let mut img: Image = Symbol::IdBase.into();

        let mut mask = 1_u32;
        for row in 2_usize..=4_usize {
            for col in 1_usize..=11_usize {
                if mask & id != 0 {
                    img.pixels[row][col] = BRIGHTNESS_FULL;
                }
                if mask < 1 << 31 {
                    mask <<= 1;
                } else {
                    break;
                }
            }
        }

        // CRC16
        let mut crc16 = crc_any::CRC::crc16ccitt_false();
        let id_bytes: [u8; 4] = [
            (id & 0xFF) as u8,
            ((id & 0xFF00) >> 8) as u8,
            ((id & 0xFF0000) >> 16) as u8,
            ((id & 0xFF000000) >> 24) as u8,
            // ((id & 0xFF0000) >> 16) as u8,
            // ((id & 0xFF00) >> 8) as u8,
            // (id & 0xFF) as u8,
        ];

        crc16.digest(&id_bytes);

        let crc = crc16.get_crc() as u16;
        log::info!("{}", crc);

        let mut mask = 1_u16;
        for first_line in 3..=9 {
            if mask & crc != 0 {
                img.pixels[1][first_line] = BRIGHTNESS_FULL;
            }
            mask <<= 1;
        }
        for second_line in 2..=10 {
            if mask & crc != 0 {
                img.pixels[5][second_line] = BRIGHTNESS_FULL;
            }
            mask <<= 1;
        }
        img
    }
}

/// This method will print the given digit to an array.
///
/// Pos is the starting column of the digit. In this case a digit has a width of three pixels and a space of one pixel.
/// Digit can be from 0 to 9. Brightness will set the 'on' pixels to this value.
fn print_digit(img: &mut [[u8; WIDTH]; HEIGHT], pos: usize, digit: u8, brightness: u8) {
    let digit_size: [usize; 2] = [symbols::NUMBERS[0][0].len(), symbols::NUMBERS[0].len()]; //[3,5]
    let num = symbols::NUMBERS[digit as usize];

    for x in 0..digit_size[0] {
        for y in 0..digit_size[1] {
            img[y + (HEIGHT - digit_size[1]) / 2][x + pos] =
                (num[y as usize][x as usize]) * brightness;
        }
    }
}

/// This method will print a sign to an array.
///
/// Pos is the starting column of the sign which is two pixels in width. Brightness will set the 'on' pixels to this value.
fn print_signum(img: &mut [[u8; WIDTH]; HEIGHT], pos: usize, brightness: u8) {
    let digit_size: [usize; 2] = [symbols::SIGNUM[0].len(), symbols::SIGNUM.len()]; //[2,5]

    for x in 0..digit_size[0] {
        for y in 0..digit_size[1] {
            img[y + (HEIGHT - digit_size[1]) / 2][x + pos] =
                (symbols::SIGNUM[y as usize][x as usize]) * brightness;
        }
    }
}
