// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module contains user specific functions to control the LED matrix for our use case. It can print symbols
//! and/or fonts to the LED matrix which are stored in the symbols.rs file.
//! In order to show the current temperature onto the LED matrix, the function show_temperature can be used. This method and the
//! the methods show_com and show_ok will be play an animation containing of several frames.

use crate::hardware_abstraction::{
    Frame, FrameId, LedMatrixDriver, LED_MATRIX_HEIGHT as HEIGHT, LED_MATRIX_WIDTH as WIDTH,
};
use core::convert::TryInto;

mod font;
mod symbols;

pub use font::Orientation;

/// Const brightness for the LED matrix
const BRIGHTNESS: u8 = 0xFF;

/// Internal parameter for set_frames_animation
enum Animation {
    Ok,
    Alarm,
    Com,
    ComAlarm,
    LowBat,
}

/// Struct for implementing an LedMatrix object with ['WIDTH'] and ['HEIGHT'] as const generics
pub struct LedMatrix<M> {
    driver: M,
}

impl<M: LedMatrixDriver> LedMatrix<M> {
    /// Creates a new instance of an LedMatrix
    pub fn new(driver: M) -> Self {
        Self { driver }
    }

    /// This method enables (true) or disables (false) the Matrix LED driver chip
    pub fn power(&mut self, on: bool) -> Result<(), M::Error> {
        self.driver.power(on)
    }

    fn clear_frame(&mut self, frame_id: FrameId) -> Result<(), M::Error> {
        let frame_zero = [[0; WIDTH]; HEIGHT];
        self.driver
            .write_frame(frame_id, Frame { pixels: frame_zero })?;
        self.driver.show_frame(frame_id)
    }

    /// This method will show the temperature on the Matrix
    ///
    /// Before the temperature is shown, a user feedback is given for 1,4s. If the alarm flag is set to true, the user feedback is a cross,
    /// if it is set to false, the feedback is a tick.
    pub fn show_temperature(&mut self, temp: i8, alarm: bool) -> Result<(), M::Error> {
        self.clear_frame(FrameId::One)?;

        let temp: font::Temperature = temp;
        let img_temp: font::Image = temp.try_into().unwrap();
        let img_temp = img_temp.set_brightness(BRIGHTNESS);

        if alarm {
            self.set_frames_animation(Animation::Alarm, 4)?;

            self.driver.write_frame(FrameId::Five, img_temp.into())?;

            self.driver.play_animation(
                FrameId::One,
                FrameId::Four,
                false,
                core::time::Duration::from_millis(450),
            )
        } else {
            self.driver.write_frame(FrameId::One, img_temp.into())?;
            self.driver.show_frame(FrameId::One)
        }
    }

    /// This method will show static symbols which has defined in ['Symbol']
    ///
    /// It uses the first frame to show the static images. They will be present till the next operation with the Matrix.
    pub fn show_symbol(&mut self, sym: font::Symbol) -> Result<(), M::Error> {
        let mut img: font::Image = sym.into();
        img = img.set_brightness(BRIGHTNESS);

        self.driver.write_frame(FrameId::One, img.into())?;
        self.driver.show_frame(FrameId::One)
    }

    /// This method will show the ['Err'] symbol
    pub fn show_err(&mut self) -> Result<(), M::Error> {
        self.show_symbol(font::Symbol::Err)
    }

    /// This method will show an ID on the Matrix
    pub fn show_id(&mut self, id: u32) -> Result<(), M::Error> {
        let id: font::Id = id;
        let img: font::Image = id.into();

        self.driver.write_frame(FrameId::One, img.into())?;
        self.driver.show_frame(FrameId::One)
    }

    /// This method will animate a tick on the Matrix
    ///
    /// There are seven frames in total which are played with a frame_delay_time of 100 ms. The
    /// whole animation takes 700 ms and is played once. The led timeout should be chosen higher
    /// (e.g. 2 s) to show the OK symbol after is has been animated.
    pub fn show_ok(&mut self) -> Result<(), M::Error> {
        self.clear_frame(FrameId::One)?;

        self.set_frames_animation(Animation::Ok, 8)?;

        // Play to Animation to Frame 6, because after the animation Frame 7 is shown
        self.driver.play_animation(
            FrameId::One,
            FrameId::Seven,
            false,
            core::time::Duration::from_millis(100),
        )
    }

    /// This method will animate a low battery
    pub fn show_low_battery(&mut self) -> Result<(), M::Error> {
        self.clear_frame(FrameId::One)?;

        self.set_frames_animation(Animation::LowBat, 5)?;

        self.driver.play_animation(
            FrameId::One,
            FrameId::Four,
            true,
            core::time::Duration::from_millis(500),
        )
    }

    /// This method will animate a communication symbol
    ///
    /// There are five frames in total which are played with a frame_delay_time of 450ms. The whole animation takes 6,75s because the animation is played three times.
    /// The alarm flag sets a constant Tick next to the communication animation if it is set to false, else there will be a cross.
    pub fn show_com(&mut self, alarm: bool) -> Result<(), M::Error> {
        // Without setting the first image the animation seems to play backwards for the first few ms
        self.clear_frame(FrameId::One)?;

        if alarm {
            self.set_frames_animation(Animation::ComAlarm, 6)?;
        } else {
            self.set_frames_animation(Animation::Com, 6)?;
        }

        self.driver.play_animation(
            FrameId::One,
            FrameId::Six,
            true,
            core::time::Duration::from_millis(300),
        )
    }

    /// This method will write all eight frames to play an animation. It cycles through the frames and sets the desired one according to the ['sym']
    /// which is given. If the animation has less than eight frames, the rest will be cleared out.
    fn set_frames_animation(
        &mut self,
        animation: Animation,
        n_frames: usize,
    ) -> Result<(), M::Error> {
        for i in 0usize..=7usize {
            let frame = match i {
                0 => FrameId::One,
                1 => FrameId::Two,
                2 => FrameId::Three,
                3 => FrameId::Four,
                4 => FrameId::Five,
                5 => FrameId::Six,
                6 => FrameId::Seven,
                7 => FrameId::Eight,
                _ => panic!("set_frames_animation called with n_frames out of bounds"),
            };

            let img = match i {
                t if t < n_frames => {
                    let animation_image: font::Image = match animation {
                        Animation::Ok => font::Symbol::Ok(i).into(),
                        Animation::Alarm => font::Symbol::Alarm(i).into(),
                        Animation::Com => font::Symbol::Com(i).into(),
                        Animation::ComAlarm => font::Symbol::ComAlarm(i).into(),
                        Animation::LowBat => font::Symbol::LowBat(i).into(),
                    };
                    animation_image.set_brightness(BRIGHTNESS).pixels
                }
                _ => [[0_u8; WIDTH]; HEIGHT],
            };

            self.driver.write_frame(frame, Frame { pixels: img })?;
        }
        Ok(())
    }
}
