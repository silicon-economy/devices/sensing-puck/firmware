// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module provides an abstraction layer of the modem for the application
//!
//! It provides simple functions to transmit and receive application data via the modem

use crate::config::{self, DeviceConfig};
use crate::protocol_definition::downlink::Message as DownMessage;
use crate::protocol_definition::uplink::Message as UpMessage;
use crate::protocol_definition::*;

use embedded_nal::SocketAddrV4;

use try_ascii::try_ascii;

use core::convert::TryFrom;
use core::fmt::Debug;

use embedded_timers::clock::Clock;

#[cfg(feature = "mbedtls")]
mod mbedtls_uses {
    extern crate alloc;
    pub use alloc::{boxed::Box, sync::Arc};
    pub use mbedtls::rng::{CtrDrbg, EntropyCallback};
    pub use mbedtls::ssl::config::{Endpoint, Preset, Transport};
    pub use mbedtls::ssl::{Config, Context};
    //use mbedtls::x509::Certificate;
    //use mbedtls::Result as TlsResult;
    pub use core::convert::TryInto;
    pub use core::time::Duration;
    pub use embedded_timers::Timer;
    pub use mbedtls_sys::types::raw_types::{c_int, c_uchar, c_void};
    pub use mbedtls_sys::types::size_t;
}
#[cfg(feature = "mbedtls")]
use mbedtls_uses::*;

#[allow(dead_code)] // rng is not used if mbedtls is not activated
/// Communication Controller abstracting the BG77 to the application
pub struct ComController<CLOCK: 'static, BG77, RNG> {
    clock: &'static CLOCK,
    bg77: BG77,
    rng: RNG,
}

#[derive(Debug, PartialEq, Eq)]
pub enum ComError<BG77: embedded_nal::TcpClientStack> {
    /// Serialization failed
    Serialization(serde_json_core::ser::Error),
    /// Deserialization failed
    Deserialization(serde_json_core::de::Error),
    /// Unencrypted communication failed, an error in the [`TcpClientStack`] occurred
    TcpCommunication(BG77::Error),
    #[cfg(feature = "mbedtls")]
    /// TLS-encrypted communication failed
    TlsCommunication(mbedtls::Error),
    /// The RX buffer is full but the whole message has not yet been received
    RxBufFull,
    /// The RX operation timed out
    RxTimeout,
}

#[cfg(feature = "mbedtls")]
impl<BG77: embedded_nal::TcpClientStack> From<mbedtls::Error> for ComError<BG77> {
    fn from(e: mbedtls::Error) -> Self {
        ComError::TlsCommunication(e)
    }
}

impl<CLOCK, BG77, RNG> ComController<CLOCK, BG77, RNG>
where
    CLOCK: Clock + Sync,
    BG77: embedded_nal::TcpClientStack,
    RNG: embedded_hal::blocking::rng::Read + 'static,
    RNG::Error: Debug,
{
    pub fn new(clock: &'static CLOCK, bg77: BG77, rng: RNG) -> Self {
        Self { clock, bg77, rng }
    }

    /// Transmits the given measurements with the current timestamp and a given communication cause
    /// and receives the response from the backend. The response is converted to a Downlink Message.
    pub fn tx_rx<Storage, StorageErr>(
        &mut self,
        tx_data: &[MeasurementData],
        timestamp: u64,
        com_cause: ComCause,
        config: &DeviceConfig<Storage, StorageErr>,
    ) -> Result<DownMessage, ComError<BG77>>
    where
        Storage: embedded_storage::Storage<Error = StorageErr>,
        StorageErr: Debug,
    {
        #[allow(non_snake_case)]
        let versionNumber = if let Some(semver) = crate::version::VERSION.semver {
            FirmwareVersionNumber {
                major: semver.major,
                minor: semver.minor,
                patch: semver.patch,
                commitCounter: semver.commits,
                releaseFlag: semver.rc,
            }
        } else {
            FirmwareVersionNumber {
                major: 0,
                minor: 0,
                patch: 0,
                commitCounter: 0,
                releaseFlag: false,
            }
        };
        #[allow(non_snake_case)]
        let commitHash = u32::from_be_bytes(crate::version::VERSION.hash);
        #[allow(non_snake_case)]
        let firmwareDescriptor = FirmwareDescriptor {
            versionNumber,
            commitHash,
            dirtyFlag: crate::version::VERSION.dirty,
        };

        let message = UpMessage {
            protocolVersion: crate::config::PROTOCOL_VERSION,
            jobId: config.job_id(),
            deviceId: *config.device_id(),
            firmwareDescriptor,
            comTimestamp: timestamp,
            lastComCause: com_cause,
            data: tx_data,
        };

        // Roughly determined from an example message plus safety margin
        const BUF_LEN: usize = 300 + 120 * config::MEASUREMENT_CAPACITY;
        let mut data_buf = [0u8; BUF_LEN];
        // reserve space for the trailing newline when serializing
        let mut tx_len = message
            .serialize(&mut data_buf[..BUF_LEN - 1])
            .map_err(|e| {
                log::error!("Could not serialize data: {:?}", e);
                ComError::Serialization(e)
            })?;

        log::debug!("tx_buf.len: {} (without trailing newline)", tx_len);
        log::debug!("tx_buf: {:?}", try_ascii(&data_buf[..tx_len]));

        data_buf[tx_len] = b'\n';
        tx_len += 1;

        let rx_len = self.bg77_communicate(&mut data_buf, tx_len, config.socket_addr())?;
        // omit the trailing newline when deserializing
        DownMessage::try_from(&data_buf[..rx_len - 1]).map_err(|e| {
            log::error!("Could not deserialize data: {:?}", e);
            ComError::Deserialization(e)
        })
    }

    /// Opens the BG77 connection and socket, transmits the given data (splitting it in chunks
    /// of maximum transmission unit size) and tries to receive data until a newline is found
    /// which is the message termination condition between IoT-Broker and Sensing-Puck. This
    /// higher level knowledge must be used here because we do not know when to stop receiving
    /// otherwise.
    /// We re-use the tx data buffer for rx for efficiency here. Therefore, we have to specify
    /// the tx-len as an additional argument because the tx_rx_buf argument has the full buffer
    /// capacity, not only the length of the actual tx data.
    fn bg77_communicate(
        &mut self,
        tx_rx_buf: &mut [u8],
        tx_len: usize,
        socket_addr: SocketAddrV4,
    ) -> Result<usize, ComError<BG77>> {
        let mut socket = self.bg77.socket().unwrap();

        log::debug!("Connecting to {}", socket_addr);
        if let Err(e) = nb::block!(self
            .bg77
            .connect(&mut socket, embedded_nal::SocketAddr::V4(socket_addr)))
        {
            log::error!("Could not connect to network or remote socket: {:?}", e);
            return Err(ComError::TcpCommunication(e));
        }

        log::trace!("Connected, try to send via bg77");

        let ret = self.bg77_tx_rx(&mut socket, tx_rx_buf, tx_len);

        log::trace!("Send done, close socket");

        self.bg77.close(socket).unwrap();
        ret
    }

    /// Implement sending and receiving with an open socket. This structure allows to use
    /// the '?' operator without accidentally skipping the cleanup (closing the socket).
    #[cfg(not(feature = "mbedtls"))]
    fn bg77_tx_rx(
        &mut self,
        socket: &mut BG77::TcpSocket,
        tx_rx_buf: &mut [u8],
        tx_len: usize,
    ) -> Result<usize, ComError<BG77>> {
        let mut sent = 0;
        while sent < tx_len {
            let chunk_len =
                nb::block!(self.bg77.send(socket, &tx_rx_buf[sent..tx_len])).map_err(|e| {
                    log::error!("Could not transmit data to socket: {:?}", e);
                    ComError::TcpCommunication(e)
                })?;
            sent += chunk_len;
        }

        // TODO Receiving data with more than one chunk is actually untested because the broker
        // never sends that much data.
        let mut received = 0;
        loop {
            let chunk_len = self.rx_chunk(socket, &mut tx_rx_buf[received..], 10)?;
            received += chunk_len;
            if let Some(newline_pos) = tx_rx_buf[..received].iter().position(|&b| b == b'\n') {
                return Ok(newline_pos + 1);
            }
            if received >= tx_rx_buf.len() {
                log::error!("Receive error: Buffer is full.");
                return Err(ComError::RxBufFull);
            }
        }
    }

    /// Receive a single chunk
    #[cfg(not(feature = "mbedtls"))]
    fn rx_chunk(
        &mut self,
        socket: &mut BG77::TcpSocket,
        rx_buf: &mut [u8],
        num_tries: usize,
    ) -> Result<usize, ComError<BG77>> {
        use embedded_hal::blocking::delay::DelayMs;
        for _ in 0..num_tries {
            self.clock.new_delay().delay_ms(1000u32);

            match self.bg77.receive(socket, rx_buf) {
                Ok(len) => {
                    log::info!(
                        "Received chunk with len {}: {:?}",
                        len,
                        try_ascii(&rx_buf[..len])
                    );
                    return Ok(len);
                }
                Err(nb::Error::WouldBlock) => {
                    // Ignore
                }
                Err(nb::Error::Other(e)) => {
                    log::error!("Receive chunk error: {:?}", e);
                    return Err(ComError::TcpCommunication(e));
                }
            }
        }
        log::error!("Receiving chunk timed out.");
        Err(ComError::RxTimeout)
    }

    /// Implement sending and receiving with an open socket. This structure allows to use
    /// the '?' operator without accidentally skipping the cleanup (closing the socket).
    #[cfg(feature = "mbedtls")]
    fn bg77_tx_rx(
        &mut self,
        socket: &mut BG77::TcpSocket,
        tx_rx_buf: &mut [u8],
        tx_len: usize,
    ) -> Result<usize, ComError<BG77>> {
        let entropy_callback = MbedtlsEntropy { rng: &mut self.rng };
        let entropy = Arc::new(entropy_callback);
        let rng = Arc::new(CtrDrbg::new(entropy, None).unwrap());
        let mut config = Config::new(Endpoint::Client, Transport::Stream, Preset::Default);
        config.set_rng(rng);
        config.set_psk(config::PSK, config::PSK_IDENTITY).unwrap();
        let mut ctx = Context::new(Arc::new(config));

        let tls_socket = MbedtlsTcpSocket {
            delay: self.clock.new_delay(),
            bg77: &mut self.bg77,
            tcp_socket: socket,
        };
        log::debug!("Establishing TLS connection ...");
        ctx.establish(tls_socket, None).map_err(|e| {
            log::error!("Could not establish TLS connection: {:?}", e);
            e
        })?;

        log::debug!("TLS connection established, sending application data ...");

        ctx.send(&tx_rx_buf[..tx_len]).map_err(|e| {
            log::error!("Could not send data: {:?}", e);
            e
        })?;

        log::debug!("Application data sent, receiving application data ...");

        // TODO Receiving data with more than one chunk is actually untested because the broker
        // never sends that much data.
        let mut received = 0;
        loop {
            let chunk_len = ctx.recv(&mut tx_rx_buf[received..]).map_err(|e| {
                log::error!("Receiving plaintext chunk failed: {:?}", e);
                e
            })?;
            received += chunk_len;
            log::debug!(
                "Application data received: chunk {}, total {}",
                chunk_len,
                received
            );
            if let Some(newline_pos) = tx_rx_buf[..received].iter().position(|&b| b == b'\n') {
                log::debug!("End-of-message found");
                return Ok(newline_pos + 1);
            }
            if received >= tx_rx_buf.len() {
                log::error!("Receive error: Buffer is full.");
                return Err(ComError::RxBufFull);
            }
        }
    }
}

#[cfg(feature = "mbedtls")]
struct MbedtlsTcpSocket<'a, Delay, BG77: embedded_nal::TcpClientStack> {
    delay: Delay,
    bg77: &'a mut BG77,
    tcp_socket: &'a mut BG77::TcpSocket,
}

#[cfg(feature = "mbedtls")]
impl<Delay, BG77> mbedtls::ssl::context::IoCallback for MbedtlsTcpSocket<'_, Delay, BG77>
where
    Delay: embedded_hal::blocking::delay::DelayMs<u32>,
    BG77: embedded_nal::TcpClientStack,
{
    // At the time of writing (2022-06-17), reading zero bytes means that the connection has been
    // terminated, see for example ssl_msg.c lines 1969ff:
    /*
            if( ssl->f_recv_timeout != NULL )
                ret = ssl->f_recv_timeout( ssl->p_bio, ssl->in_hdr, len,
                                                                    timeout );
            else
                ret = ssl->f_recv( ssl->p_bio, ssl->in_hdr, len );

            MBEDTLS_SSL_DEBUG_RET( 2, "ssl->f_recv(_timeout)", ret );

            if( ret == 0 )
                return( MBEDTLS_ERR_SSL_CONN_EOF );
    */
    // As can be seen, this is especially caused by the fact that mbedtls-rust is not yet using the
    // f_recv_timeout function but f_recv instead.
    //
    // So we manually implement that "read with timeout" functionality here and avoid returning 0.
    unsafe extern "C" fn call_recv(user_data: *mut c_void, data: *mut c_uchar, len: size_t) -> c_int
    where
        Self: Sized,
    {
        let slf = &mut *(user_data as *mut MbedtlsTcpSocket<Delay, BG77>);
        let rx_buf = core::slice::from_raw_parts_mut(data, len);
        let num_tries = 10;
        for _ in 0..num_tries {
            match slf.bg77.receive(slf.tcp_socket, rx_buf) {
                Ok(len) => {
                    log::debug!("Received ciphertext chunk with len {}", len,);
                    return len.try_into().unwrap();
                }
                Err(nb::Error::WouldBlock) => {
                    // Ignore error and retry after delay
                    slf.delay.delay_ms(1000);
                }
                Err(nb::Error::Other(e)) => {
                    log::error!("Receive chunk error: {:?}", e);
                    return mbedtls_sys::ERR_NET_RECV_FAILED;
                }
            }
        }
        log::error!("Receiving chunk timed out.");
        mbedtls_sys::ERR_SSL_TIMEOUT
    }

    unsafe extern "C" fn call_send(
        user_data: *mut c_void,
        data: *const c_uchar,
        len: size_t,
    ) -> c_int
    where
        Self: Sized,
    {
        let slf = &mut *(user_data as *mut MbedtlsTcpSocket<Delay, BG77>);
        let tx_data = core::slice::from_raw_parts(data, len);
        let mut sent = 0;
        while sent < len {
            match nb::block!(slf.bg77.send(slf.tcp_socket, &tx_data[sent..len])) {
                Ok(chunk_len) => sent += chunk_len,
                Err(e) => {
                    log::error!("Could not transmit data to socket: {:?}", e);
                    return ::mbedtls_sys::ERR_NET_SEND_FAILED;
                }
            }
        }
        sent.try_into().unwrap()
    }

    // Returns self as void* as it is required by the mbedtls C interface.
    // The pointer is passed as `user_data` argument
    // to the other two methods `call_recv` and `call_send` above.
    fn data_ptr(&mut self) -> *mut c_void {
        let r: &mut MbedtlsTcpSocket<Delay, BG77> = self;
        r as *mut _ as *mut c_void
    }
}

#[cfg(feature = "mbedtls")]
/// Implements the [`EntropyCallback`] over the supplied RNG
///
/// The crux here is that we can neither move the RNG into our type because we will never get it
/// back nor can we supply a `&mut` because [`CtrDrbg::new`] requires
/// `T: EntropyCallback + 'static`, i.e. the argument must be allowed to live for a static lifetime
/// which a type containing a reference is not. Thus, we have to cheat here and use a raw pointer
/// which is `unsafe`ly used later. This is only safe because our [`CtrDrbg`] itself does not
/// outlive the method call because the [`Config`] does not outlive the method call and the
/// [`Context`] does not outlive the method call.
struct MbedtlsEntropy<RNG> {
    rng: *mut RNG,
}

#[cfg(feature = "mbedtls")]
unsafe impl<RNG> Send for MbedtlsEntropy<RNG> {}
#[cfg(feature = "mbedtls")]
unsafe impl<RNG> Sync for MbedtlsEntropy<RNG> {}

#[cfg(feature = "mbedtls")]
impl<RNG> EntropyCallback for MbedtlsEntropy<RNG>
where
    RNG: embedded_hal::blocking::rng::Read,
    RNG::Error: Debug,
{
    unsafe extern "C" fn call(user_data: *mut c_void, data: *mut c_uchar, len: size_t) -> c_int
    where
        Self: Sized,
    {
        let slf = (user_data as *mut MbedtlsEntropy<RNG>).as_mut().unwrap();
        let rng = unsafe { &mut *slf.rng };
        let bytes = core::slice::from_raw_parts_mut(data, len);
        rng.read(bytes).expect("RNG failed");
        0
    }

    // Returns self as void* as it is required by the mbedtls C interface.
    // The pointer is passed as `user_data` argument
    // to the other method `call` above.
    fn data_ptr(&self) -> *mut c_void {
        self as *const _ as *mut _
    }
}

#[cfg(feature = "mbedtls")]
struct MbedtlsTimer<CLOCK: Clock + 'static> {
    timer_int: Timer<'static, CLOCK>,
    timer_fin: Timer<'static, CLOCK>,
}

#[cfg(feature = "mbedtls")]
impl<CLOCK: Clock + Sync + 'static> mbedtls::ssl::context::TimerCallback for MbedtlsTimer<CLOCK> {
    unsafe extern "C" fn set_timer(
        p_timer: *mut mbedtls_sys::types::raw_types::c_void,
        int_ms: u32,
        fin_ms: u32,
    ) where
        Self: Sized,
    {
        let slf = (p_timer as *mut MbedtlsTimer<CLOCK>).as_mut().unwrap();
        slf.timer_int
            .try_start(Duration::from_millis(int_ms.into()))
            .unwrap();
        slf.timer_fin
            .try_start(Duration::from_millis(fin_ms.into()))
            .unwrap();
    }

    unsafe extern "C" fn get_timer(
        p_timer: *mut mbedtls_sys::types::raw_types::c_void,
    ) -> mbedtls_sys::types::raw_types::c_int
    where
        Self: Sized,
    {
        let slf = (p_timer as *mut MbedtlsTimer<CLOCK>).as_mut().unwrap();
        #[allow(clippy::bool_to_int_with_if)]
        if slf.timer_fin.is_expired().unwrap() {
            2
        } else if slf.timer_int.is_expired().unwrap() {
            1
        } else {
            0
        }
    }

    // Returns self as void* as it is required by the mbedtls C interface.
    // The pointer is passed as `user_data` argument
    // to the other two methods `set_timer` and `get_timer` above.
    fn data_ptr(&mut self) -> *mut mbedtls_sys::types::raw_types::c_void {
        self as *mut _ as *mut _
    }
}
