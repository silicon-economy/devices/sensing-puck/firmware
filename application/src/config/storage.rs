// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Persist configuration and internal state
//!
//! This module allows to write the current config and internal state to the EEPROM. The git
//! version hash will be prepended and the data will be secured with a CRC32. The deserialization
//! via [`restore`] will fail if the git version hash has changed or the CRC does not match.
//!
//! Currently, the git version hash is not part of the CRC-calculation but handled separately.
//! This has the benefit that we can early return when deserializing but it has the drawback that
//! the git version hash is not secured by the CRC. Alternatively, we could calculate the CRC
//! over both parts and handle all three parts (git version hash, data, crc) in a single buffer.
//! This would reduce the storage accesses to a single read and write call.

use core::convert::TryInto;
use core::fmt::Debug;

pub const MAX_SERIALIZED_LEN: usize = 256 - 4 - 4; // 2kBit minus hash minus crc32

#[derive(Debug)]
pub enum PersistError<StorageErr>
where
    StorageErr: Debug,
{
    GitHash,
    Serialization(postcard::Error),
    Deserialization(postcard::Error),
    Storage(StorageErr),
    Crc32,
}

use PersistError::*;

pub fn restore<'de, T, Storage, StorageErr>(
    storage: &mut Storage,
    version_hash: [u8; 4],
    buffer: &'de mut [u8; MAX_SERIALIZED_LEN],
) -> Result<T, PersistError<StorageErr>>
where
    T: serde::Deserialize<'de> + Clone + Copy,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
{
    // Check git version hash
    let mut version_bytes = [0u8; 4];
    storage.read(0, &mut version_bytes).map_err(Storage)?;
    if version_bytes != version_hash {
        return Err(GitHash);
    }
    // Deserialize data
    storage.read(4, buffer).map_err(|e| Storage(e))?;
    let (data, remaining_bytes) = postcard::take_from_bytes(buffer).map_err(Deserialization)?;
    let data_len = buffer.len() - remaining_bytes.len();
    let buffer = &buffer[..data_len];
    // Check CRC32
    let mut crc32_bytes = [0u8; 4];
    storage
        .read((4 + data_len).try_into().unwrap(), &mut crc32_bytes)
        .map_err(|e| Storage(e))?;
    let mut crc32 = crc_any::CRCu32::crc32();
    crc32.digest(buffer);
    if crc32_bytes != crc32.get_crc().to_be_bytes() {
        return Err(Crc32);
    }
    Ok(data)
}

pub fn persist<T, Storage, StorageErr>(
    storage: &mut Storage,
    version_hash: [u8; 4],
    value: T,
) -> Result<(), PersistError<StorageErr>>
where
    T: serde::Serialize,
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
{
    // Write git version hash, serialized data and crc32
    storage.write(0, &version_hash).map_err(Storage)?;
    let mut buf = [0u8; MAX_SERIALIZED_LEN];
    let serialized = postcard::to_slice(&value, &mut buf).map_err(|e| Serialization(e))?;
    storage.write(4, serialized).map_err(Storage)?;
    let mut crc32 = crc_any::CRCu32::crc32();
    crc32.digest(&serialized);
    storage
        .write(
            (4 + serialized.len()).try_into().unwrap(),
            &crc32.get_crc().to_be_bytes(),
        )
        .map_err(Storage)
}
