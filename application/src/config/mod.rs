// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Defines all of the Devices configuration
//!
//! There are different kinds of configuration defined here.
//!
//! 1. Job Config and other config changeable via backend
//! This is found in DEFAULT_REMOTE_CONFIG which also contains the job config. The only exception
//! is DEFAULT_MATRIX_ORIENTATION which is not yet available as a field in the downlink message.
//!
//! 2. Fixed Data/Config for communication
//! For example PROTOCOL_VERSION, DEVICE_ID and IP_ADDR, PORT.
//!
//! 3, Device Internal Config
//! For example SHOW_SUCCESS_TIMEOUT and SHOW_FAILURE_TIMEOUT or the alarm flag.
//!
//! This module also provides functions to get the latest config and to process new data.

// TODO Currently, we just store the whole downlink message but it only makes sense to store the
// configuration part of the message. For example, we do not want to support changing the device
// ID so this should not be stored. Or the clearAlarm flag is not a configuration but a one-time
// request to do something. It does not make sense to store such information in eeprom.

use crate::protocol_definition::downlink::Message as DlMessage;
use crate::protocol_definition::*;
use const_env::from_env; // can possibly be removed one day, see https://crates.io/crates/const_env
use core::fmt::Debug;
use core::time::Duration;
use embedded_nal::{Ipv4Addr, SocketAddrV4};
use serde::{Deserialize, Serialize};

mod storage;
use crate::version::VERSION;

/// Protocol Version supported by current version, see documentation for details
pub const PROTOCOL_VERSION: u8 = 2;
/// Unique Device ID
#[from_env("SENSING_PUCK_DEVICE_ID")]
pub const DEVICE_ID: u32 = 0;
/// PSK Identity for PSK-based TLS connection setup
#[cfg(feature = "mbedtls")]
#[from_env("SENSING_PUCK_PSK_IDENTITY")]
pub const PSK_IDENTITY: &str = "sensing-puck-0";
/// PSK for PSK-based TLS connection setup
#[cfg(feature = "mbedtls")]
#[from_env("SENSING_PUCK_PSK")]
pub const PSK: &[u8] = b"ABCDEFGHIJKLMNOQ";
/// Capacity of measurement vector (MAXMAX in protocol definition)
pub const MEASUREMENT_CAPACITY: usize = 100; // MAXMAX
/// Default Number of measurements before communicating with backend
pub const DEFAULT_DATA_AMOUNT: u32 = 5; // < MEASUREMENT_CAPACITY
/// times \[ms\] until the battery low symbol turns off again
pub const SHOW_LOW_BAT: Duration = Duration::from_millis(5000);
/// times \[ms\] the battery is not low, immediately turn off the display.
pub const SHOW_NO_LOW_BAT: Duration = Duration::from_millis(2);
/// Time \[s\] the "Communication Success" screen is shown
pub const SHOW_SUCCESS_TIMEOUT: Duration = Duration::from_secs(4);
/// Time \[s\] the "Communication Failure" screen is shown
pub const SHOW_FAILURE_TIMEOUT: Duration = Duration::from_secs(6);

/// Ip Address of the backend
pub const IP_ADDR: Ipv4Addr = Ipv4Addr::new(192, 44, 23, 73);
// Tcp Port of the backend
pub const PORT: u16 = 5683;

// TODO Can we do this better with Default trait?
/// Default config that is used if there is no valid config in EEPROM
pub const DEFAULT_REMOTE_CONFIG: DlMessage = DlMessage {
    protocolVersion: PROTOCOL_VERSION,
    jobId: 0,
    jobState: JobState::Finished,
    deviceId: DEVICE_ID,
    clearAlarm: false,
    measurementCycles: MeasurementConfig::new(10, 5),
    dataAmount: DEFAULT_DATA_AMOUNT,
    temperatureAlarm: AlarmConfig {
        upperBound: 35.0_f32,
        lowerBound: 15.0_f32,
    },
    humidityAlarm: AlarmConfig {
        upperBound: 65.0_f32,
        lowerBound: 30.0_f32,
    },
    voltageAlarm: Some(2.99),
    ledTimings: LedTimingConfig::new(10, 5, 4),
};

// TODO Add field in downlink message together with broker people
pub const LED_MATRIX_ORIENTATION: crate::led_matrix::Orientation =
    crate::led_matrix::Orientation::SensorUp;

/// Managing Object for device configuration
pub struct DeviceConfig<Storage, StorageErr>
where
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
{
    /// Job Config in form of the Downlink Message (stored in EEPROM)
    remote: DlMessage,
    /// Internal alarm flag (can only be reset via backend, stored in EEPROM)
    alarm_flag: bool,
    /// Indicates is transmission because of an alarm should happen
    alarm_send: bool,
    /// Indicates that a dataset (used as send condition)
    new_data: bool, // TODO I really don't like that flag. The whole event dispatching needs a rework.
    /// Indicates if the job state (Active/Finished) has changed
    new_job_state: bool,
    /// Eeprom as non volatile memory for remote and alarm_flag
    eeprom: Storage,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Copy)]
struct ConfigStorage {
    remote: DlMessage,
    alarm_flag: bool,
}

impl<Storage, StorageErr> DeviceConfig<Storage, StorageErr>
where
    Storage: embedded_storage::Storage<Error = StorageErr>,
    StorageErr: Debug,
{
    pub fn new(mut eeprom: Storage) -> Self {
        // Read the config from EEPROM or use a default config
        let mut buffer = [0u8; storage::MAX_SERIALIZED_LEN];
        let restore_res =
            storage::restore::<ConfigStorage, _, _>(&mut eeprom, VERSION.hash, &mut buffer);
        let (remote, alarm_flag) = match restore_res {
            Ok(restored) => {
                log::info!("Config restored from Eeprom.");
                log::info!("config.remote = {:?}", restored.remote);
                log::info!("config.alarm_flag = {:?}", restored.alarm_flag);
                (restored.remote, restored.alarm_flag)
            }
            Err(storage::PersistError::GitHash) => {
                log::info!("Git hash changed. Using default config.");
                (DEFAULT_REMOTE_CONFIG, false)
            }
            Err(e) => {
                log::warn!("Reading config from eeprom failed: {:?}", e);
                (DEFAULT_REMOTE_CONFIG, false)
            }
        };

        Self {
            remote,
            alarm_flag,
            alarm_send: false,
            new_data: false,
            new_job_state: true,
            eeprom,
        }
    }

    fn store_eeprom(&mut self) {
        let config_storage = ConfigStorage {
            remote: self.remote,
            alarm_flag: self.alarm_flag,
        };
        if let Err(e) = storage::persist(&mut self.eeprom, VERSION.hash, config_storage) {
            log::warn!("Failed to update Config in EEPROM: {:?}", e);
        } else {
            log::info!("Updated Config in EEPROM");
        }
    }

    // --------- Setter/Getter (Pairs) ---------------------------------------------
    /// Returns current Job ID
    pub fn job_id(&self) -> u64 {
        self.remote.jobId
    }

    // TODO Alarm is a state parameter and not a config, should be moved
    /// Updates the alarm state. If the state changes to true, the alarm_send flag will be set.
    /// The alarm flag will also be store in EEPROM.
    pub fn set_alarm(&mut self, value: bool) {
        if !self.alarm_flag && value {
            self.alarm_send = true;
        }

        self.alarm_flag = value;

        self.store_eeprom();
    }

    /// Returns alarm flag
    pub fn alarm(&self) -> &bool {
        &self.alarm_flag
    }

    /// Resets the alarm send flag
    pub fn clear_alarm_send(&mut self) {
        self.alarm_send = false;
    }

    /// Sets the new data flag for send condition
    pub fn set_new_data(&mut self) {
        self.new_data = true;
    }

    /// Returns the data amount for the current job
    pub fn data_amount(&self) -> u32 {
        self.remote.dataAmount
    }

    /// Sets new remote/job config and stores it in EEPROM
    /// Will also re-apply the default job if the job was finished
    /// Clears the alarm flag if requested by the backend and checks if the job state changed and sets new_job_state accordingly
    pub fn set_remote_config(&mut self, value: DlMessage) {
        self.new_job_state = self.remote.jobState != value.jobState;

        self.remote = value;

        if value.clearAlarm {
            // TODO This already calls store_eeprom() so it is possibly called
            // twice but I do not want to fix this currently because these
            // chunks of application logic seem misplaced in the config
            // module anyways
            self.set_alarm(false);
        }

        if value.jobState == crate::protocol_definition::JobState::Finished {
            self.remote = DEFAULT_REMOTE_CONFIG;
            // TODO See above: Possibly writing to eeprom three times?
            self.set_alarm(false);
            self.new_data = false;

            log::debug!("JobFinished, set Default Config");
        }

        self.store_eeprom();
    }

    /// Returns the device ID
    pub fn device_id(&self) -> &u32 {
        &self.remote.deviceId
    }

    /// Returns current measurement config
    pub fn measurement_config(&self) -> &MeasurementConfig {
        &self.remote.measurementCycles
    }

    /// Returns current temperature config
    pub fn temperature_alarm_config(&self) -> &AlarmConfig {
        &self.remote.temperatureAlarm
    }

    /// Return current humidity config
    pub fn humidity_alarm_config(&self) -> &AlarmConfig {
        &self.remote.humidityAlarm
    }

    /// Return current voltage config
    pub fn voltage_alarm_config(&self) -> &Option<f32> {
        &self.remote.voltageAlarm
    }

    /// Returns current display timing config
    pub fn led_timing_config(&self) -> &LedTimingConfig {
        &self.remote.ledTimings
    }

    // --------- Queries ---------------------------------------------
    /// Returns the new_job_state flag
    pub fn is_new_job_state(&self) -> bool {
        self.new_job_state
    }

    /// Returns if the current job state is active
    pub fn is_new_job_active(&self) -> bool {
        self.remote.jobState == JobState::Active
    }

    /// Checks for send condition.
    /// Returns true if we have new data (new_data == true) and
    /// the alarm_send flag is set or the current_data_length is at least the data amount from the job config
    pub fn is_send_condition(&mut self, current_data_length: u32) -> bool {
        if self.new_data {
            self.new_data = false;

            self.alarm_send || current_data_length >= self.remote.dataAmount
        } else {
            false
        }
    }

    // --------- Others ---------------------------------------------
    /// Clears the new job state
    pub fn clear_new_job_state(&mut self) {
        self.new_job_state = false;
    }

    /// Returns the Socket Address (IP + Port)
    pub fn socket_addr(&self) -> SocketAddrV4 {
        SocketAddrV4::new(IP_ADDR, PORT)
    }
}
