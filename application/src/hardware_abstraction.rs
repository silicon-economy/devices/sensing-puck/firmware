// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! This module contains traits which serve as hardware abstractions from the perspective of the
//! `application` crate. These traits will be implemented by the `firmware` crate using the
//! hardware supplied by the `board` as well as by the `simulator` using mocks and the `std` crate.

pub use measurements::*;

use core::fmt::Debug;

/// Generic sleep function trait
pub trait SleepFn {
    /// Sleep for the given duration or infinitely if `None`. Returns `true` in case of a
    /// double-tap, `false` in case of a regular wakeup (after the sleep duration).
    fn sleep(&mut self, duration: Option<core::time::Duration>) -> bool;
}

/// Generic sensor trait with functions common sensors
pub trait TemperatureHumiditySensor {
    /// Custom error type
    type Error: Debug;
    /// Read the sensors serial number
    fn read_serial_number(&mut self) -> Result<u32, Self::Error>;
    // fn start_measurement(&self, ...)
    // fn fetch_measurement(&self, ...)
    fn single_shot(&mut self) -> Result<(Temperature, Humidity), Self::Error>;
}

/// Trait to define types that allow voltage measurements
pub trait VoltageMeasurement {
    /// Custom error type
    type Error: Debug;
    // returns the battery voltage
    fn measurement_vbat(&mut self) -> Result<Voltage, Self::Error>;
}

//
// LED Matrix Driver
//

/// Const width of the LED Matrix
pub const LED_MATRIX_WIDTH: usize = 13;
/// Const height of the LED Matrix
pub const LED_MATRIX_HEIGHT: usize = 7;

/// Public struct for holding a frame array with the given size
///
/// Similar to the definition in the issi-is31fl3731 driver (without generics), redefined here to
/// avoid a dependency between driver and application.
pub struct Frame {
    pub pixels: [[u8; LED_MATRIX_WIDTH]; LED_MATRIX_HEIGHT],
}

/// Frame identifier
///
/// Similar to the definition in the issi-is31fl3731 driver (without enforcing specific values),
/// redefined here to avoid a dependency between driver and application.
#[derive(Clone, Copy)]
pub enum FrameId {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
}

/// LED Matrix Driver abstraction
pub trait LedMatrixDriver {
    /// Custom error type
    type Error: Debug;

    /// Power the driver (chip) on (true) or off (false)
    fn power(&mut self, turn_on: bool) -> Result<(), Self::Error>;

    /// Writes an array of PWM values to the desired frame
    ///
    /// The frame will be selected by `frame_id`. The driver should set each control and PWM pin
    /// according to the data in the `frame` array. For used LEDs, the control pin will be set to
    /// one and the PWM values will be set to the data in the `frame` array.
    fn write_frame(&mut self, frame_id: FrameId, frame: Frame) -> Result<(), Self::Error>;

    /// Displays the given frame with `frame_id` to the LED matrix
    fn show_frame(&mut self, frame_id: FrameId) -> Result<(), Self::Error>;

    /// Enables the AutoPlayAnimation feature of the LED driver
    ///
    /// The AutoPlayAnimation cycles from the `first_frame` to the `last_frame` with the
    /// `frame_delay_time` between each frame.  On the actual sensing puck hardware, the delay
    /// time will be discretized to multiples of 11ms with a maximum of 704ms (6 bit resolution).
    fn play_animation(
        &mut self,
        first_frame: FrameId,
        last_frame: FrameId,
        repeat: bool,
        inter_frame_delay: core::time::Duration,
    ) -> Result<(), Self::Error>;
}
