// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Defines the Uplink Message to send measurement to the backend

use super::*;

/// The Uplink Message struct for communicating with the backend
#[allow(non_snake_case)]
#[derive(Serialize, Debug, Copy, Clone)] // Deserializing data: &'a [MeasurementData] is impossible
pub struct Message<'a> {
    /// Protocol version used by the device
    pub protocolVersion: u8,
    /// Current job id
    pub jobId: u64,
    /// The Devices ID
    pub deviceId: u32,
    /// Descriptor of current firmware version
    pub firmwareDescriptor: FirmwareDescriptor,
    /// Timestamp of the communication (ms since start)
    pub comTimestamp: u64,
    /// Last Com Cause, currently "Data" | "Alarm" | "PreAlarm" | "User"
    pub lastComCause: ComCause, // "Data" | "Alarm" | "PreAlarm" | "User" | "JobRequest"
    /// The measurements to be transmitted
    pub data: &'a [MeasurementData], // array len <MAXMAX
}

impl<'a> Message<'a> {
    /// Serializes a Message into a byte slice
    pub fn serialize(&self, buf: &mut [u8]) -> Result<usize, serde_json_core::ser::Error> {
        serde_json_core::to_slice(&self, buf)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn com_causes() {
        let mut buf = [0_u8; 1024];

        let measurements = [
            MeasurementData {
                temperature: 20.0,
                humidity: 57.0,
                voltage: Some(3.55),
                timestamp: 90000,
                motionState: MotionState::Static,
            },
            MeasurementData {
                temperature: 22.0,
                humidity: 59.0,
                voltage: Some(3.50),
                timestamp: 91000,
                motionState: MotionState::Static,
            },
            MeasurementData {
                temperature: 25.0,
                humidity: 67.0,
                voltage: Some(3.45),
                timestamp: 92000,
                motionState: MotionState::Moving,
            },
        ];

        let message_data = Message {
            protocolVersion: 2,
            jobId: 8,
            deviceId: 17416,
            firmwareDescriptor: FirmwareDescriptor {
                versionNumber: FirmwareVersionNumber {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    commitCounter: 0,
                    releaseFlag: false,
                },
                commitHash: 0x12345678,
                dirtyFlag: false,
            },
            comTimestamp: 99999,
            lastComCause: ComCause::Data,
            data: &measurements[..],
        };
        let test_str_data = b"{\"protocolVersion\":2,\"jobId\":8,\"deviceId\":17416,\"firmwareDescriptor\":{\"versionNumber\":{\"major\":0,\"minor\":1,\"patch\":0,\"commitCounter\":0,\"releaseFlag\":false},\"commitHash\":305419896,\"dirtyFlag\":false},\"comTimestamp\":99999,\"lastComCause\":\"Data\",\"data\":[{\"temperature\":20.0,\"humidity\":57.0,\"voltage\":3.55,\"timestamp\":90000,\"motionState\":\"Static\"},{\"temperature\":22.0,\"humidity\":59.0,\"voltage\":3.5,\"timestamp\":91000,\"motionState\":\"Static\"},{\"temperature\":25.0,\"humidity\":67.0,\"voltage\":3.45,\"timestamp\":92000,\"motionState\":\"Moving\"}]}";

        let message_alarm = Message {
            protocolVersion: 2,
            jobId: 8,
            deviceId: 17416,
            firmwareDescriptor: FirmwareDescriptor {
                versionNumber: FirmwareVersionNumber {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    commitCounter: 0,
                    releaseFlag: false,
                },
                commitHash: 0x12345678,
                dirtyFlag: false,
            },
            comTimestamp: 99999,
            lastComCause: ComCause::Alarm,
            data: &measurements[..],
        };
        let test_str_alarm = b"{\"protocolVersion\":2,\"jobId\":8,\"deviceId\":17416,\"firmwareDescriptor\":{\"versionNumber\":{\"major\":0,\"minor\":1,\"patch\":0,\"commitCounter\":0,\"releaseFlag\":false},\"commitHash\":305419896,\"dirtyFlag\":false},\"comTimestamp\":99999,\"lastComCause\":\"Alarm\",\"data\":[{\"temperature\":20.0,\"humidity\":57.0,\"voltage\":3.55,\"timestamp\":90000,\"motionState\":\"Static\"},{\"temperature\":22.0,\"humidity\":59.0,\"voltage\":3.5,\"timestamp\":91000,\"motionState\":\"Static\"},{\"temperature\":25.0,\"humidity\":67.0,\"voltage\":3.45,\"timestamp\":92000,\"motionState\":\"Moving\"}]}";

        let message_pre_alarm = Message {
            protocolVersion: 2,
            jobId: 8,
            deviceId: 17416,
            firmwareDescriptor: FirmwareDescriptor {
                versionNumber: FirmwareVersionNumber {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    commitCounter: 0,
                    releaseFlag: false,
                },
                commitHash: 0x12345678,
                dirtyFlag: false,
            },
            comTimestamp: 99999,
            lastComCause: ComCause::PreAlarm,
            data: &measurements[..],
        };
        let test_str_pre_alarm = b"{\"protocolVersion\":2,\"jobId\":8,\"deviceId\":17416,\"firmwareDescriptor\":{\"versionNumber\":{\"major\":0,\"minor\":1,\"patch\":0,\"commitCounter\":0,\"releaseFlag\":false},\"commitHash\":305419896,\"dirtyFlag\":false},\"comTimestamp\":99999,\"lastComCause\":\"PreAlarm\",\"data\":[{\"temperature\":20.0,\"humidity\":57.0,\"voltage\":3.55,\"timestamp\":90000,\"motionState\":\"Static\"},{\"temperature\":22.0,\"humidity\":59.0,\"voltage\":3.5,\"timestamp\":91000,\"motionState\":\"Static\"},{\"temperature\":25.0,\"humidity\":67.0,\"voltage\":3.45,\"timestamp\":92000,\"motionState\":\"Moving\"}]}";

        let message_user = Message {
            protocolVersion: 2,
            jobId: 8,
            deviceId: 17416,
            firmwareDescriptor: FirmwareDescriptor {
                versionNumber: FirmwareVersionNumber {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    commitCounter: 0,
                    releaseFlag: false,
                },
                commitHash: 0x12345678,
                dirtyFlag: false,
            },
            comTimestamp: 99999,
            lastComCause: ComCause::User,
            data: &measurements[..],
        };
        let test_str_user = b"{\"protocolVersion\":2,\"jobId\":8,\"deviceId\":17416,\"firmwareDescriptor\":{\"versionNumber\":{\"major\":0,\"minor\":1,\"patch\":0,\"commitCounter\":0,\"releaseFlag\":false},\"commitHash\":305419896,\"dirtyFlag\":false},\"comTimestamp\":99999,\"lastComCause\":\"User\",\"data\":[{\"temperature\":20.0,\"humidity\":57.0,\"voltage\":3.55,\"timestamp\":90000,\"motionState\":\"Static\"},{\"temperature\":22.0,\"humidity\":59.0,\"voltage\":3.5,\"timestamp\":91000,\"motionState\":\"Static\"},{\"temperature\":25.0,\"humidity\":67.0,\"voltage\":3.45,\"timestamp\":92000,\"motionState\":\"Moving\"}]}";

        let message_job_request = Message {
            protocolVersion: 2,
            jobId: 8,
            deviceId: 17416,
            firmwareDescriptor: FirmwareDescriptor {
                versionNumber: FirmwareVersionNumber {
                    major: 0,
                    minor: 1,
                    patch: 0,
                    commitCounter: 0,
                    releaseFlag: false,
                },
                commitHash: 0x12345678,
                dirtyFlag: false,
            },
            comTimestamp: 99999,
            lastComCause: ComCause::JobRequest,
            data: &measurements[..],
        };
        let test_str_job_request = b"{\"protocolVersion\":2,\"jobId\":8,\"deviceId\":17416,\"firmwareDescriptor\":{\"versionNumber\":{\"major\":0,\"minor\":1,\"patch\":0,\"commitCounter\":0,\"releaseFlag\":false},\"commitHash\":305419896,\"dirtyFlag\":false},\"comTimestamp\":99999,\"lastComCause\":\"JobRequest\",\"data\":[{\"temperature\":20.0,\"humidity\":57.0,\"voltage\":3.55,\"timestamp\":90000,\"motionState\":\"Static\"},{\"temperature\":22.0,\"humidity\":59.0,\"voltage\":3.5,\"timestamp\":91000,\"motionState\":\"Static\"},{\"temperature\":25.0,\"humidity\":67.0,\"voltage\":3.45,\"timestamp\":92000,\"motionState\":\"Moving\"}]}";

        let ser_len = message_data.serialize(&mut buf).unwrap();
        assert_eq!(test_str_data, &buf[..ser_len]);

        let ser_len = message_alarm.serialize(&mut buf).unwrap();
        assert_eq!(test_str_alarm, &buf[..ser_len]);

        let ser_len = message_pre_alarm.serialize(&mut buf).unwrap();
        assert_eq!(test_str_pre_alarm, &buf[..ser_len]);

        let ser_len = message_user.serialize(&mut buf).unwrap();
        assert_eq!(test_str_user, &buf[..ser_len]);

        let ser_len = message_job_request.serialize(&mut buf).unwrap();
        assert_eq!(test_str_job_request, &buf[..ser_len]);
    }
}
