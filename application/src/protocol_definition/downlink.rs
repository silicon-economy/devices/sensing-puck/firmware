// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Defines the Downlink Message to send measurement to the backend

use super::*;

use core::convert::TryFrom;

/// Represents the downlink message received from the backend
#[allow(non_snake_case)]
#[derive(PartialEq, Serialize, Deserialize, Debug, Copy, Clone)]
pub struct Message {
    /// Protocol version used by the device
    pub protocolVersion: u8,
    /// Current job id
    pub jobId: u64,
    /// Job State, Active or Finished
    pub jobState: JobState, // "Active" or "Finished"
    /// The Devices ID
    pub deviceId: u32,
    /// True if the device should clear its internal alarm flag
    pub clearAlarm: bool,
    /// Measurement config
    pub measurementCycles: MeasurementConfig,
    /// Data Count for automatic transmission
    pub dataAmount: u32, // = MAX (<MAXMAX, MAXMAX predefined by us)
    /// Temperature range config
    pub temperatureAlarm: AlarmConfig,
    /// Humidity range config
    pub humidityAlarm: AlarmConfig,
    /// Battery voltage boundary config
    pub voltageAlarm: Option<f32>,
    /// Led/Display timings
    pub ledTimings: LedTimingConfig,
}

impl TryFrom<&[u8]> for Message {
    type Error = serde_json_core::de::Error;

    /// Tries to convert a byte slice to a Message
    fn try_from(from: &[u8]) -> Result<Self, Self::Error> {
        serde_json_core::from_slice::<'_, Message>(from).map(|(val, _consumed_len)| val)
    }
}

#[cfg(test)]
mod tests {
    use core::convert::TryFrom;

    use super::*;

    #[test]
    fn active_no_clear() {
        let test_str = b"{
            \"protocolVersion\": 2,
            \"jobId\": 8,
            \"jobState\": \"Active\",
            \"deviceId\": 42,
            \"clearAlarm\": false,
            \"measurementCycles\": {
                \"storage\": 12,
                \"transport\": 24
            },
            \"dataAmount\": 4,
            \"temperatureAlarm\": {
                \"upperBound\": 26.0,
                \"lowerBound\": 16.0
            },
            \"humidityAlarm\": {
                \"upperBound\": 40.0,
                \"lowerBound\": 60.0
            },
            \"voltageAlarm\": 3.0,
            \"ledTimings\": {
                \"showId\": 12,
                \"showAlarm\": 10,
                \"showTemp\": 3
            }
        }";

        let test_msg = Message {
            protocolVersion: 2,
            jobId: 8,
            jobState: JobState::Active,
            deviceId: 42,
            clearAlarm: false,
            measurementCycles: MeasurementConfig {
                storage: 12,
                transport: 24,
            },
            dataAmount: 4,
            temperatureAlarm: AlarmConfig {
                upperBound: 26.0_f32,
                lowerBound: 16.0_f32,
            },
            humidityAlarm: AlarmConfig {
                upperBound: 40.0_f32,
                lowerBound: 60.0_f32,
            },
            voltageAlarm: Some(3.0),
            ledTimings: LedTimingConfig {
                showId: 12,
                showAlarm: 10,
                showTemp: 3,
            },
        };

        let test_deser_msg = Message::try_from(&test_str[..]).unwrap();

        assert_eq!(test_msg, test_deser_msg);
    }

    #[test]
    fn active_clear() {
        let test_str = b"{
            \"protocolVersion\": 2,
            \"jobId\": 8,
            \"jobState\": \"Active\",
            \"deviceId\": 42,
            \"clearAlarm\": true,
            \"measurementCycles\": {
                \"storage\": 12,
                \"transport\": 24
            },
            \"dataAmount\": 4,
            \"temperatureAlarm\": {
                \"upperBound\": 26.0,
                \"lowerBound\": 16.0
            },
            \"humidityAlarm\": {
                \"upperBound\": 40.0,
                \"lowerBound\": 60.0
            },
            \"voltageAlarm\": 3.0,
            \"ledTimings\": {
                \"showId\": 12,
                \"showAlarm\": 10,
                \"showTemp\": 3
            }
        }";

        let test_msg = Message {
            protocolVersion: 2,
            jobId: 8,
            jobState: JobState::Active,
            clearAlarm: true,
            deviceId: 42,
            measurementCycles: MeasurementConfig {
                storage: 12,
                transport: 24,
            },
            dataAmount: 4,
            temperatureAlarm: AlarmConfig {
                upperBound: 26.0_f32,
                lowerBound: 16.0_f32,
            },
            humidityAlarm: AlarmConfig {
                upperBound: 40.0_f32,
                lowerBound: 60.0_f32,
            },
            voltageAlarm: Some(3.0),
            ledTimings: LedTimingConfig {
                showId: 12,
                showAlarm: 10,
                showTemp: 3,
            },
        };

        let test_deser_msg = Message::try_from(&test_str[..]).unwrap();

        assert_eq!(test_msg, test_deser_msg);
    }

    #[test]
    fn finished_no_clear() {
        let test_str = b"{
            \"protocolVersion\": 2,
            \"jobId\": 8,
            \"jobState\": \"Finished\",
            \"deviceId\": 42,
            \"clearAlarm\": false,
            \"measurementCycles\": {
                \"storage\": 12,
                \"transport\": 24
            },
            \"dataAmount\": 4,
            \"temperatureAlarm\": {
                \"upperBound\": 26.0,
                \"lowerBound\": 16.0
            },
            \"humidityAlarm\": {
                \"upperBound\": 40.0,
                \"lowerBound\": 60.0
            },
            \"voltageAlarm\": 3.0,
            \"ledTimings\": {
                \"showId\": 12,
                \"showAlarm\": 10,
                \"showTemp\": 3
            }
        }";

        let test_msg = Message {
            protocolVersion: 2,
            jobId: 8,
            jobState: JobState::Finished,
            deviceId: 42,
            clearAlarm: false,
            measurementCycles: MeasurementConfig {
                storage: 12,
                transport: 24,
            },
            dataAmount: 4,
            temperatureAlarm: AlarmConfig {
                upperBound: 26.0_f32,
                lowerBound: 16.0_f32,
            },
            humidityAlarm: AlarmConfig {
                upperBound: 40.0_f32,
                lowerBound: 60.0_f32,
            },
            voltageAlarm: Some(3.0),
            ledTimings: LedTimingConfig {
                showId: 12,
                showAlarm: 10,
                showTemp: 3,
            },
        };

        let test_deser_msg = Message::try_from(&test_str[..]).unwrap();

        assert_eq!(test_msg, test_deser_msg);
    }

    #[test]
    fn finished_clear() {
        let test_str = b"{
            \"protocolVersion\": 2,
            \"jobId\": 8,
            \"jobState\": \"Finished\",
            \"deviceId\": 42,
            \"clearAlarm\": true,
            \"measurementCycles\": {
                \"storage\": 12,
                \"transport\": 24
            },
            \"dataAmount\": 4,
            \"temperatureAlarm\": {
                \"upperBound\": 26.0,
                \"lowerBound\": 16.0
            },
            \"humidityAlarm\": {
                \"upperBound\": 40.0,
                \"lowerBound\": 60.0
            },
            \"voltageAlarm\": 3.0,
            \"ledTimings\": {
                \"showId\": 12,
                \"showAlarm\": 10,
                \"showTemp\": 3
            }
        }";

        let test_msg = Message {
            protocolVersion: 2,
            jobId: 8,
            jobState: JobState::Finished,
            deviceId: 42,
            clearAlarm: true,
            measurementCycles: MeasurementConfig {
                storage: 12,
                transport: 24,
            },
            dataAmount: 4,
            temperatureAlarm: AlarmConfig {
                upperBound: 26.0_f32,
                lowerBound: 16.0_f32,
            },
            humidityAlarm: AlarmConfig {
                upperBound: 40.0_f32,
                lowerBound: 60.0_f32,
            },
            voltageAlarm: Some(3.0),
            ledTimings: LedTimingConfig {
                showId: 12,
                showAlarm: 10,
                showTemp: 3,
            },
        };

        let test_deser_msg = Message::try_from(&test_str[..]).unwrap();

        assert_eq!(test_msg, test_deser_msg);
    }

    #[test]
    fn default_messages() {
        let default_no_job_test_str = b"{
                \"protocolVersion\": 2,
                \"jobId\": 0,
                \"jobState\": \"Finished\",
                \"deviceId\": 42,
                \"clearAlarm\": false,
                \"measurementCycles\": {
                    \"storage\": 60,
                    \"transport\": 10
                },
                \"dataAmount\": 12,
                \"temperatureAlarm\": {
                    \"upperBound\": 35.0,
                    \"lowerBound\": 15.0
                },
                \"humidityAlarm\": {
                    \"upperBound\": 65.0,
                    \"lowerBound\": 30.0
                },
                \"voltageAlarm\": 2.9,
                \"ledTimings\": {
                    \"showId\": 10,
                    \"showAlarm\": 5,
                    \"showTemp\": 5
                }
            }";

        let default_no_job_test_msg = Message {
            protocolVersion: 2,
            jobId: 0,
            jobState: JobState::Finished,
            deviceId: 42,
            clearAlarm: false,
            measurementCycles: MeasurementConfig {
                storage: 60,
                transport: 10,
            },
            dataAmount: 12,
            temperatureAlarm: AlarmConfig {
                upperBound: 35.0_f32,
                lowerBound: 15.0_f32,
            },
            humidityAlarm: AlarmConfig {
                upperBound: 65.0_f32,
                lowerBound: 30.0_f32,
            },
            voltageAlarm: Some(2.9),
            ledTimings: LedTimingConfig {
                showId: 10,
                showAlarm: 5,
                showTemp: 5,
            },
        };

        let deser_default_no_job_msg = Message::try_from(&default_no_job_test_str[..]).unwrap();

        assert_eq!(default_no_job_test_msg, deser_default_no_job_msg);

        let default_job_test_str = b"{
                \"protocolVersion\": 2,
                \"jobId\": 1,
                \"jobState\": \"Active\",
                \"deviceId\": 42,
                \"clearAlarm\": false,
                \"measurementCycles\": {
                    \"storage\": 60,
                    \"transport\": 10
                },
                \"dataAmount\": 12,
                \"temperatureAlarm\": {
                    \"upperBound\": 35.0,
                    \"lowerBound\": 15.0
                },
                \"humidityAlarm\": {
                    \"upperBound\": 65.0,
                    \"lowerBound\": 30.0
                },
                \"voltageAlarm\": 2.9,
                \"ledTimings\": {
                    \"showId\": 10,
                    \"showAlarm\": 5,
                    \"showTemp\": 5
                }
            }";

        let default_job_test_msg = Message {
            protocolVersion: 2,
            jobId: 1,
            jobState: JobState::Active,
            deviceId: 42,
            clearAlarm: false,
            measurementCycles: MeasurementConfig {
                storage: 60,
                transport: 10,
            },
            dataAmount: 12,
            temperatureAlarm: AlarmConfig {
                upperBound: 35.0_f32,
                lowerBound: 15.0_f32,
            },
            humidityAlarm: AlarmConfig {
                upperBound: 65.0_f32,
                lowerBound: 30.0_f32,
            },
            voltageAlarm: Some(2.9),
            ledTimings: LedTimingConfig {
                showId: 10,
                showAlarm: 5,
                showTemp: 5,
            },
        };

        let deser_default_job_msg = Message::try_from(&default_job_test_str[..]).unwrap();

        assert_eq!(default_job_test_msg, deser_default_job_msg);
    }
}
