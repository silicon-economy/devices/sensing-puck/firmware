// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Defines the Protocol between the device and the backend
//!
//! The communication is done via JSON.
//! The JSONs are converted to internal structs via serde.
//! See uplink and downlink for the specific TX and RX Messages
use core::time::Duration;
use serde::{Deserialize, Serialize};

pub mod downlink;
pub mod uplink;

/// State of a Job, can be Active or Finished
#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Copy, Clone)]
pub enum JobState {
    Active,
    Finished,
}

/// Measurement intervals \[s\]
#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Copy, Clone)]
pub struct MeasurementConfig {
    /// When no movement was detected
    storage: u32, // seconds
    /// While moving
    transport: u32, // seconds
}

impl MeasurementConfig {
    pub const fn new(storage: u32, transport: u32) -> Self {
        Self { storage, transport }
    }

    pub fn storage(&self) -> Duration {
        Duration::from_secs(self.storage as u64)
    }
    pub fn transport(&self) -> Duration {
        Duration::from_secs(self.transport as u64)
    }
}

/// Range config for temperature and humidity alarms.
/// Measurements outside of [upperBound, lowerBound] will cause an alarm.
#[allow(non_snake_case)]
#[derive(PartialEq, Serialize, Deserialize, Debug, Copy, Clone)]
pub struct AlarmConfig {
    pub upperBound: f32,
    pub lowerBound: f32,
}

/// Timeouts \[s\] until the Display turns off again
#[allow(non_snake_case)]
#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Copy, Clone)]
pub struct LedTimingConfig {
    /// When showing ID
    showId: u8, // seconds, > 5
    /// When showing the temperature while having an alarm
    showAlarm: u8, // seconds
    /// When showing the temperature while alarm flag is not set
    showTemp: u8, // seconds
}

impl LedTimingConfig {
    #[allow(non_snake_case)]
    pub const fn new(showId: u8, showAlarm: u8, showTemp: u8) -> Self {
        Self {
            showId,
            showAlarm,
            showTemp,
        }
    }
    pub fn show_id(&self) -> Duration {
        Duration::from_secs(self.showId as u64)
    }
    pub fn show_alarm(&self) -> Duration {
        Duration::from_secs(self.showAlarm as u64)
    }
    pub fn show_temp(&self) -> Duration {
        Duration::from_secs(self.showTemp as u64)
    }
}

// TODO The current git-semver implementation generates the version information with an
// Option<SemanticVersion> (here this would be Option<FirmwareVersionNumber>) and the
// commitHash is added as &str. The reasoning for this decision was that this is how we
// look at git hashes anyways.
// Maybe, the protocol could be adjusted to be compatible with git-semver so that we just
// have to use the output of that package?
#[allow(non_snake_case)]
#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Copy, Clone)]
pub struct FirmwareDescriptor {
    pub versionNumber: FirmwareVersionNumber,
    pub commitHash: u32,
    pub dirtyFlag: bool,
}

#[allow(non_snake_case)]
#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Copy, Clone)]
pub struct FirmwareVersionNumber {
    pub major: u32,
    pub minor: u32,
    pub patch: u32,
    pub commitCounter: u32,
    pub releaseFlag: bool,
}

/// Indicates why a communication was initialized
#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Copy, Clone)]
pub enum ComCause {
    /// Configure Data Amount reached
    Data,
    /// Alarm was just set
    Alarm,
    /// Not yet implemented
    PreAlarm,
    /// User triggered transmission by double tapping
    User,
    /// Requesting Job Data
    JobRequest,
}

/// Data structure for measurements
#[allow(non_snake_case)]
#[derive(PartialEq, Serialize, Deserialize, Debug, Copy, Clone)]
pub struct MeasurementData {
    /// Temperature \[°C\]
    pub temperature: f32,
    /// Humidity \[%rH\]
    pub humidity: f32,
    /// Voltage \[V\]
    pub voltage: Option<f32>,
    /// Timestamp where the measurement was taken (ms since start)
    pub timestamp: u64,
    /// Motion state at measurement time (currently always Static)
    pub motionState: MotionState,
}

/// State of motion
#[derive(PartialEq, Eq, Serialize, Deserialize, Debug, Copy, Clone)]
pub enum MotionState {
    /// No movement detected
    Static,
    /// Movement detected
    Moving,
}
