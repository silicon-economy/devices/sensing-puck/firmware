// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! # Simulator
//!
//! The simulator provides all the hardware abstractions required for running the sensing puck
//! application logic on a host machine. It offers a text-based command-line interface
//! to trigger events or set measurement values. The simulator can be used to run/test/debug the
//! application logic without the limitations that could arise from using the actual hardware or
//! without even requiring access to the actual hardware.
//!
//! Because the repository is configured to build for `thumbv7em-none-eabihf` by default, the
//! simulator has to be run with:
//! ```
//! cargo run --bin simulator --target <host-os-triple>
//! ```
//! Depending on your operating system, your `host-os-triple` could be for example
//! `x86_64-unknown-linux-gnu` or `aarch64-apple-darwin`.
//!
//! In the simulator, the set of available commands can be shown by typing `help`. At the time of
//! writing, the following commands are supported:
//! ```text
//! quit | q          - quit the simulator
//! temperature <f64> - set the temperature
//! humidity <f64>    - set the humidity
//! voltage <f64>     - set the battery voltage
//! tap               - double tap"
//! ```

use std::sync::{
    atomic::{AtomicBool, Ordering::Relaxed},
    Arc, Mutex,
};

mod hardware_impl;
use hardware_impl as hw;

fn main() {
    simple_logger::SimpleLogger::new().init().unwrap();

    let temperature = Arc::new(Mutex::new(measurements::Temperature::from_celsius(20.0)));
    let humidity = Arc::new(Mutex::new(measurements::Humidity::from_percent(50.0)));
    let th_sensor =
        hw::SimulatedTemperatureHumiditySensor::new(temperature.clone(), humidity.clone());
    let voltage = Arc::new(Mutex::new(measurements::Voltage::from_volts(3.6)));
    let voltage_measurement = hw::SimulatedVoltageMeasurement::new(voltage.clone());
    let storage = hw::SimulatedStorage::new();
    let led_matrix = hw::SimulatedLedMatrix::new();
    let modem = hw::SimulatedModem;
    let rng = hw::SimulatedRng;
    let clock = hw::SimulatedClock::get();
    let double_tap = Arc::new(AtomicBool::new(false));
    let sleep = hw::SimulatedSleep::new(double_tap.clone());

    std::thread::spawn(|| {
        application::sensing_puck_app(
            th_sensor,
            voltage_measurement,
            storage,
            led_matrix,
            modem,
            rng,
            clock,
            sleep,
        )
    });

    user_control(temperature, humidity, voltage, double_tap);
}

fn user_control(
    temperature: Arc<Mutex<measurements::Temperature>>,
    humidity: Arc<Mutex<measurements::Humidity>>,
    voltage: Arc<Mutex<measurements::Voltage>>,
    double_tap: Arc<AtomicBool>,
) {
    println!("Type 'help' to show all commands");
    loop {
        let mut input = String::new();
        if let Err(e) = std::io::stdin().read_line(&mut input) {
            eprintln!("read_line error: {e}");
            continue;
        }
        if let Some(input) = input.strip_prefix("temperature ") {
            let value: f64 = match input.trim_end().parse() {
                Ok(val) => val,
                Err(e) => {
                    eprintln!("Could not parse temperature (f64) '{input}': {e}");
                    continue;
                }
            };
            *temperature.lock().unwrap() = measurements::Temperature::from_celsius(value);
        } else if let Some(input) = input.strip_prefix("humidity ") {
            let value: f64 = match input.trim_end().parse() {
                Ok(val) => val,
                Err(e) => {
                    eprintln!("Could not parse humidity (f64) '{input}': {e}");
                    continue;
                }
            };
            *humidity.lock().unwrap() = measurements::Humidity::from_percent(value);
        } else if let Some(input) = input.strip_prefix("voltage ") {
            let value: f64 = match input.trim_end().parse() {
                Ok(val) => val,
                Err(e) => {
                    eprintln!("Could not parse voltage (f64) '{input}': {e}");
                    continue;
                }
            };
            *voltage.lock().unwrap() = measurements::Voltage::from_volts(value);
        } else if input.trim_end() == "tap" {
            double_tap.store(true, Relaxed);
        } else if input.trim_end() == "quit" || input.trim_end() == "q" {
            break;
        } else {
            println!(
                "\
quit | q          - quit the simulator
temperature <f64> - set the temperature
humidity <f64>    - set the humidity
voltage <f64>     - set the battery voltage
tap               - double tap"
            );
        }
    }
}
