// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use application::hardware_abstraction::{
    Frame, FrameId, LedMatrixDriver, LED_MATRIX_HEIGHT, LED_MATRIX_WIDTH,
};
use core::convert::Infallible;
use std::sync::mpsc;

/// "Shows" the LED Matrix by printing some simple ASCII characters to stderr. Stderr is chosen so
/// that the output can be redirected, if desired.
///
/// The animation is played in another background thread which is stopped when power is turned off
/// or `show_frame` is called. If `play_animation` is called when the animation is already being
/// played, we panic because this seems to be an application logic bug. In the beginning, we wanted
/// to also panic if `write_frame` is called when the animation is running but calling
/// `write_frame` immediately followed by `show_frame` seems to be the default way of transitioning
/// from an animation to a static image. Although this seems racy, this works in practice.
pub struct SimulatedLedMatrix {
    frames: [[[u8; LED_MATRIX_WIDTH]; LED_MATRIX_HEIGHT]; 8],
    /// Optionally stores a Sender to send a termination signal to the animation thread
    animation_thread_signal: Option<mpsc::Sender<()>>,
}

impl SimulatedLedMatrix {
    pub fn new() -> Self {
        Self {
            frames: [[[0; LED_MATRIX_WIDTH]; LED_MATRIX_HEIGHT]; 8],
            animation_thread_signal: None,
        }
    }

    fn stop_animation(&mut self) {
        if let Some(sender) = self.animation_thread_signal.take() {
            // Signal the animation thread to stop now. We ignore the errors because the thread for
            // a non-repeating animation could already have stopped.
            let _ = sender.send(());
        }
    }

    fn assert_no_animation(&mut self, err_msg: &str) {
        if let Some(_sender) = self.animation_thread_signal.take() {
            panic!("{}", err_msg);
        }
    }
}

/// LED Matrix Driver abstraction
impl LedMatrixDriver for SimulatedLedMatrix {
    type Error = Infallible;

    /// Power the driver (chip) on (true) or off (false)
    fn power(&mut self, turn_on: bool) -> Result<(), Infallible> {
        if !turn_on {
            self.stop_animation();
            print_frame([[0; LED_MATRIX_WIDTH]; LED_MATRIX_HEIGHT]);
        }
        Ok(())
    }

    /// Writes an array of PWM values to the desired frame
    ///
    /// The frame will be selected by `frame_id`. The driver should set each control and PWM pin
    /// according to the data in the `frame` array. For used LEDs, the control pin will be set to
    /// one and the PWM values will be set to the data in the `frame` array.
    fn write_frame(&mut self, frame_id: FrameId, frame: Frame) -> Result<(), Infallible> {
        // If something else should be shown after the animation, this is regularly done by calling
        // `write_frame` immediately followed by `show_frame`. This feels like a race condition but
        // works in practice.
        //self.assert_no_animation("animation was playing when write_frame was called");
        self.frames[get_index(frame_id)] = frame.pixels;
        Ok(())
    }

    /// Displays the given frame with `frame_id` to the LED matrix
    fn show_frame(&mut self, frame_id: FrameId) -> Result<(), Infallible> {
        self.stop_animation();
        print_frame(self.frames[get_index(frame_id)]);
        Ok(())
    }

    /// Enables the AutoPlayAnimation feature of the LED driver
    ///
    /// The AutoPlayAnimation cycles from the `first_frame` to the `last_frame` with the
    /// `frame_delay_time` between each frame.  On the actual sensing puck hardware, the delay
    /// time will be discretized to multiples of 11ms with a maximum of 704ms (6 bit resolution).
    fn play_animation(
        &mut self,
        first_frame: FrameId,
        last_frame: FrameId,
        repeat: bool,
        inter_frame_delay: core::time::Duration,
    ) -> Result<(), Infallible> {
        self.assert_no_animation("animation was playing when play_animation was called");
        let (tx, rx) = mpsc::channel();
        self.animation_thread_signal = Some(tx);
        let frames = self.frames;
        std::thread::spawn(move || loop {
            let from = get_index(first_frame);
            let to = get_index(last_frame);
            #[allow(clippy::needless_range_loop)]
            for index in from..=to {
                print_frame(frames[index]);
                std::thread::sleep(inter_frame_delay);
                match rx.try_recv() {
                    Ok(()) => return,
                    Err(mpsc::TryRecvError::Empty) => {},
                    Err(mpsc::TryRecvError::Disconnected) => panic!("SimulatedLedMatrix's signalling mpsc::Sender was dropped before any signal was sent"),
                }
            }
            if !repeat {
                break;
            }
        });
        Ok(())
    }
}

fn get_index(frame_id: FrameId) -> usize {
    match frame_id {
        FrameId::One => 0,
        FrameId::Two => 1,
        FrameId::Three => 2,
        FrameId::Four => 3,
        FrameId::Five => 4,
        FrameId::Six => 5,
        FrameId::Seven => 6,
        FrameId::Eight => 7,
    }
}

fn print_frame(flipped_pixels: [[u8; LED_MATRIX_WIDTH]; LED_MATRIX_HEIGHT]) {
    // optionally flip
    let mut pixels = flipped_pixels;
    if let application::LedMatrixOrientation::SensorUp = application::LED_MATRIX_ORIENTATION {
        for x in 0..LED_MATRIX_WIDTH {
            for y in 0..LED_MATRIX_HEIGHT {
                pixels[LED_MATRIX_HEIGHT - y - 1][LED_MATRIX_WIDTH - x - 1] = flipped_pixels[y][x];
            }
        }
    }
    // top row border
    eprint!("┌");
    for _ in 0..LED_MATRIX_WIDTH {
        eprint!("─");
    }
    eprintln!("┐");
    // rows
    for row in pixels.iter().take(LED_MATRIX_HEIGHT) {
        eprint!("│");
        for pixel in row.iter().take(LED_MATRIX_WIDTH) {
            match pixel {
                0..=50 => eprint!(" "),
                51..=101 => eprint!("░"),
                102..=153 => eprint!("▒"),
                154..=204 => eprint!("▓"),
                205..=255 => eprint!("█"),
            }
        }
        eprintln!("│");
    }
    // bottom row border
    eprint!("└");
    for _ in 0..LED_MATRIX_WIDTH {
        eprint!("─");
    }
    eprintln!("┘");
}
