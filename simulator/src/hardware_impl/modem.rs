// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use std::io::{self, Read, Write};
use std::net;
use std::time;

/// `embedded_nal::TcpClientStack` implementation over `std::net::TcpStream`s
///
/// The `SimulatedModem` can open as many connections as the OS allows. The `TcpStream`s are set to
/// a relatively low `read_timeout` so that they behave similar to nonblocking hardware modems.
pub struct SimulatedModem;

/// The `TcpSocket` type for the `embedded_nal::TcpClientStack` implementation with an internal
/// `TcpStream`. It is optional because a `std::net::TcpStream` can only be created in connected
/// state, i.e. if it is `Some`, it is connected.
pub struct SimulatedTcpSocket {
    stream: Option<net::TcpStream>,
}

impl embedded_nal::TcpClientStack for SimulatedModem {
    type TcpSocket = SimulatedTcpSocket;
    type Error = std::io::Error;

    fn socket(&mut self) -> Result<Self::TcpSocket, Self::Error> {
        Ok(SimulatedTcpSocket { stream: None })
    }

    fn connect(
        &mut self,
        socket: &mut Self::TcpSocket,
        remote: embedded_nal::SocketAddr,
    ) -> nb::Result<(), Self::Error> {
        let remote = match remote {
            embedded_nal::SocketAddr::V4(remote) => remote,
            embedded_nal::SocketAddr::V6(_) => panic!("IPv6 not implemented"),
        };
        let addr = remote.ip().octets();
        let port = remote.port();
        let addr = net::Ipv4Addr::new(addr[0], addr[1], addr[2], addr[3]);
        let remote = net::SocketAddrV4::new(addr, port);
        let stream = net::TcpStream::connect(remote)?;
        stream.set_read_timeout(Some(time::Duration::from_millis(10)))?;
        socket.stream = Some(stream);
        Ok(())
    }

    fn is_connected(&mut self, socket: &Self::TcpSocket) -> Result<bool, Self::Error> {
        Ok(socket.stream.is_some())
    }

    fn send(
        &mut self,
        socket: &mut Self::TcpSocket,
        buffer: &[u8],
    ) -> nb::Result<usize, Self::Error> {
        if let Some(stream) = &mut socket.stream {
            Ok(stream.write(buffer)?)
        } else {
            Err(nb::Error::Other(io::Error::from(
                io::ErrorKind::NotConnected,
            )))
        }
    }

    fn receive(
        &mut self,
        socket: &mut Self::TcpSocket,
        buffer: &mut [u8],
    ) -> nb::Result<usize, Self::Error> {
        if let Some(stream) = &mut socket.stream {
            match stream.read(buffer) {
                Ok(len) => {
                    // In mbedtls, length 0 is interpreted as EOF which terminates the connection.
                    // So in order to allow this, we return `nb::Error::WouldBlock` instead.
                    if len != 0 {
                        Ok(len)
                    } else {
                        Err(nb::Error::WouldBlock)
                    }
                }
                Err(e) => match e.kind() {
                    io::ErrorKind::WouldBlock | io::ErrorKind::TimedOut => {
                        Err(nb::Error::WouldBlock)
                    }
                    _ => Err(nb::Error::Other(e)),
                },
            }
        } else {
            Err(nb::Error::Other(io::Error::from(
                io::ErrorKind::NotConnected,
            )))
        }
    }

    fn close(&mut self, _socket: Self::TcpSocket) -> Result<(), Self::Error> {
        // we have ownership over socket now, RAII does the rest
        Ok(())
    }
}
