// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Hardware implementations for the simulator

pub mod temperature_humidity_sensor;
pub use temperature_humidity_sensor::SimulatedTemperatureHumiditySensor;
pub mod voltage_measurement;
pub use voltage_measurement::SimulatedVoltageMeasurement;
pub mod storage;
pub use storage::SimulatedStorage;
pub mod led_matrix;
pub use led_matrix::SimulatedLedMatrix;
pub mod modem;
pub use modem::SimulatedModem;
pub mod rng;
pub use rng::SimulatedRng;
pub mod clock;
pub use clock::SimulatedClock;
pub mod sleep;
pub use sleep::SimulatedSleep;
