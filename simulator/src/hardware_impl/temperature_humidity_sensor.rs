// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use core::convert::Infallible;
use std::sync::{Arc, Mutex};

/// Temperature and Humidity Sensor which returns temperature and humidity from variables behind
/// mutexes
pub struct SimulatedTemperatureHumiditySensor {
    temperature: Arc<Mutex<measurements::Temperature>>,
    humidity: Arc<Mutex<measurements::Humidity>>,
}

impl SimulatedTemperatureHumiditySensor {
    pub fn new(
        temperature: Arc<Mutex<measurements::Temperature>>,
        humidity: Arc<Mutex<measurements::Humidity>>,
    ) -> Self {
        Self {
            temperature,
            humidity,
        }
    }
}

impl application::hardware_abstraction::TemperatureHumiditySensor
    for SimulatedTemperatureHumiditySensor
{
    type Error = Infallible;

    fn read_serial_number(&mut self) -> Result<u32, Infallible> {
        Ok(0)
    }

    // fn start_measurement(&self, ...)
    // fn fetch_measurement(&self, ...)
    fn single_shot(
        &mut self,
    ) -> Result<(measurements::Temperature, measurements::Humidity), Infallible> {
        Ok((
            *self.temperature.lock().unwrap(),
            *self.humidity.lock().unwrap(),
        ))
    }
}
