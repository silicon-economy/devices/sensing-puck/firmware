// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use embedded_timers as etim;
use std::time;

/// An `embedded_timers::clock::Clock` using `std::time::Instant`, only used as shareable
/// reference, i.e. it is effectively a singleton
#[derive(Debug)]
pub struct SimulatedClock {
    creation_instant: time::Instant,
}

impl SimulatedClock {
    pub fn get() -> &'static Self {
        // We need a static SimulatedClock so we can return a &'static. Since we need the creation
        // time at runtime, this has to be initialized lazily.
        lazy_static::lazy_static! {
            static ref CLOCK: SimulatedClock = SimulatedClock {
                creation_instant: time::Instant::now(),
            };
        }
        &CLOCK
    }
}

impl etim::clock::Clock for SimulatedClock {
    /// Try to get the time since the clock was created
    fn try_now(&self) -> Result<etim::clock::Instant, etim::clock::ClockError> {
        Ok(time::Instant::now().duration_since(self.creation_instant))
    }
}
