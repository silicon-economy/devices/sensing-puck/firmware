// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use std::sync::{
    atomic::{AtomicBool, Ordering::Relaxed},
    Arc,
};
use std::{thread, time};

/// Takes `BitFlags` behind a mutex and returns a sleep function closure to call the application
///
/// Sleeps by calling `std::thread::sleep` regularly for at most 100 ms. In between, the given
/// `BitFlags` are checked if any flag is set ("interrupt"). If so, the sleep function returns.
pub struct SimulatedSleep {
    double_tap: Arc<AtomicBool>,
}

impl SimulatedSleep {
    pub fn new(double_tap: Arc<AtomicBool>) -> Self {
        Self { double_tap }
    }
}

impl application::hardware_abstraction::SleepFn for SimulatedSleep {
    fn sleep(&mut self, duration: Option<time::Duration>) -> bool {
        let sleep_start = time::Instant::now();
        self.double_tap.store(false, Relaxed);
        loop {
            let sleep_rest = duration.map(|d| d.saturating_sub(sleep_start.elapsed()));
            if let Some(true) = sleep_rest.map(|r| r == time::Duration::ZERO) {
                return false;
            }
            let sleep_chunk = match sleep_rest {
                Some(rest) if rest < time::Duration::from_millis(100) => rest,
                _ => time::Duration::from_millis(100),
            };
            thread::sleep(sleep_chunk);
            if self.double_tap.load(Relaxed) {
                return true;
            }
        }
    }
}
