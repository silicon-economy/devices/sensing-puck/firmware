// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use rand::RngCore;

/// Implements [`embedded_hal::blocking::rng::Read`] by using [`rand::rngs::ThreadRng`]
pub struct SimulatedRng;

impl embedded_hal::blocking::rng::Read for SimulatedRng {
    type Error = core::convert::Infallible;
    fn read(&mut self, buffer: &mut [u8]) -> Result<(), Self::Error> {
        rand::thread_rng().fill_bytes(buffer);
        Ok(())
    }
}
