// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use core::convert::Infallible;
use std::sync::{Arc, Mutex};

/// Voltage Measurement from variable behind mutex
pub struct SimulatedVoltageMeasurement {
    voltage: Arc<Mutex<measurements::Voltage>>,
}

impl SimulatedVoltageMeasurement {
    pub fn new(voltage: Arc<Mutex<measurements::Voltage>>) -> Self {
        Self { voltage }
    }
}

impl application::hardware_abstraction::VoltageMeasurement for SimulatedVoltageMeasurement {
    type Error = Infallible;
    fn measurement_vbat(&mut self) -> Result<measurements::Voltage, Infallible> {
        Ok(*self.voltage.lock().unwrap())
    }
}
