// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use embedded_storage::{ReadStorage, Storage};

/// Storage in a 2kBit buffer
pub struct SimulatedStorage {
    buffer: [u8; 256],
}

impl SimulatedStorage {
    pub fn new() -> Self {
        Self { buffer: [0; 256] }
    }
}

impl ReadStorage for SimulatedStorage {
    type Error = &'static str;
    fn read(&mut self, offset: u32, bytes: &mut [u8]) -> Result<(), Self::Error> {
        let offset = offset as usize;
        if offset + bytes.len() > self.buffer.len() {
            Err("Out of bounds read")
        } else {
            bytes.copy_from_slice(&self.buffer[offset..offset + bytes.len()]);
            Ok(())
        }
    }
    fn capacity(&self) -> usize {
        self.buffer.len()
    }
}

impl Storage for SimulatedStorage {
    fn write(&mut self, offset: u32, bytes: &[u8]) -> Result<(), Self::Error> {
        let offset = offset as usize;
        if offset + bytes.len() > self.buffer.len() {
            Err("Out of bounds write")
        } else {
            self.buffer[offset..offset + bytes.len()].copy_from_slice(bytes);
            Ok(())
        }
    }
}
